#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleTangle
    {
        private static byte[] data = System.Convert.FromBase64String("eV97aWw6a2x8Yi4fHwMKTz0AABtPLC5f7W5NX2JpZkXpJ+mYYm5ublJJCE/lXAWYYu2gsYTMQJY8BTQLRekn6Zhibm5qam9fDV5kX2ZpbDpiaWZF6SfpmGJubmpqb2ztbm5vM8ezEU1apUq6tmC5BLvNS0x+mM7DaWw6cmFreWt7RL8GKPsZZpGbBOLve0S/Bij7GWaRmwTiQS/JmCgiEF/ta9Rf7WzMz2xtbm1tbm1fYmlmWfYjQhfYguP0s5wY9J0ZvRhfIK42yGpmE3gvOX5xG7zY5ExUKMy6ACa3GfBcewrOGPumQm1sbm9uzO1uTwAJTxsHCk8bBwoBTw4fHwMGDA4bBgkGDA4bCk8NFk8OARZPHw4dGwtaTHokejZy3PuYmfPxoD/Vrjc/HQ4MGwYMCk8cGw4bCgIKARscQV9raXxtOjxefF9+aWw6a2V8ZS4fH0Bf7qxpZ0RpbmpqaG1tX+7Zde7cPQoDBg4BDApPAAFPGwcGHE8MCh3tbm9pZkXpJ+mYDAtqbl/unV9FaQ0DCk8cGw4BCw4dC08bCh0CHE8O3l83gzVrXeMH3OBysQockAgxCtNogxJW7OQ8T7xXq97Q9SBlBJBEkwMKTyYBDEFeSV9LaWw6a2R8ci4fBgkGDA4bBgABTy4aGwcAHQYbFl4WTw4cHBoCChxPDgwMCh8bDgEMCuAc7g+pdDRmQP3dlysnnw9X8XqaGwcAHQYbFl55X3tpbDprbHxiLh9qb2ztbmBvX+1uZW3tbm5vi/7GZq8MXBiYVWhDOYS1YE5htdUcdiDaGBhBDh8fAwpBDAACQA4fHwMKDA5nMV/tbn5pbDpyT2vtbmdf7W5rXxAux/eWvqUJ80sEfr/M1It0RaxwXFk1Xw1eZF9maWw6a2l8bTo8XnzkduaxliQDmmjETV9th3dRlz9mvFpdXltfXFk1eGJcWl9dX1ZdXltfpnYdmjJhuhAw9J1KbNU64CIyYp5LjYS+2B+wYCqOSKWeAheCiNp4eElfS2lsOmtkfHIuHx8DCk8sCh0b+vEVY8so5DS7eVhcpKtgIqF7Br4fAwpPLAodGwYJBgwOGwYAAU8uGl9+aWw6a2V8ZS4fHwMKTyYBDEFeFV/tbhlfYWlsOnJgbm6Qa2tsbW62WRCu6Dq2yPbWXS2Ut7oe8RHOPUEvyZgoIhBnMV9waWw6ckxrd1952HTS/C1LfUWoYHLZIvMxDKck73gfAwpPPQAAG08sLl9xeGJfWV9bXU8OAQtPDAodGwYJBgwOGwYAAU8fQ08MCh0bBgkGDA4bCk8fAAMGDBbEzB79KDw6rsBALtyXlIwfoonMI2DyUpxEJkd1p5Gh2tZhtjFzuaRScOrs6nT2UihYncb0L+FDu97/fbcqEXAjBD/5LuarGw1kf+wu6Fzl7mdEaW5qamhtbnlxBxsbHxxVQEAY2lXCm2Bhb/1k3k55QRu6U2K0DXkI4GfbT5ikw0NPAB/ZUG5f49gsoGlfYGlsOnJ8bm6Qa2pfbG5ukF9yAQtPDAABCwYbBgABHE8ACU8aHApw/rRxKD+EaoIxFutChFnNOCM6g9GbHPSBvQtgpBYgW7fNUZYXkASnP8XlurWLk79maFjfGhpO");
        private static int[] order = new int[] { 12,13,56,13,47,54,30,32,12,48,52,27,41,39,20,46,56,37,38,34,42,26,51,39,27,40,55,54,42,57,36,33,37,41,56,37,56,53,52,52,47,48,42,58,48,59,56,59,48,54,51,54,59,53,59,55,57,58,58,59,60 };
        private static int key = 111;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
