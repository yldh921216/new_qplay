#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("VKz47rrc6BV/2mmZ+rNb7gxTT+nf+rnj1oK8p4iSAxII7mrDvEONRF8rPHSqCZu5S/yed4uXEjOeVhvcc1IpVgbxH28tngTqab1i7x3v9NEOTHlmgVxD2HoE3bJduVtIhIAk9F/tbk1fYmlmRekn6Zhibm5uam9sGP2YoBhwjWoLIu/QYVF8cMAFHKRalIuTKfGf+Uoae3TFSO5wvafpq3duNp/8RBwj/yp03JpEQB4gyZ10rc3yqNS0KOnafOJh49lQwT04AentbmBvX+1uZW3tbm5vp6v4/qjTykXqlfcfqpnXGRXgVyCKtfsWDKJAoY9zftX57q0vzqeMtYnl6BtsfP7Sio7h5vpKpdWiMqFuFLk1NsZaLHX3QjXl7CTblm1sbm9u");
        private static int[] order = new int[] { 2,3,13,5,11,13,9,10,8,12,13,11,13,13,14 };
        private static int key = 111;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
