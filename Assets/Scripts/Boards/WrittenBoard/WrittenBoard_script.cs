﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EZObjectPools;
using UnityEngine.UI;
using Firebase.Database;
using Firebase.Extensions;
using TMPro;

public class WrittenBoard_script : MonoBehaviour
{
    public string currentWrittenBoard_uniqueKey;
    public string currentWrittenBoard_uid;
    EZObjectPool commentEzPool;
    public List<GameObject> commentInstanceList = new List<GameObject>();
    public Transform commentInstanceParentTransform;
    public Instance_button currentButtonScript;
    public Button thumbsUpButton;
    public TextMeshProUGUI thumbsUpCount_text;
    private BoardControlManager boardControlScript;
   
    public TextMeshProUGUI commentsCountText;
    public TextMeshProUGUI viewCountText;
    public TextMeshProUGUI boardCommentsCountText;
    public Text nicknameText;

    public List<string> boardsList = new List<string>();


    private void Start()
    {
        boardControlScript = FindObjectOfType<BoardControlManager>();
        commentEzPool = GameObject.Find("CommentInstance_objectsPool").GetComponent<EZObjectPool>();
        GoogleSignInDemo.Instance.databaseRef.Child("Boards").ChildChanged += WrittenBoard_script_ChildChanged;
  
       // GoogleSignInDemo.Instance.databaseRef.Child("Boards").Child(currentWrittenBoard_uniqueKey).Child("Comments").ChildRemoved += WrittenBoard_script_ChildRemoved;
       // GoogleSignInDemo.Instance.databaseRef.Child("Boards").Child(currentWrittenBoard_uniqueKey).Child("Comments").ChildAdded += WrittenBoard_script_ChildAdded;
    }
    private void OnEnable()
    {
        if( currentButtonScript == null )
        {
            this.gameObject.SetActive(false);
            return;
        }

        if (currentButtonScript.writerUid == DataBaseManager.Instance.currentPlayerUid)
        {
            thumbsUpButton.interactable = false;        
        }
        else
        {
            thumbsUpButton.interactable = true;
        }
        nicknameText.text = currentButtonScript.writerNickName;     
        


        for( int x =0;  x< DataBaseManager.Instance.commentEzPool.ObjectList.Count; x++)
        {
            Comment_instanceButton commentScript = DataBaseManager.Instance.commentEzPool.ObjectList[x].GetComponent<Comment_instanceButton>();
           
            if(commentScript.writtenBoardUniqueKey == currentWrittenBoard_uniqueKey)
            {
                DataBaseManager.Instance.commentEzPool.ObjectList[x].SetActive(true);
                commentInstanceList.Add(DataBaseManager.Instance.commentEzPool.ObjectList[x]);
            }
        }


    }

    private void OnDisable()
    {
        for (int x = 0; x < commentInstanceList.Count; x++)
        {
            if (commentInstanceList[x] != null)  //if you dont write null check , it make you Trouble
                commentInstanceList[x].SetActive(false);
        }
        commentInstanceList.Clear();



        if (!boardControlScript.bulletinBoardPanel.activeInHierarchy) boardControlScript.bulletinBoardPanel.SetActive(true);

    }
  
    private void WrittenBoard_script_ChildChanged(object sender, ChildChangedEventArgs e)
    {
        string contentValue = null;
        string tagValue = null;
        string titleValue = null;
        string writerUidValue = null;
        long thumbsUpCountValue = 0;
        
        if (e.Snapshot.Key == currentWrittenBoard_uniqueKey)
        {
            if(e.Snapshot.HasChild("thumbsUpCount"))
            {
                long.TryParse(e.Snapshot.Child("thumbsUpCount").Child("ThumbsUpCountValue").Value.ToString(), out thumbsUpCountValue);
                thumbsUpCount_text.text = thumbsUpCountValue.ToString();
                currentButtonScript.thumbsUpCount = thumbsUpCountValue;
            }


            //switch (e.Snapshot.Key)
            //{
            //    case "thumbsUpCount":
            //        long.TryParse(e.Snapshot.Child("ThumbsUpCountValue").Value.ToString(), out thumbsUpCountValue);
            //        thumbsUpCount_text.text = thumbsUpCountValue.ToString();
            //        currentButtonScript.thumbsUpCount = thumbsUpCountValue;
            //        break;

            //    case "content":
            //        currentButtonScript.content = contentValue;
            //        break;

            //    case "tag":
            //        currentButtonScript.tag = tagValue;
            //        break;

            //    case "title":
            //        currentButtonScript.title = titleValue;
            //        break;

            //    case "writerUid":
            //        currentButtonScript.writerUid = writerUidValue;
            //        break;

            }
     }       
    
    
    // Example
    private void WrittenBoard_script_ChildRemoved(object sender, ChildChangedEventArgs e)
    {
        print("Removed");
        string removedUniqueKey = e.Snapshot.Key;
        for (int x = 0; x <  DataBaseManager.Instance.commentInstancesList.Count; x++)
        {
            //Comment_instanceButton buttonScript = DataBaseManager.Instance.commentInstancesList[x].GetComponent<Comment_instanceButton>();
            //if (buttonScript.uniqueKey == removedUniqueKey)
            //{
            //    buttonScript.gameObject.GetComponent<PooledObject>().GoToAvailable();
            //    DataBaseManager.Instance.commentInstancesList.RemoveAt(x);
            //    return;
            //}
        }
    }
      private void WrittenBoard_script_ChildAdded(object sender, ChildChangedEventArgs e)
    {
        print("AddComment");
        print(e.Snapshot);
        print(e.Snapshot.Value);
        print(e.Snapshot.Children);

       string contentValue = null;
       string commentorUidValue = null;
       string commentorNicknameValue = null;
       string commentUniqueKey = null;
       string commentDate = null;
       string BoardKey = currentWrittenBoard_uniqueKey;
       
         commentUniqueKey = e.Snapshot.Key;

          foreach (var item in e.Snapshot.Children)
          {
             if (item.Key == "Contents") { contentValue = item.Value.ToString(); }
             else if (item.Key == "CommentUid") { commentorUidValue = item.Value.ToString(); }
             else if (item.Key == "Date") { commentDate = item.Value.ToString(); }   
             else if (item.Key == "CommentNickname") { commentorNicknameValue = item.Value.ToString();  }
          }
          
      bool isTrue = commentEzPool.TryGetNextObject(transform.position, Quaternion.identity, out GameObject comment_instanceButton);

        Comment_instanceButton commentButtonScript = null ;
       if (isTrue)
       {
          if (e.Snapshot != null)
           {
                 comment_instanceButton.name = "Comment_Instance" + e.Snapshot.Key;

                Text commentContentText = comment_instanceButton.transform.Find("Comment_content_text").GetComponent<Text>();

                comment_instanceButton.transform.parent = commentInstanceParentTransform;
                comment_instanceButton.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
                commentInstanceList.Add(comment_instanceButton);
                commentContentText.text = contentValue;

                commentButtonScript = comment_instanceButton.GetComponent<Comment_instanceButton>();
                commentButtonScript.content = contentValue;
                commentButtonScript.commentorUid = commentorUidValue;
                commentButtonScript.uniqueKey = e.Snapshot.Key;
                commentButtonScript.writtenBoardUniqueKey = BoardKey;
                commentButtonScript.commentorNickName = commentorNicknameValue;
            }
        }
  
        if (commentButtonScript != null)
        {
           print( commentorUidValue);
           GoogleSignInDemo.Instance.databaseRef.Child("User").Child(commentorUidValue).Child ("Inventory").Child("Equipment").GetValueAsync().ContinueWithOnMainThread(task =>
          {
              if (task.IsCompleted)
              {
                  DataSnapshot snapshot = task.Result;
                  foreach (var item in snapshot.Children)
                  {
              
                      switch (item.Key)
                      {
                         
                          case "EquippedBackItem":
                              commentButtonScript.commentor_backItemString = item.Value.ToString();
                              break;
                          case "EquippedBackgroundItem":
                              commentButtonScript.commentor_backgroundItemString = item.Value.ToString();
                              break;
                          case "EquippedBaseCharacterCode":
                              commentButtonScript.commentor_baseCharacterCodeString = item.Value.ToString();
                              break;
                          case "EquippedHairItem":
                              commentButtonScript.commentor_hairItemString = item.Value.ToString();
                              break;
                          case "EquippedBodyItem":
                              commentButtonScript.commentor_bodyItemString = item.Value.ToString();
                              break;
                          case "EquippedEarsItem":
                              commentButtonScript.commentor_earsItemString = item.Value.ToString();
                              break;
                          case "EquippedEyesItem":
                              commentButtonScript.commentor_eyesItemString = item.Value.ToString();
                              break;
                          case "EquippedMouthItem":
                              commentButtonScript.commentor_mouthItemString = item.Value.ToString();
                              break;
                          case "EquippedSpecialItem":
                              commentButtonScript.commentor_specialItemString = item.Value.ToString();
                              break;

                      }
                  }
                  commentButtonScript.getEquipment = true;
              }
              else
              {

              }
          });
        }








    }



    







}
