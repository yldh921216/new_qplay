﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeleteButton_comment : MonoBehaviour
{
    GameObject confirmPanel;
    Comment_instanceButton commentInstanceScript;
    ConfirmPanel confirmScript;
    Button deleteButton;

    private void OnEnable()
    {
        deleteButton = GetComponent<Button>();
        confirmPanel = GameObject.Find("LobbyCanvas").transform.Find("ConfirmPanel").gameObject;
        confirmScript = confirmPanel.GetComponent<ConfirmPanel>();
        commentInstanceScript = GetComponentInParent<Comment_instanceButton>();
        deleteButton.onClick.AddListener(() => DeleteButtonClick());
    }
    void OnDisable()
    {
        deleteButton.onClick.RemoveAllListeners();
    }

    private void DeleteButtonClick()
    {
        confirmScript.deleteProgress = true;
        confirmPanel.transform.Find("WarningText").GetComponent<Text>().text = "You want to delete?";
        confirmScript.commentInstanceScript = commentInstanceScript;
        confirmPanel.SetActive(true);
    }




}
