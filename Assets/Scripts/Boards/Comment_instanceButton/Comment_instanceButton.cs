﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Firebase.Database;
using TMPro;
using EZObjectPools;

public class Comment_instanceButton : MonoBehaviour
{
    private GameObject fixButton;
    private GameObject deleteButton;
    private Button commentButton;

    TextMeshProUGUI thumbsUpCount_text;
    Text contentText;
    Text commentorNicknameText;
    Button thumbsUpButton;

    WrittenBoard_script writtenBoardScript;



    public string writtenBoardUniqueKey;
    public string uniqueKey;
    public string commentorNickName;
    public string commentorUid;
    public string content;
    public string date_commentTime;
    public int thumbsUpCount;
    public byte[] profileImageBytes;

    [Header ("Commentor's Character Data")]
    public bool getEquipment;
    public string commentor_baseCharacterCodeString;      // girl0 , girl1, girl2 , boy0, boy1, boy2   
    public string commentor_backItemString;
    public string commentor_backgroundItemString;
    public string commentor_hairItemString;
    public string commentor_bodyItemString;
    public string commentor_earsItemString;
    public string commentor_eyesItemString;
    public string commentor_mouthItemString;
    public string commentor_specialItemString;


    [Header("Commentor Image")]
    Image commentorBackgroundImage;
    Image commentorBackImage;
    Image commentorBaseCharacterImgae;
    Image commentorHairImage;
    Image commentorBodyImage;
    Image commentorEyesImage;
    Image commentorEarsImage;
    Image commentorMouthImage;
    Image commentorSpecialImage;

    SoundManager soundManager;
    AudioSource audioSource;
    AudioClip thumbsUpClip;

   

    private void OnEnable()
    {
        if(!commentButton)
        commentButton = GetComponent<Button>();

        deleteButton = transform.Find("Delete_button").gameObject;
    
        if(!fixButton)
        fixButton = transform.Find("Fix_button").gameObject; 

        if(!contentText)
        contentText = transform.Find("Comment_content_text").GetComponent<Text>();

        if(!thumbsUpCount_text)
        thumbsUpCount_text = transform.Find("ThumbsUp_button").Find("ThumbsUpCount_text").GetComponent<TextMeshProUGUI>();
       
        if(!thumbsUpButton)
        thumbsUpButton = transform.Find("ThumbsUp_button").GetComponent<Button>();

        if(!writtenBoardScript)
        writtenBoardScript = DataBaseManager.Instance.writtenBoardScript;

        commentorNicknameText = transform.Find("Commentor_nickname_text").GetComponent<Text>();          
        
        commentorNicknameText.text = commentorNickName;

        StartCoroutine(SaveCommentorEquipmentSprite());
        
        if(!soundManager)
        { 
            soundManager = FindObjectOfType<SoundManager>();     
            audioSource = soundManager.GetComponent<AudioSource>();
            thumbsUpClip = soundManager.thumbsUpSoundEffect;
        }


        if(GoogleSignInDemo.Instance.firebaseUser.UserId == commentorUid)
        {
            deleteButton.SetActive(true);
        }

    }
    

    void Start()
   {
        commentButton.onClick.AddListener(() => ReadCommentInstance());
        thumbsUpButton.onClick.AddListener(() => OnClickThumbsUpButton());

        GoogleSignInDemo.Instance.databaseRef.Child("Boards").Child(writtenBoardUniqueKey).Child("Comments").Child(uniqueKey).ChildChanged += CommentChanged;
        if (commentorUid == GoogleSignInDemo.Instance.firebaseUser.UserId)
        {
            // fixButton.gameObject.SetActive(true);
            deleteButton.gameObject.SetActive(true);
        }


        commentorNicknameText.text = commentorNickName;
        StartCoroutine(SaveCommentorEquipmentSprite());


    }



    //private void Comment_instanceButton_ChildRemoved(object sender, ChildChangedEventArgs e)
    //{
    //    print("removed");
    //    string removedUniqueKey = e.Snapshot.Key;
    //    for (int x = 0; x < writtenBoardScript.commentInstanceList.Count; x++)
    //    {
    //        if (writtenBoardScript.commentInstanceList[x] != null)
    //        {
    //            Comment_instanceButton buttonScript = writtenBoardScript.commentInstanceList[x].GetComponent<Comment_instanceButton>();
    //            if (buttonScript.uniqueKey == removedUniqueKey)
    //            {
    //                writtenBoardScript.commentInstanceList.RemoveAt(x);
    //                writtenBoardScript.commentInstanceList[x].GetComponent<PooledObject>().GoToAvailable();
    //                return;
    //            }
    //        }


    //    }
    //}
    private void CommentChanged(object sender, ChildChangedEventArgs e)
    {
        string contentValue = null;
        int thumbsUpCountValue = 0;

        switch (e.Snapshot.Key)
        {
            case "ThumbsUpCount":
                int.TryParse(e.Snapshot.Child("ThumbsUpCountValue").Value.ToString(), out thumbsUpCountValue);
                thumbsUpCount_text.text = thumbsUpCountValue.ToString();
                thumbsUpCount = thumbsUpCountValue;
                break;

            case "Contents":
                contentValue = e.Snapshot.Value.ToString();
                content = contentValue;
                contentText.text = content;
                break;
        }
    }
    private void Comment_instanceButton_ChildChanged(object sender, ChildChangedEventArgs e)
    {
        if (getEquipment)
        {
            foreach (var item in e.Snapshot.Children)
            {
                switch (item.Key)
                {
                    case "EquippedBackItem":
                        commentor_backItemString = item.Value.ToString();
                        commentorBackImage.sprite = ItemManager.Instance.backItemDic[commentor_backItemString] as Sprite;
                        break;
                    case "EquippedBackgroundItem":
                        commentor_backgroundItemString = item.Value.ToString();
                        commentorBackgroundImage.sprite = ItemManager.Instance.backgroundItemDic[commentor_backgroundItemString] as Sprite;
                        break;
                    case "EquippedBaseCharacterCode":
                        commentor_baseCharacterCodeString = item.Value.ToString();
                        commentorBaseCharacterImgae.sprite = ItemManager.Instance.baseCharacterSpriteDic[commentor_baseCharacterCodeString] as Sprite;
                        break;
                    case "EquippedHairItem":
                        commentor_hairItemString = item.Value.ToString();
                        commentorHairImage.sprite = ItemManager.Instance.hairRankDic[commentor_hairItemString] as Sprite;
                        break;
                    case "EquippedBodyItem":
                        commentor_bodyItemString = item.Value.ToString();
                        commentorBodyImage.sprite = ItemManager.Instance.bodyItemsDic[commentor_bodyItemString] as Sprite;
                        break;
                    case "EquippedEarsItem":
                        commentor_earsItemString = item.Value.ToString();
                        commentorEarsImage.sprite = ItemManager.Instance.earsItemsDic[commentor_earsItemString] as Sprite;
                        break;
                    case "EquippedEyesItem":
                        commentor_eyesItemString = item.Value.ToString();
                        commentorEyesImage.sprite = ItemManager.Instance.eyesItemsDic[commentor_eyesItemString] as Sprite;
                        break;
                    case "EquippedMouthItem":
                        commentor_mouthItemString = item.Value.ToString();
                        commentorMouthImage.sprite = ItemManager.Instance.mouthItemsDic[commentor_mouthItemString] as Sprite;
                        break;
                    case "EquippedSpecialItem":
                        commentor_specialItemString = item.Value.ToString();
                        commentorSpecialImage.sprite = ItemManager.Instance.specialItemsDic[commentor_specialItemString] as Sprite;
                        break;
                }
            }
        }
        else
        {

        }
    }

    private void OnDisable()
    {
        commentButton.onClick.RemoveAllListeners();
    }
     
    private void ReadCommentInstance ()
    {
        // wideCommentWrittenPanel
        // comment+comment -- > 

    }
    private void OnClickThumbsUpButton()
    {
        audioSource.PlayOneShot(thumbsUpClip);
        GoogleSignInDemo.Instance.databaseRef.Child("Boards").Child(writtenBoardUniqueKey).Child("Comments").Child(uniqueKey).Child("ThumbsUpCount").RunTransaction(mutableData =>
        {
            int count = 0;

            if (mutableData.Child("ThumbsUpCountValue").Value == null || mutableData.Child("ThumbsUpCountValue").Value.ToString() == "")
            {
                count = 0;
            }
            else
            {
                int.TryParse(mutableData.Child("ThumbsUpCountValue").Value.ToString(), out int result);
                count = result;
            }

            Dictionary<string, object> thumbsUpCount = new Dictionary<string, object>();

            thumbsUpCount["ThumbsUpCountValue"] = count + 1;
            mutableData.Value = thumbsUpCount;

            DataBaseManager.Instance.currentGoldCount += 5;
            DataBaseManager.Instance.SetGold();
            DataBaseManager.Instance.goldBombCount = 5;
            StartCoroutine( DataBaseManager.Instance.GoldBomb(thumbsUpButton.transform.position));
            return TransactionResult.Success(mutableData);
        });

    }

    IEnumerator SaveCommentorEquipmentSprite()
    {
        while (!getEquipment)
        {
            yield return null;
        }

        thumbsUpCount_text.text = thumbsUpCount.ToString();

       commentorBackgroundImage = transform.Find("Commentor_profile_image").transform.Find("Commentor_background").GetComponent<Image>();
        commentorBackgroundImage.sprite = ItemManager.Instance.backgroundItemDic[commentor_backgroundItemString] as Sprite;

        commentorBackImage = transform.Find("Commentor_profile_image").transform.Find("Commentor_back").GetComponent<Image>();        
        commentorBackImage.sprite = ItemManager.Instance.backItemDic[commentor_backItemString] as Sprite;

        commentorBaseCharacterImgae = transform.Find("Commentor_profile_image").transform.Find("Commentor_base").GetComponent<Image>();
        commentorBaseCharacterImgae.sprite = ItemManager.Instance.baseCharacterSpriteDic[commentor_baseCharacterCodeString] as Sprite;

        commentorHairImage = transform.Find("Commentor_profile_image").transform.Find("Commentor_hair").GetComponent<Image>();
        commentorHairImage.sprite = ItemManager.Instance.hairItemsDic[commentor_hairItemString] as Sprite;

        commentorBodyImage = transform.Find("Commentor_profile_image").transform.Find("Commentor_body").GetComponent<Image>();
        commentorBodyImage.sprite = ItemManager.Instance.bodyItemsDic[commentor_bodyItemString] as Sprite;

        commentorEyesImage = transform.Find("Commentor_profile_image").transform.Find("Commentor_eyes").GetComponent<Image>();
        commentorEyesImage.sprite = ItemManager.Instance.eyesItemsDic[commentor_eyesItemString] as Sprite;

        commentorEarsImage = transform.Find("Commentor_profile_image").transform.Find("Commentor_ears").GetComponent<Image>();
        commentorEarsImage.sprite = ItemManager.Instance.earsItemsDic[commentor_earsItemString] as Sprite;

        commentorMouthImage = transform.Find("Commentor_profile_image").transform.Find("Commentor_mouth").GetComponent<Image>();
        commentorMouthImage.sprite = ItemManager.Instance.mouthItemsDic[commentor_mouthItemString] as Sprite;

        commentorSpecialImage = transform.Find("Commentor_profile_image").transform.Find("Commentor_special").GetComponent<Image>();
        commentorSpecialImage.sprite = ItemManager.Instance.specialItemsDic[commentor_specialItemString] as Sprite;

    }
}
