﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;
using Firebase.Database;

public class Instance_button : MonoBehaviour
{
    private BoardControlManager boardControlScript;

    Button instanceButton;
    Button fixButton;
    Button deleteButton;

    public string uniqueKey;
    public string writerNickName;
    public string writerUid;
    public string tagString;
    public string title;
    public string content;
    public string date_writingTime;
    public long  thumbsUpCount = 0;
    public long commentsCount;

    /////// Writer Character
    public bool getEquipment;
    public string writer_baseCharacterCodeString;      // girl0 , girl1, girl2 , boy0, boy1, boy2   
    public string writer_hairItemString;
    public string writer_bodyItemString;
    public string writer_earsItemString;
    public string writer_eyesItemString;
    public string writer_mouthItemString;
    public string writer_specialItemString;
    public string writer_backgroundItemString;
    public string writer_backItemString;

    Image backgroundImage;
    Image baseCharacterImage;
    Image backImage;
    Image hairItemImage;
    Image bodyItemImage;
    Image earsItemImage;
    Image eyesItemImage;
    Image mouthItemImage;
    Image specialItemImage;

    TextMeshProUGUI thumbsUpCountInstanceButtonText;
    TextMeshProUGUI commentsCountText;

    SoundManager soundManager;
    AudioSource audioSource;
    AudioClip clickSoundClip;





    private void OnEnable()
    {
        if (!instanceButton)
        {
            instanceButton = GetComponent<Button>();

            if(instanceButton == null)
            {
                this.gameObject.AddComponent<Button>();
            }
        }

      //fixButton = transform.Find("FixButton").GetComponentInChildren<Button>();
        deleteButton = transform.Find("DeleteButton").GetComponentInChildren<Button>();


        thumbsUpCountInstanceButtonText = transform.Find("ThumbsUpCount_image").transform.Find("ThumbsUpCount_text").GetComponent<TextMeshProUGUI>();
        commentsCountText = transform.Find("CommentCount_image").transform.Find("CommentCount_text").GetComponent<TextMeshProUGUI>();


        GetCommentsCount();


        instanceButton.onClick.AddListener(() => ReadInstanceButton());
        

        if (writerUid == GoogleSignInDemo.Instance.firebaseUser.UserId)
        {
           //fixButton.gameObject.SetActive(true);
            deleteButton.gameObject.SetActive(true);
        }

        StartCoroutine(SaveEquipmentSprite());

        GoogleSignInDemo.Instance.databaseRef.Child("User").Child(writerUid).Child("Inventory").Child("Equipment").ChildChanged += Instance_button_ChildChanged;



        soundManager = FindObjectOfType<SoundManager>();
        audioSource = soundManager.GetComponent<AudioSource>();
        clickSoundClip = soundManager.buttonClickSoundEffect;


    }

    public void GetCommentsCount()
    {
        GoogleSignInDemo.Instance.databaseRef.Child("Boards").Child(uniqueKey).Child("Comments").GetValueAsync().ContinueWith(task =>
        {
            if(task.IsCompleted)
            {
                DataSnapshot snapShot = task.Result;
                commentsCount = snapShot.ChildrenCount;
                commentsCountText.text = commentsCount.ToString();
            }
        });
    }





    private void Instance_button_ChildChanged(object sender, ChildChangedEventArgs e)
    {
        if(getEquipment)
        {
            foreach ( var item in e.Snapshot.Children)
            {
                print(item.Key);
                switch (item.Key)
                {
                    case "EquippedBackItem":
                           writer_backItemString = item.Value.ToString();
                           backImage.sprite = ItemManager.Instance.backItemDic[writer_backItemString] as Sprite;
                           break;
                    case "EquippedBackgroundItem":
                          writer_backgroundItemString = item.Value.ToString();
                          backgroundImage.sprite = ItemManager.Instance.backgroundItemDic[writer_backgroundItemString] as Sprite;
                          break;
                    case "EquippedBaseCharacterCode":
                           writer_baseCharacterCodeString  = item.Value.ToString();
                           baseCharacterImage.sprite = ItemManager.Instance.baseCharacterSpriteDic[writer_baseCharacterCodeString] as Sprite;
                           break;
                    case "EquippedHairItem":
                           writer_hairItemString  = item.Value.ToString();
                           hairItemImage.sprite = ItemManager.Instance.hairRankDic[writer_hairItemString] as Sprite;
                           break;
                    case "EquippedBodyItem":
                           writer_bodyItemString = item.Value.ToString();
                           bodyItemImage.sprite = ItemManager.Instance.bodyItemsDic[writer_bodyItemString] as Sprite;
                           break;
                    case "EquippedEarsItem":
                           writer_earsItemString = item.Value.ToString();
                           earsItemImage.sprite = ItemManager.Instance.earsItemsDic[writer_earsItemString] as Sprite;
                           break;
                    case "EquippedEyesItem":
                           writer_eyesItemString = item.Value.ToString();
                           eyesItemImage.sprite = ItemManager.Instance.eyesItemsDic[writer_eyesItemString] as Sprite;
                           break;
                    case "EquippedMouthItem":
                           writer_mouthItemString  = item.Value.ToString();
                          mouthItemImage.sprite = ItemManager.Instance.mouthItemsDic[writer_mouthItemString] as Sprite;
                           break;
                    case "EquippedSpecialItem":
                           writer_specialItemString = item.Value.ToString();
                           specialItemImage.sprite = ItemManager.Instance.specialItemsDic[writer_specialItemString] as Sprite;
                           break;
                }
            }
        }
        else
        {
            print("Didnt get Board Equipment yet");
        }
    }

    public void ReadInstanceButton()  
    {
        audioSource.clip = clickSoundClip;
        audioSource.Play();


        DataBaseManager.Instance.boardControlScript.bulletinBoardPanel.SetActive(false);

        if (!DataBaseManager.Instance.writtenBoard.activeInHierarchy)
        {
           DataBaseManager.Instance.writtenBoardScript.currentWrittenBoard_uid = writerUid;
           DataBaseManager.Instance.writtenBoardScript.currentWrittenBoard_uniqueKey = uniqueKey;
           DataBaseManager.Instance.writtenBoardScript.currentButtonScript= this;
           DataBaseManager.Instance.writtenBoard.SetActive(true);
           



           GameObject selectedButton = EventSystem.current.currentSelectedGameObject;
          

            if (selectedButton.GetComponent<Instance_button>() != null)
            {
                Instance_button buttonScript = selectedButton.GetComponent<Instance_button>();
                string uIdValue = buttonScript.writerUid;
                string tagValue = buttonScript.tagString;
                string titleValue = buttonScript.title;
                string contentValue = buttonScript.content;
                string nicknameValue = buttonScript.writerNickName;
                long thumbsUpValue = buttonScript.thumbsUpCount;

                // byte[] writerProfileImageValue = buttonScript.profileImageBytes;


                if (!DataBaseManager.Instance.writtenBoard_contentText) { Debug.LogError("null : writtenBoard_contentText"); return; }
                 DataBaseManager.Instance.writtenBoard_contentText.text = contentValue;

                if (!DataBaseManager.Instance.writtenBoard_titleText) { Debug.LogError("null : writtenBoard_titleText"); return; }
                DataBaseManager.Instance.writtenBoard_titleText.text = titleValue;

                if (!DataBaseManager.Instance.writtenBoard_thumbsUpText) { Debug.LogError("null : writtenBoard_thumbsUpText"); return; }
                DataBaseManager.Instance.writtenBoard_thumbsUpText.text = thumbsUpValue.ToString();

                if(!DataBaseManager.Instance.writtenBoard_commentsCountText) { Debug.LogError("null : writtenBoard_thumbsUpText"); return; }
                DataBaseManager.Instance.writtenBoard_commentsCountText.text = commentsCount.ToString();

                if(!backgroundImage.sprite) { Debug.LogError("null : writtenBoard_backgroundImage"); return; }
                DataBaseManager.Instance.writtenBoard_backgroundItemImage.sprite = backgroundImage.sprite;

                if(!backImage.sprite) { Debug.LogError("null : writtenBoard_backImage"); return; }
                DataBaseManager.Instance.writtenBoard_backItemImage.sprite = backImage.sprite;

                if (!baseCharacterImage.sprite) { Debug.LogError("null : writtenBoard_baseCharacterImage"); return; }
                DataBaseManager.Instance.writtenBoard_baseCharacterImage.sprite = baseCharacterImage.sprite;

                if (!hairItemImage.sprite) { Debug.LogError("null : writtenBoard_hairItemImage"); return; }
                DataBaseManager.Instance.writtenBoard_hairItemImage.sprite = hairItemImage.sprite;

                if (!bodyItemImage.sprite) { Debug.LogError("null :.writtenBoard_bodyItemImage"); return; }
                DataBaseManager.Instance.writtenBoard_bodyItemImage.sprite = bodyItemImage.sprite;

                if (!earsItemImage.sprite) { Debug.LogError("null :writtenBoard_earsItemImage"); return; }
                DataBaseManager.Instance.writtenBoard_earsItemImage.sprite = earsItemImage.sprite;

                if (!eyesItemImage.sprite) { Debug.LogError("writtenBoard_eyesItemImage"); return; }
                DataBaseManager.Instance.writtenBoard_eyesItemImage.sprite = eyesItemImage.sprite;

                if (!mouthItemImage.sprite) { Debug.LogError("null : writtenBoard_mouthItemImage"); return; }
                DataBaseManager.Instance.writtenBoard_mouthItemImage.sprite = mouthItemImage.sprite;

                if (!specialItemImage.sprite) { Debug.LogError("null : writtenBoard_specialItemImage"); return; }
                DataBaseManager.Instance.writtenBoard_specialItemImage.sprite = specialItemImage.sprite;
            }       
            
        }
    }

   IEnumerator SaveEquipmentSprite()
    {
        while (!getEquipment) yield return null;
       
        thumbsUpCountInstanceButtonText.text = thumbsUpCount.ToString();

        backgroundImage = transform.parent.Find("profile_image_parent").transform.Find("Writer_backgroundItem").GetComponent<Image>();
        backgroundImage.sprite = ItemManager.Instance.backgroundItemDic[writer_backgroundItemString] as Sprite;
    
        backImage = transform.parent.Find("profile_image_parent").transform.Find("Writer_backItem").GetComponent<Image>();
        backImage.sprite = ItemManager.Instance.backItemDic[writer_backItemString] as Sprite;

        baseCharacterImage = transform.parent.Find("profile_image_parent").transform.Find("Writer_baseItem").GetComponent<Image>();
        baseCharacterImage.sprite = ItemManager.Instance.baseCharacterSpriteDic[writer_baseCharacterCodeString] as Sprite;

        hairItemImage = transform.parent.Find("profile_image_parent").transform.Find("Writer_hairItem").GetComponent<Image>();
        hairItemImage.sprite = ItemManager.Instance.hairItemsDic[writer_hairItemString] as Sprite;

        bodyItemImage = transform.parent.Find("profile_image_parent").transform.Find("Writer_bodyItem").GetComponent<Image>();
        bodyItemImage.sprite = ItemManager.Instance.bodyItemsDic[writer_bodyItemString] as Sprite;

        eyesItemImage = transform.parent.Find("profile_image_parent").transform.Find("Writer_eyesItem").GetComponent<Image>();
        eyesItemImage.sprite = ItemManager.Instance.eyesItemsDic[writer_eyesItemString] as Sprite;

        earsItemImage = transform.parent.Find("profile_image_parent").transform.Find("Writer_earsItem").GetComponent<Image>();
        earsItemImage.sprite = ItemManager.Instance.earsItemsDic[writer_earsItemString] as Sprite;

        mouthItemImage = transform.parent.Find("profile_image_parent").transform.Find("Writer_mouthItem").GetComponent<Image>();
        mouthItemImage.sprite = ItemManager.Instance.mouthItemsDic[writer_mouthItemString] as Sprite;

        specialItemImage = transform.parent.Find("profile_image_parent").transform.Find("Writer_specialItem").GetComponent<Image>();
        specialItemImage.sprite = ItemManager.Instance.specialItemsDic[writer_specialItemString] as Sprite;

    }



}
