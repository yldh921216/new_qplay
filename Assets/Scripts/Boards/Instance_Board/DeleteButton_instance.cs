﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using EZObjectPools;


public class DeleteButton_instance : MonoBehaviour
{
    GameObject confirmPanel;
    Instance_button instanceScript;
    ConfirmPanel confirmScript;
    Button deleteButton;

    private void OnEnable()
    {
        deleteButton = GetComponent<Button>();
        confirmPanel = GameObject.Find("LobbyCanvas").transform.Find("ConfirmPanel").gameObject;
        confirmScript = confirmPanel.GetComponent<ConfirmPanel>();
        instanceScript = GetComponentInParent<Instance_button>();
        deleteButton.onClick.AddListener(() => DeleteButtonClick());
    }
     void  OnDisable()
    {
        deleteButton.onClick.RemoveAllListeners();
    }

    private void DeleteButtonClick() 
    {
        confirmScript.deleteProgress = true;
        confirmPanel.transform.Find("WarningText").GetComponent<Text>().text = "You want to delete?";       
        confirmScript.instanceScript = instanceScript;
        confirmPanel.SetActive(true);
    }


   

}
