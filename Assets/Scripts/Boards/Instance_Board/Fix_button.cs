﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fix_button : MonoBehaviour
{

    GameObject confirmPanel;
    ConfirmPanel confirmScript;
    Instance_button instanceScript;


    private void OnEnable()
    {
        confirmPanel = GameObject.Find("LobbyCanvas").transform.Find("ConfirmPanel").gameObject;
        confirmScript = confirmPanel.GetComponent<ConfirmPanel>();
        instanceScript = GetComponentInParent<Instance_button>();
        Button fixButton = GetComponent<Button>();
        fixButton.onClick.AddListener(() => FixButtonClick());
    }

    private void FixButtonClick()
    {
        confirmScript.fixProgress = true;
        confirmPanel.SetActive(true);
        confirmPanel.transform.Find("WarningText").GetComponent<Text>().text = "You want to Fix it?";
        confirmScript.instanceScript = instanceScript;
    }




}
