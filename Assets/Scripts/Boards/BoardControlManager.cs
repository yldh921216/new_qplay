﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoardControlManager : MonoBehaviour
{
    
    WriteNickName writeNicknameScript;
    DressManager dressRoomScript;

    [Header("Bottom Menu")]
    public GameObject uiButtons;
    public Button characterMenuButton;
    public Button bulletinBoardMenuButton;
    public Button storeMenuButton;
    public Button dressRoomButton;
    public Sprite originalSprite;
    public Sprite selectedImageSprite;
    public Button[] menuButtons;


    [Header ("Panels")]
    public GameObject characterPanel;
    public GameObject bulletinBoardPanel;
    public GameObject nicknameRegisterPanel;
    public GameObject characterDressRoomPanel;
    public GameObject storePanel;
    public  GameObject writtenBoard;
    public GameObject[] panels;


    [Header("Character Panel")]
    public Button DressRoomButton;


    [Header ("Bulletin Boards")]
    public GameObject writingPanel;
    public Button newWritingActiveButton;
    public Image newWritingActiveImage;


    [Header("Gold Store Panel")]
    public GameObject goldStorePanel;
    public GameObject packagePanel;
    public GameObject successPopUpPanel;

    StoreManager storeManager;
    public GameObject waitingForUpdate;


    [Header("Warning")]
    public GameObject warningContent;
    public GameObject warningTitle;



    private void Awake()
    {
        dressRoomScript = FindObjectOfType<DressManager>();
        storeManager = FindObjectOfType<StoreManager>();
        writeNicknameScript = FindObjectOfType<WriteNickName>();
    }
    private void Start()
    {   
        StartCoroutine(ShowFirstView());  
    }


    public IEnumerator ShowFirstView ()
    {
        // Upload Global Event etc.. 
        while (!writeNicknameScript.nicknameIsExisted) yield return null;

            ClosePanels();
            bulletinBoardPanel.SetActive(true);
       
    }
    public void CharacterMenuButton()
    {
        if (characterPanel.activeSelf && !bulletinBoardPanel.activeInHierarchy && !writtenBoard.activeInHierarchy && !writingPanel.activeInHierarchy && !storePanel.activeInHierarchy) return; 
        else
        {
            ClosePanels();        
            characterPanel.SetActive(true);
            ButtonColorChange();
            
        }
    }
    public void BulletinBoardMenuButton()
    {
        if (bulletinBoardPanel.activeSelf && !characterPanel.activeInHierarchy && !writtenBoard.activeInHierarchy && !writingPanel.activeInHierarchy && !storePanel.activeInHierarchy) return;
        else
        {
            ClosePanels();
            bulletinBoardPanel.SetActive(true);
            ButtonColorChange();
        }
    }
    public void StoreRoomMenuButton() 
    {
        if (storePanel.activeSelf && !characterPanel.activeInHierarchy && !writtenBoard.activeInHierarchy && !writingPanel.activeInHierarchy && !bulletinBoardPanel.activeInHierarchy) return;
        else
        {
            ClosePanels();
            storePanel.SetActive(true);
            ButtonColorChange();
        }

    }
  
    public void PlaygroundMenuButton() 
    {
      
        
    }

    public void WaitingForUpdate()
    {
        StartCoroutine(storeManager.PopUpPanel(waitingForUpdate));
    }

    public void WarningBlankContent()
    {
        StartCoroutine(storeManager.PopUpPanel(warningContent));
    }

    public void WarningBlankTitle()
    {
        StartCoroutine(storeManager.PopUpPanel(warningTitle));
    }

    public void WarningBoth()
    {
        StartCoroutine(storeManager.PopUpPanel(warningTitle));
        StartCoroutine(storeManager.PopUpPanel(warningContent));
    }






    public void ButtonColorChange()
    {
        if (characterPanel.activeSelf)
        {
            for ( int x = 0; x <menuButtons.Length; x++)
            {
                if (menuButtons[x].image.sprite != originalSprite)
                    menuButtons[x].image.sprite = originalSprite;
            }
            characterMenuButton.image.sprite = selectedImageSprite;
        }
       else if(storePanel.activeSelf)
        {
            for (int x = 0; x < menuButtons.Length; x++)
            {
                if(menuButtons[x].image.sprite != originalSprite)
                menuButtons[x].image.sprite = originalSprite;
            }
            storeMenuButton.image.sprite = selectedImageSprite;
        }
        else if (bulletinBoardPanel.activeSelf)
        {
            for (int x = 0; x < menuButtons.Length; x++)
            {
                if (menuButtons[x].image.sprite != originalSprite)
                    menuButtons[x].image.sprite = originalSprite;
            }
            bulletinBoardMenuButton.image.sprite = selectedImageSprite;
        }
        //else if (infoPanel)
        // else if (PlayPanel)
    }




    //////// Global Button
    public void ClosePanels()
    {     
        for (int x = 0; x < panels.Length; x++)
        {   
            if (panels[x].gameObject.activeInHierarchy)
                {
                   if(panels[x] == storePanel)
                   {
                      storeManager.storePanel_baseImage.sprite = ItemManager.Instance.baseCharacterSpriteDic[dressRoomScript.last_MyBaseCharacterString] as Sprite;
                      storeManager.storePanel_backgroundImage.sprite = ItemManager.Instance.backgroundItemDic[dressRoomScript.last_MyBackgroundItemString] as Sprite;
                      storeManager.storePanel_backImage.sprite = ItemManager.Instance.backItemDic[dressRoomScript.last_MyBackItemString] as Sprite;
                      storeManager.storePanel_bodyImage.sprite = ItemManager.Instance.bodyItemsDic[dressRoomScript.last_MyBodyItemString] as Sprite;
                      storeManager.storePanel_hairImage.sprite = ItemManager.Instance.hairItemsDic[dressRoomScript.last_MyHairItemString] as Sprite;
                      storeManager.storePanel_eyesImage.sprite = ItemManager.Instance.eyesItemsDic[dressRoomScript.last_MyEyesItemString] as Sprite;
                      storeManager.storePanel_earsImage.sprite = ItemManager.Instance.earsItemsDic[dressRoomScript.last_MyEarsItemString] as Sprite;
                      storeManager.storePanel_mouthImage.sprite = ItemManager.Instance.mouthItemsDic[dressRoomScript.last_MyMouthItemString] as Sprite;
                      storeManager.storePanel_specialImage.sprite = ItemManager.Instance.specialItemsDic[dressRoomScript.last_MySpecialItemString] as Sprite;
                 
                    for ( int y = 0;  y < storeManager.glows.Length; y++)
                    {
                        storeManager.glows[y].SetActive(false);
                    }
                     
                
                
                }

                  if(panels[x] == writtenBoard)
                   {
                      DataBaseManager.Instance.writtenBoardScript.currentWrittenBoard_uniqueKey = null;
                   } 
                    panels[x].gameObject.SetActive(false);
                   
                }          
        }
    }
    public void OnClickDressRoomButton() 
    {
        ClosePanels();
        characterDressRoomPanel.SetActive(true);
        uiButtons.SetActive(false);
       
    }
    public void ExitButtonOfDressRoomPanel()
    {
        characterDressRoomPanel.SetActive(false);
        characterPanel.SetActive(true);
        uiButtons.SetActive(true);
        ButtonColorChange();

        
        dressRoomScript.dressRoom_backgroundImage.sprite = ItemManager.Instance.backgroundItemDic[dressRoomScript.last_MyBackgroundItemString] as Sprite;
        dressRoomScript.dressRoom_baseCharacterImage.sprite =  ItemManager.Instance.baseCharacterSpriteDic[dressRoomScript.last_MyBaseCharacterString] as Sprite;
         dressRoomScript.dressRoom_backImage.sprite =  ItemManager.Instance.backItemDic[dressRoomScript.last_MyBackItemString] as Sprite;
        dressRoomScript.dressRoom_bodyImage.sprite =  ItemManager.Instance.bodyItemsDic[dressRoomScript.last_MyBodyItemString] as Sprite;
         dressRoomScript.dressRoom_hairImage.sprite = ItemManager.Instance.hairItemsDic[dressRoomScript.last_MyHairItemString] as Sprite;
       dressRoomScript.dressRoom_eyesImage.sprite =  ItemManager.Instance.eyesItemsDic[dressRoomScript.last_MyEyesItemString] as Sprite;
        dressRoomScript.dressRoom_earsImage.sprite =  ItemManager.Instance.earsItemsDic[dressRoomScript.last_MyEarsItemString] as Sprite;
         dressRoomScript.dressRoom_mouthImage.sprite =   ItemManager.Instance.mouthItemsDic[dressRoomScript.last_MyMouthItemString] as Sprite;
       dressRoomScript.dressRoom_specialImage.sprite = ItemManager.Instance.specialItemsDic[dressRoomScript.last_MySpecialItemString] as Sprite;

        for( int x=0; x<dressRoomScript.glows.Length; x++)
        {
            dressRoomScript.glows[x].SetActive(false);
        }

    }
    public void OnClickStoreButtonInDressRoomPanel()
    {
        characterDressRoomPanel.SetActive(false);
        storePanel.SetActive(true);
        uiButtons.SetActive(true);
        ButtonColorChange();
    }

   

   //////// Bulletin Board Menu  
    public void NewWriteInputActiveButton()
    {
        if(!writingPanel.activeInHierarchy)  writingPanel.SetActive(true);
    }    
    public void CommentInputActiveButton() { }
    public void ExitToBoardPanelButton ()
    {
        ClosePanels();
        bulletinBoardPanel.SetActive(true);     
    }

   

  
    //////// Loading Panel ---> Whenever Client wait getting data from database , show loading Panel ;
    public void ShowLoadingPanel () { }
    


    public void OpenGoldStorePanel ()
    {
        goldStorePanel.SetActive(true);
    }
    public void OpenPackagePanel()
    {
        packagePanel.SetActive(true);
    }

    public void ExitGoldStorePanel ()
    {
        goldStorePanel.SetActive(false);
    }

    public void ExitPackagePanel()
    {
        packagePanel.SetActive(false);
    }



   


}
