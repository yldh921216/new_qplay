﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Firebase.Extensions;
using Firebase.Database;
using UnityEngine.EventSystems;
using DG.Tweening;

public class StoreManager : MonoBehaviour
{
    [Header("Script")]
    DressManager dressManagerScript;

    [Header("StorePanel CharacterImages")]
    public Image storePanel_baseImage;
    public Image storePanel_hairImage;
    public Image storePanel_bodyImage;
    public Image storePanel_eyesImage;
    public Image storePanel_backImage;
    public Image storePanel_earsImage;
    public Image storePanel_mouthImage;
    public Image storePanel_specialImage;
    public Image storePanel_backgroundImage;

    [Header ("Selected Slot")]
    public string selectedItemKey;
    public string selectedItemCode;
    public int selectedItemGoldPrice;
    public int selectedItemGemPrice;

    [Header("GlowEffect")]
    public GameObject glowEffectToHairSlot;
    public GameObject glowEffectToBaseSlot;
    public GameObject glowEffectToBodySlot;
    public GameObject glowEffectToEyesSlot;
    public GameObject glowEffectToEarsSlot;
    public GameObject glowEffectToMouthSlot;
    public GameObject glowEffectToSpecialSlot;
    public GameObject glowEffectToBackSlot;
    public GameObject glowEffectToBackgroundSlot;
    public GameObject[] glows;

    [Header("Confirm")]
    public bool confirm = false;
    public bool purchasingIsDone = false;
    public bool NowEquipConfirming = false;
    public bool SetItemNow = false;
    public GameObject confirmPanel;
    public GameObject youWantToEquipNowPanel;

    [Header("Plastic Surgery")]
    public GameObject plasticSurgeryPanel;


    [Header("Store View")]
    public GameObject bodyStoreView;
    public GameObject hairStoreView;
    public GameObject accessoryStoreView;
    public GameObject backgroundStoreView;
    public GameObject specialStoreView;
    public GameObject baseView;
    public GameObject[] views;

    [Header("Store Tap")]
    public Button[] tapButtons;
    public Image tapSelectFocusImage;

    [Header("Warning Panel")]
    public GameObject notEnoughGoldPanel;
    public GameObject alreadyHaveItPanel;



    public class StoreItemData
    {
        public string itemKeyValue;
        public string itemCodeValue;
        public int itemGoldPriceValue;
        public int itemGemPriceValue;
        public string itemTypeValue; 
    }


    private void Awake()
    {
        dressManagerScript = FindObjectOfType<DressManager>();
    }

    private void OnDisable()    ////// Be careful
    {    // Equipt Last string
        //storePanel_hairImage.sprite = ItemManager.Instance.hairItemsDic[dressManagerScript.last_MyHairItemString] as Sprite;
        //storePanel_bodyImage.sprite = ItemManager.Instance.bodyItemsDic[dressManagerScript.last_MyBodyItemString] as Sprite;
        //storePanel_eyesImage.sprite = ItemManager.Instance.eyesItemsDic[dressManagerScript.last_MyEyesItemString] as Sprite;
        //storePanel_earsImage.sprite = ItemManager.Instance.earsItemsDic[dressManagerScript.last_MyEarsItemString] as Sprite;
        //storePanel_mouthImage.sprite = ItemManager.Instance.mouthItemsDic[dressManagerScript.last_MyMouthItemString] as Sprite;
        //storePanel_specialImage.sprite = ItemManager.Instance.specialItemsDic[dressManagerScript.last_MySpecialItemString] as Sprite;
        //storePanel_backgroundImage.sprite = ItemManager.Instance.backgroundItemDic[dressManagerScript.last_MyBackgroundItemString] as Sprite;
        //storePanel_backImage.sprite = ItemManager.Instance.backItemDic[dressManagerScript.last_MyBackItemString] as Sprite;
    }

   
    public void OnClickStoreItemSlot ()
    {
        GameObject storeItemSlot = EventSystem.current.currentSelectedGameObject;
        StoreSlot_itemScript storeItemScript = storeItemSlot.GetComponent<StoreSlot_itemScript>();
        
        if(selectedItemCode == storeItemScript.itemCode)  
        {    
            if (selectedItemKey == "Hair")
            {
                glowEffectToHairSlot.SetActive(false);
                storePanel_hairImage.sprite = ItemManager.Instance.hairItemsDic[dressManagerScript.last_MyHairItemString] as Sprite;
                selectedItemCode = null;
            }
            else if (selectedItemKey == "Body")
            {
                glowEffectToBodySlot.SetActive(false);
                storePanel_bodyImage.sprite = ItemManager.Instance.bodyItemsDic[dressManagerScript.last_MyBodyItemString] as Sprite;
                selectedItemCode = null;
            }
            else if (selectedItemKey == "Eyes")
            {
                glowEffectToEyesSlot.SetActive(false);
                storePanel_eyesImage.sprite = ItemManager.Instance.eyesItemsDic[dressManagerScript.last_MyEyesItemString] as Sprite;
                selectedItemCode = null;
            }
            else if (selectedItemKey == "Ears")
            {
                glowEffectToEarsSlot.SetActive(false);
                storePanel_earsImage.sprite = ItemManager.Instance.earsItemsDic[dressManagerScript.last_MyEarsItemString] as Sprite;
                selectedItemCode = null;
            }
            else if (selectedItemKey == "Mouth")
            {
                glowEffectToMouthSlot.SetActive(false);
                storePanel_mouthImage.sprite = ItemManager.Instance.mouthItemsDic[dressManagerScript.last_MyMouthItemString] as Sprite;
                selectedItemCode = null;
            }
            else if (selectedItemKey == "Special")
            {
                glowEffectToSpecialSlot.SetActive(false);
                storePanel_specialImage.sprite = ItemManager.Instance.specialItemsDic[dressManagerScript.last_MySpecialItemString] as Sprite;
                selectedItemCode = null;
            }
            else if (selectedItemKey == "Back")
            {
                glowEffectToBackSlot.SetActive(false);
                print(dressManagerScript.last_MyBackItemString);
                storePanel_backImage.sprite = ItemManager.Instance.backItemDic[dressManagerScript.last_MyBackItemString] as Sprite;
                selectedItemCode = null;
            }
            else if (selectedItemKey == "Background")
            {
                glowEffectToBackgroundSlot.SetActive(false);
                storePanel_backgroundImage.sprite = ItemManager.Instance.backgroundItemDic[dressManagerScript.last_MyBackgroundItemString] as Sprite;
                selectedItemCode = null;
            }
            else if ( selectedItemKey == "Base")
            {
                glowEffectToBaseSlot.SetActive(false);
                storePanel_baseImage.sprite = ItemManager.Instance.baseCharacterSpriteDic[dressManagerScript.last_MyBaseCharacterString] as Sprite;
                selectedItemCode = null;
            }

            return;
        }

        selectedItemKey = storeItemScript.itemKey;
        selectedItemCode = storeItemScript.itemCode;
        selectedItemGoldPrice = storeItemScript.itemGoldPrice;
        selectedItemGemPrice = storeItemScript.itemGemPrice; 
        
        if(selectedItemKey == "Hair")
        {
            glowEffectToHairSlot.SetActive(true);
            glowEffectToHairSlot.transform.position = storeItemSlot.gameObject.transform.position;
            glowEffectToHairSlot.transform.parent = storeItemSlot.transform;
            storePanel_hairImage.sprite = ItemManager.Instance.hairItemsDic[selectedItemCode] as Sprite;
        }
        else if( selectedItemKey == "Base")
        {
            glowEffectToBaseSlot.SetActive(true);
            glowEffectToBaseSlot.transform.position = storeItemSlot.gameObject.transform.position;
            glowEffectToBaseSlot.transform.parent = storeItemSlot.transform;
            storePanel_baseImage.sprite = ItemManager.Instance.baseCharacterSpriteDic[selectedItemCode] as Sprite;

        }
        else if ( selectedItemKey == "Body")
        {
            glowEffectToBodySlot.SetActive(true);
            glowEffectToBodySlot.transform.position = storeItemSlot.gameObject.transform.position;
            glowEffectToBodySlot.transform.parent = storeItemSlot.transform;
            storePanel_bodyImage.sprite = ItemManager.Instance.bodyItemsDic[selectedItemCode] as Sprite;
        }
        else if ( selectedItemKey == "Eyes")
        {
            glowEffectToEyesSlot.SetActive(true);
            glowEffectToEyesSlot.transform.position = storeItemSlot.gameObject.transform.position;
            glowEffectToEyesSlot.transform.parent = storeItemSlot.transform;
            storePanel_eyesImage.sprite = ItemManager.Instance.eyesItemsDic[selectedItemCode] as Sprite;
        }
        else if ( selectedItemKey =="Ears")
        {
            glowEffectToEarsSlot.SetActive(true);
            glowEffectToEarsSlot.transform.position = storeItemSlot.gameObject.transform.position;
            glowEffectToEarsSlot.transform.parent = storeItemSlot.transform;
            storePanel_earsImage.sprite = ItemManager.Instance.earsItemsDic[selectedItemCode] as Sprite;
        }
        else if (selectedItemKey == "Mouth")
        {
            glowEffectToMouthSlot.SetActive(true);
            glowEffectToMouthSlot.transform.position = storeItemSlot.gameObject.transform.position;
            glowEffectToMouthSlot.transform.parent = storeItemSlot.transform;
            storePanel_mouthImage.sprite = ItemManager.Instance.mouthItemsDic[selectedItemCode] as Sprite;
        }
        else if ( selectedItemKey =="Special")
        {
            glowEffectToSpecialSlot.SetActive(true);
            glowEffectToSpecialSlot.transform.position = storeItemScript.gameObject.transform.position;
            glowEffectToSpecialSlot.transform.parent = storeItemSlot.transform;
            storePanel_specialImage.sprite = ItemManager.Instance.specialItemsDic[selectedItemCode] as Sprite;
        }
        else if ( selectedItemKey == "Back")
        {
            glowEffectToBackSlot.SetActive(true);
            glowEffectToBackgroundSlot.transform.position = storeItemScript.gameObject.transform.position;
            glowEffectToBackSlot.transform.parent = storeItemSlot.transform;
            storePanel_backImage.sprite = ItemManager.Instance.backItemDic[selectedItemCode] as Sprite;
        }
        else if (selectedItemKey =="Background")
        {
            glowEffectToBackgroundSlot.SetActive(true);
            glowEffectToBackgroundSlot.transform.position = storeItemScript.gameObject.transform.position;
            glowEffectToBackgroundSlot.transform.parent = storeItemSlot.transform;
            storePanel_backgroundImage.sprite = ItemManager.Instance.backgroundItemDic[selectedItemCode] as Sprite;
        }

    }
    public void OnClickYesButton() 
    {
        confirm = true; 
        confirmPanel.SetActive(false);
    }
    public void OnClickNoButton()
    {
        StopAllCoroutines();
        confirmPanel.SetActive(false);
        confirm = false;
    }
    public void OnClickPurchaseButton() 
    {
        confirmPanel.SetActive(true);
        StartCoroutine(PurchasingItem()); 
        
    }
    IEnumerator PurchasingItem()
    {
        Button buyButton = EventSystem.current.currentSelectedGameObject.GetComponent<Button>();
        StoreSlot_itemScript buyItemScript = buyButton.transform.GetComponentInParent<StoreSlot_itemScript>();

        print(buyItemScript);
        while (confirm == false)
        {
            yield return null;
        }

        confirm = false;

        if (buyItemScript.itemGoldPrice > DataBaseManager.Instance.currentGoldCount || buyItemScript.itemGemPrice > DataBaseManager.Instance.currentGemCount)
        {
            StartCoroutine( PopUpPanel(notEnoughGoldPanel));
            yield break;
        }

        if (dressManagerScript.inventoryItems.Contains(buyItemScript.itemCode))
        {
            StartCoroutine(PopUpPanel(alreadyHaveItPanel));
            yield break;
        }

        PopUpEquipNowPanel();
        while (!NowEquipConfirming)
        {
            yield return null;
        }

        NowEquipConfirming = false;


        print(buyItemScript.itemType);

        if (buyItemScript.itemType == "Unique")
        {
            ////// becareful when use the mutableData and ChildAdded Listener -----> 
            DataBaseManager.Instance.databaseReference.Child("Items").Child("Unique").Child(buyItemScript.itemCode)
                .RunTransaction(mutableData =>
                {
                    int count = 0;
                    int.TryParse(mutableData.Child("ItemCount").Value.ToString(), out int result);
                    count = result;

                    if (count == 0)
                    {
                        Debug.LogError("All Solded");
                        return TransactionResult.Abort();
                    }

                    Dictionary<string, object> itemCount = new Dictionary<string, object>();
                    itemCount["ItemCount"] = count - 1;

                    mutableData.Value = itemCount;

                    return TransactionResult.Success(mutableData);

                }).ContinueWithOnMainThread(Task =>
                    {
                        if (Task.IsCompleted)
                        {
                            StoreItemData itemData = new StoreItemData();
                            itemData.itemKeyValue = buyItemScript.itemKey;
                            itemData.itemCodeValue = buyItemScript.itemCode;
                            itemData.itemGoldPriceValue = buyItemScript.itemGoldPrice;
                            itemData.itemGemPriceValue = buyItemScript.itemGemPrice;
                            itemData.itemTypeValue = buyItemScript.itemType;
                            string itemJson = JsonUtility.ToJson(itemData);

                            DataBaseManager.Instance.databaseReference.Child("User").Child(GoogleSignInDemo.Instance.firebaseUser.UserId).Child("Inventory")
                            .Child(buyItemScript.itemKey).Child(buyItemScript.itemCode).SetRawJsonValueAsync(itemJson).ContinueWithOnMainThread(task =>
                            {
                                if (task.IsCompleted)
                                {
                                    Debug.LogError("Purchasing Unique Item");
                                    DataBaseManager.Instance.currentGoldCount -= buyItemScript.itemGoldPrice;
                                    DataBaseManager.Instance.currentGemCount -= buyItemScript.itemGemPrice;
                                    purchasingIsDone = true;
                                }
                            });
                        }
                    });
        }

        else
        {
            print("normal");

            StoreItemData itemData = new StoreItemData();
            itemData.itemKeyValue = buyItemScript.itemKey;
            itemData.itemCodeValue = buyItemScript.itemCode;
            itemData.itemGoldPriceValue = buyItemScript.itemGoldPrice;
            itemData.itemGemPriceValue = buyItemScript.itemGemPrice;
            itemData.itemTypeValue = buyItemScript.itemType;
            string itemJson = JsonUtility.ToJson(itemData);

            GoogleSignInDemo.Instance.databaseRef.Child("User").Child(GoogleSignInDemo.Instance.firebaseUser.UserId).Child("Inventory").Child("Items")
            .Child(buyItemScript.itemKey).Child(buyItemScript.itemCode).SetRawJsonValueAsync(itemJson).ContinueWithOnMainThread(task =>
            {
                if (task.IsCompleted)
                {
                    dressManagerScript.inventoryItems.Add(buyItemScript.itemCode);
                    purchasingIsDone = true;

                }
            });

            if (SetItemNow)
            {
                if( buyItemScript.itemKey == "Base")
                {
                    GoogleSignInDemo.Instance.databaseRef.Child("User").Child(GoogleSignInDemo.Instance.firebaseUser.UserId).Child("Inventory").Child("Equipment")
                    .Child("EquippedBaseCharacterCode").SetValueAsync(buyItemScript.itemCode);
                    dressManagerScript.last_MyBaseCharacterString = buyItemScript.itemCode;

                }
                else if (buyItemScript.itemKey == "Back")
                {
                    GoogleSignInDemo.Instance.databaseRef.Child("User").Child(GoogleSignInDemo.Instance.firebaseUser.UserId).Child("Inventory").Child("Equipment")
                    .Child("EquippedBackItem").SetValueAsync(buyItemScript.itemCode);
                    dressManagerScript.last_MyBackItemString = buyItemScript.itemCode;
                }
                else if (buyItemScript.itemKey == "Background")
                {
                    GoogleSignInDemo.Instance.databaseRef.Child("User").Child(GoogleSignInDemo.Instance.firebaseUser.UserId).Child("Inventory").Child("Equipment")
                    .Child("EquippedBackgroundItem").SetValueAsync(buyItemScript.itemCode);
                    dressManagerScript.last_MyBackgroundItemString = buyItemScript.itemCode;

                }
                else if (buyItemScript.itemKey == "Body")
                {
                    GoogleSignInDemo.Instance.databaseRef.Child("User").Child(GoogleSignInDemo.Instance.firebaseUser.UserId).Child("Inventory").Child("Equipment")
                    .Child("EquippedBodyItem").SetValueAsync(buyItemScript.itemCode);
                    dressManagerScript.last_MyBodyItemString = buyItemScript.itemCode;
                }
                else if (buyItemScript.itemKey == "Hair")
                {
                    GoogleSignInDemo.Instance.databaseRef.Child("User").Child(GoogleSignInDemo.Instance.firebaseUser.UserId).Child("Inventory").Child("Equipment")
                    .Child("EquippedHairItem").SetValueAsync(buyItemScript.itemCode);
                    dressManagerScript.last_MyHairItemString = buyItemScript.itemCode;
                }
                else if (buyItemScript.itemKey == "Eyes")
                {
                    GoogleSignInDemo.Instance.databaseRef.Child("User").Child(GoogleSignInDemo.Instance.firebaseUser.UserId).Child("Inventory").Child("Equipment")
                   .Child("EquippedEyesItem").SetValueAsync(buyItemScript.itemCode);
                    dressManagerScript.last_MyEyesItemString = buyItemScript.itemCode;
                }
                else if (buyItemScript.itemKey == "Ears")
                {
                    GoogleSignInDemo.Instance.databaseRef.Child("User").Child(GoogleSignInDemo.Instance.firebaseUser.UserId).Child("Inventory").Child("Equipment")
                    .Child("EquippedEarsItem").SetValueAsync(buyItemScript.itemCode);
                    dressManagerScript.last_MyEarsItemString = buyItemScript.itemCode;
                }
                else if (buyItemScript.itemKey == "Mouth")
                {
                    GoogleSignInDemo.Instance.databaseRef.Child("User").Child(GoogleSignInDemo.Instance.firebaseUser.UserId).Child("Inventory").Child("Equipment")
                   .Child("EquippedMouthItem").SetValueAsync(buyItemScript.itemCode);
                    dressManagerScript.last_MyMouthItemString = buyItemScript.itemCode;
                }
                else if (buyItemScript.itemKey == "Special")
                {
                    GoogleSignInDemo.Instance.databaseRef.Child("User").Child(GoogleSignInDemo.Instance.firebaseUser.UserId).Child("Inventory").Child("Equipment")
                   .Child("EquippedSpecialItem").SetValueAsync(buyItemScript.itemCode);
                    dressManagerScript.last_MySpecialItemString = buyItemScript.itemCode;
                }
            }

            while (!purchasingIsDone)
            {              
                yield return null;
            }

            DataBaseManager.Instance.currentGemCount -= buyItemScript.itemGemPrice;
            DataBaseManager.Instance.currentGoldCount -= buyItemScript.itemGoldPrice;


            print(DataBaseManager.Instance.currentGoldCount);
            DataBaseManager.Instance.SetGold();
            print(DataBaseManager.Instance.currentGoldCount);





            purchasingIsDone = false;
            confirm = false;
            SetItemNow = false;

        }
    }

    public void Purchasing_1000w_complete()
    {
        DataBaseManager.Instance.currentGoldCount += 3000;
        DataBaseManager.Instance.boardControlScript.goldStorePanel.SetActive(false);
        StartCoroutine(PopUpPanel(DataBaseManager.Instance.boardControlScript.successPopUpPanel));
        DataBaseManager.Instance.SetGold();
        DataBaseManager.Instance.goldBombCount = 100;
        DataBaseManager.Instance.GoldBomb(transform.position);
     }
    public void Purchasing_1500w_complete()
    {
        DataBaseManager.Instance.currentGoldCount += 5000;
        DataBaseManager.Instance.boardControlScript.goldStorePanel.SetActive(false);
        StartCoroutine(PopUpPanel(DataBaseManager.Instance.boardControlScript.successPopUpPanel));
        DataBaseManager.Instance.SetGold();
        DataBaseManager.Instance.goldBombCount = 100;
        StartCoroutine(DataBaseManager.Instance.GoldBomb(transform.position));
    }
    public void Purchasing_2500w_complete()
    {
        DataBaseManager.Instance.currentGoldCount += 9000;
        DataBaseManager.Instance.boardControlScript.goldStorePanel.SetActive(false);
        StartCoroutine(PopUpPanel(DataBaseManager.Instance.boardControlScript.successPopUpPanel));
        DataBaseManager.Instance.SetGold();
        DataBaseManager.Instance.goldBombCount = 100;
        StartCoroutine(DataBaseManager.Instance.GoldBomb(transform.position));
    }
    public void Purchasing_3333w_complete()
    {
        DataBaseManager.Instance.currentGoldCount += 12000;
        DataBaseManager.Instance.boardControlScript.goldStorePanel.SetActive(false);
        StartCoroutine(PopUpPanel(DataBaseManager.Instance.boardControlScript.successPopUpPanel));
        DataBaseManager.Instance.SetGold();
        DataBaseManager.Instance.goldBombCount = 100;
        StartCoroutine(DataBaseManager.Instance.GoldBomb(transform.position));
    }
    public void Purchasing_5000w_complete()
    {
        DataBaseManager.Instance.currentGoldCount += 20000;
        DataBaseManager.Instance.boardControlScript.goldStorePanel.SetActive(false);
        StartCoroutine(PopUpPanel(DataBaseManager.Instance.boardControlScript.successPopUpPanel));
        DataBaseManager.Instance.SetGold();
        DataBaseManager.Instance.goldBombCount = 100;
        StartCoroutine(DataBaseManager.Instance.GoldBomb(transform.position));
    }
    public void Purchasing_10000w_complete()
    {
        DataBaseManager.Instance.currentGoldCount += 50000;
        DataBaseManager.Instance.boardControlScript.goldStorePanel.SetActive(false);
        StartCoroutine(PopUpPanel(DataBaseManager.Instance.boardControlScript.successPopUpPanel));
        DataBaseManager.Instance.SetGold();
        DataBaseManager.Instance.goldBombCount = 100;
        StartCoroutine(DataBaseManager.Instance.GoldBomb(transform.position));
    }
    public void Purchasing_packageProduct_complete()
    {
        DataBaseManager.Instance.currentGoldCount += 150000;
        DataBaseManager.Instance.boardControlScript.packagePanel.SetActive(false);
          StartCoroutine(PopUpPanel(DataBaseManager.Instance.boardControlScript.successPopUpPanel));
        DataBaseManager.Instance.SetGold();
        DataBaseManager.Instance.goldBombCount = 100;
        StartCoroutine(DataBaseManager.Instance.GoldBomb(transform.position));
    }


    public IEnumerator PopUpPanel(GameObject panel)
    {
        panel.SetActive(true);
        panel.transform.DOShakePosition(0.5f, 50);
        yield return new WaitForSeconds(2f);
        panel.SetActive(false);
    }







    public void PopUpEquipNowPanel()
    {
        youWantToEquipNowPanel.SetActive(true);
    }
    public void OnClickYesNowEquipButton()
    {
        NowEquipConfirming = true;
        SetItemNow = true;
        confirm = false;
        youWantToEquipNowPanel.SetActive(false);

     }
    public void OnClickNoNowEquipButton()
    {
        SetItemNow = false;
        confirm = false;
        youWantToEquipNowPanel.SetActive(false);
        StopAllCoroutines();
    }
    public void OnClickPlasticSurgeryButton()
    {
        plasticSurgeryPanel.SetActive(true);
    }

    public void CloseViews()
    {
        for (int x = 0; x < views.Length; x++)
        {
            if (views[x].activeInHierarchy)
                views[x].SetActive(false);
        }
    }
    public void ChangeTapButtonColorAndFocus()
    {
        for (int x = 0; x < tapButtons.Length; x++)
        {
            if (tapButtons[x].gameObject == EventSystem.current.currentSelectedGameObject)
            {
                tapButtons[x].image.color = new Color(1, 1, 1);
                tapSelectFocusImage.transform.position = new Vector3(tapButtons[x].transform.position.x, tapSelectFocusImage.transform.position.y, 0);
            }
            else
            {
                tapButtons[x].image.color = new Color(0.2901f, 0.6745f, 0.9686f, 1);
            }
        }

    }
    public void OnClickBodyTagButton()
    {
        CloseViews();
        bodyStoreView.SetActive(true);
        ChangeTapButtonColorAndFocus();
    }
    public void OnClickHairTagButton()
    {
        CloseViews();
        hairStoreView.SetActive(true);
        ChangeTapButtonColorAndFocus();
    }
    public void OnClickAccessoryButton()
    {
        CloseViews();
        accessoryStoreView.SetActive(true);
        ChangeTapButtonColorAndFocus();
    } 
    public void OnClickBackgroundButton()
    {
        CloseViews();
        backgroundStoreView.SetActive(true);
        ChangeTapButtonColorAndFocus();
    }
    public void OnClickSpecialButton()
    {
        CloseViews();
        specialStoreView.SetActive(true);
        ChangeTapButtonColorAndFocus();
    }
    public void OnClickBaseCharacterButton()
    {
        CloseViews();
        baseView.SetActive(true);
        ChangeTapButtonColorAndFocus();
    }

}
