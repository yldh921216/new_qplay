﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemManager : MonoBehaviour
{
    public static ItemManager _instance;
    public static ItemManager Instance
    {
        get
        {
            if (!_instance)
            {
                _instance = FindObjectOfType<ItemManager>();

                if (_instance == null)
                {
                    GameObject container = new GameObject();
                    container.name = "ItemManager";
                    _instance = container.AddComponent<ItemManager>();
                }
            }
            return _instance;
        }
    }


    [Header("Background_items")]
    public List<Sprite> backgroundSpriteList = new List<Sprite>();
    public Dictionary<string, object> backgroundItemDic = new Dictionary<string, object>();
    public List<Sprite> backgroundIconSpriteList = new List<Sprite>();
    public Dictionary<string, object> backgroundItemIconDic = new Dictionary<string, object>();
    public List<Sprite> backgroundRankSpriteList = new List<Sprite>();
    public Dictionary<string, object> backgroundRankDic = new Dictionary<string, object>();

    [Header("Back_items")]
    public List<Sprite> backSpriteList = new List<Sprite>();
    public Dictionary<string, object> backItemDic = new Dictionary<string, object>();
    public List<Sprite> backIconSpirteList = new List<Sprite>();
    public Dictionary<string, object> backItemIconDic = new Dictionary<string, object>();
    public List<Sprite> backRankSpriteList = new List<Sprite>();
    public Dictionary<string, object> backRankDic = new Dictionary<string, object>();


    [Header("Base_character")]
    public List<Sprite> baseGirl_0_SpriteList = new List<Sprite>();
    public List<Sprite> baseGirl_1_SpriteList = new List<Sprite>();
    public List<Sprite> baseGirl_2_SpriteList = new List<Sprite>();

    public List<Sprite> baseBoy_0_SpriteList = new List<Sprite>();
    public List<Sprite> baseBoy_1_SpriteList = new List<Sprite>();
    public List<Sprite> baseBoy_2_SpriteList = new List<Sprite>();
    public Dictionary<string, object> baseCharacterSpriteDic = new Dictionary<string, object>();

    public List<Sprite> baseCharacter_girl0_IconList = new List<Sprite>();
    public List<Sprite> baseCharacter_boy0_IconList = new List<Sprite>();
    public Dictionary<string, object> baseCharacterIconDic = new Dictionary<string, object>();
    public List<Sprite> baseCharacterRankList = new List<Sprite>();
    public Dictionary<string, object> baseCharacterRankDic = new Dictionary<string, object>();
   



    [Header("Hair_items")]
    public List<Sprite> hairCommonSpriteList = new List<Sprite>();
    public List<Sprite> hairGirl_0_SpriteList = new List<Sprite>();
    public List<Sprite> hairGirl_1_SpriteList = new List<Sprite>();
    public List<Sprite> hairGirl_2_SpriteLiist = new List<Sprite>();
    public List<Sprite> hairBoy_0_SpriteList = new List<Sprite>();
    public Dictionary<string, object> hairItemsDic = new Dictionary<string, object>();

    public List<Sprite> hair_girl0_IconSpriteList = new List<Sprite>();
    public List<Sprite> hair_boy0_IconSpriteLIst = new List<Sprite>();
    public Dictionary<string, object> hairIconDic = new Dictionary<string, object>();
    public List<Sprite> hair_girl0_rankList = new List<Sprite>();
    public Dictionary<string, object> hairRankDic = new Dictionary<string, object>();

   
    [Header("Body_items")]
    public List<Sprite> bodyCommonSpriteList = new List<Sprite>();
    public List<Sprite> bodyGirl_0_SpriteList = new List<Sprite>();
    public List<Sprite> bodyBoy_0_SpriteList = new List<Sprite>();
    public Dictionary<string, object> bodyItemsDic = new Dictionary<string, object>();

    public List<Sprite> body_common_iconSpriteList = new List<Sprite>();
    public List<Sprite> body_girl0_IconSpriteList = new List<Sprite>();
    public List<Sprite> body_boy0_IconSpriteList = new List<Sprite>();
    public Dictionary<string, object> bodyIconDic = new Dictionary<string, object>();

    public List<Sprite> body_girl0_rankList = new List<Sprite>();
    public Dictionary<string, object> bodyRankDic = new Dictionary<string, object>();


    [Header("Eyes_items")]
    public List<Sprite> eyesCommonSpriteList = new List<Sprite>();
    public List<Sprite> eyesGirl_0_SpriteList = new List<Sprite>();
    public List<Sprite> eyesBoy_0_SpriteList = new List<Sprite>();
    public Dictionary<string, object> eyesItemsDic = new Dictionary<string, object>();

    public List<Sprite> eyes_common_IconSpriteList = new List<Sprite>();
    public Dictionary<string, object> eyesIconDic = new Dictionary<string, object>();


    [Header("Ears_items")]
    public List<Sprite> earsCommonSpriteList = new List<Sprite>();
    public Dictionary<string, object> earsItemsDic = new Dictionary<string, object>();

    public List<Sprite> ears_Common_IconSpriteList = new List<Sprite>();
    public Dictionary<string, object> earsIconDic = new Dictionary<string, object>();



    [Header("Mouth_items")]
    public List<Sprite> mouthCommonSpriteList = new List<Sprite>();
    public List<Sprite> mouthGirl_0_SpriteList = new List<Sprite>();
    public List<Sprite> mouthBoy_0_SpriteList = new List<Sprite>();
    public Dictionary<string, object> mouthItemsDic = new Dictionary<string, object>();

    public List<Sprite> mouth_common_iconSpriteList = new List<Sprite>();
    public Dictionary<string, object> mouthIconDic = new Dictionary<string, object>();

    public List<Sprite> mouth_girl0_rankList = new List<Sprite>();
    public Dictionary<string, object> mouthRankDic = new Dictionary<string, object>();
    
    [Header("Special_items")]
    public List<Sprite> specialSpriteList =new  List<Sprite>();
    public List<ParticleSystem> specialParticleList =new  List<ParticleSystem>();
    public Dictionary<string, object> specialItemsDic = new Dictionary<string, object>();

    public List<Sprite> specialIcon_sprite_List = new List<Sprite>();
    public Dictionary<string, object> specialIconDic = new Dictionary<string, object>();

    public List<Sprite> special_sprite_rankList = new List<Sprite>();
    public Dictionary<string, object> specialRankDic = new Dictionary<string, object>();

    //////// Price Dictionary
    public Dictionary<string, int> PriceOfAllItemsDic = new Dictionary<string, int>();

    /////// Utility
    public bool ItemSettingIsOver = false;


    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
       SetAllItemSpritesInDictionary();

    }
    public void UniqueItemSetting() { }

    public void SetAllItemSpritesInDictionary()
    {
        ////// background
        for (int x= 0; x < backgroundSpriteList.Count; x++ )
        {
            backgroundItemDic.Add($"Background_{x}", backgroundSpriteList[x]);
        }
        for(int x = 0; x < backgroundIconSpriteList.Count; x++)
        {
            backgroundItemIconDic.Add($"Background_{x}", backgroundIconSpriteList[x]);
        }
        for(int x=  0; x < backgroundRankSpriteList.Count; x++)
        {
            backgroundRankDic.Add($"Background_{x}",  backgroundRankSpriteList[x]);
        }



        //////// back
        for ( int x= 0; x< backSpriteList.Count; x++)
        {
            backItemDic.Add($"Back_{x}", backSpriteList[x]);
        }
        for (int x= 0; x< backIconSpirteList.Count; x++)
        {
            backItemIconDic.Add($"Back_{x}", backIconSpirteList[x]);
        }
        for(int x = 0; x<backRankSpriteList.Count; x++)
        {
            backRankDic.Add($"Back_{x}", backRankSpriteList[x]);
        }


        ////// base
        for (int x = 0; x< baseGirl_0_SpriteList.Count; x++)
        {
            baseCharacterSpriteDic.Add($"Base_girl0_{x}", baseGirl_0_SpriteList[x]);
        }
        for(int x=0; x< baseCharacter_girl0_IconList.Count; x++)
        {
            baseCharacterIconDic.Add($"Base_girl0_{x}", baseCharacter_girl0_IconList[x]);
        }
        for (int x = 0; x < baseCharacter_boy0_IconList.Count; x++)
        {
            baseCharacterIconDic.Add($"Base_boy0_{x}", baseCharacter_boy0_IconList[x]);
        }

        for(int x=0; x<baseCharacterRankList.Count; x++)
        {
            baseCharacterRankDic.Add($"Base_girl0_{x}", baseCharacterRankList[x]);
        }
               

        for (int x= 0; x <baseGirl_1_SpriteList.Count; x++)
        {
            baseCharacterSpriteDic.Add($"Base_girl1_{x}", baseGirl_1_SpriteList[x]);
        }
        for ( int x =0; x<baseGirl_2_SpriteList.Count; x++)
        {
            baseCharacterSpriteDic.Add($"Base_girl2_{x}", baseGirl_2_SpriteList[x]);
        }
        for ( int x =0; x< baseBoy_0_SpriteList.Count; x++)
        {
            baseCharacterSpriteDic.Add($"Base_boy0_{x}", baseBoy_0_SpriteList[x]);
        }
        for (int x =0; x< baseBoy_1_SpriteList.Count; x++  )
        {
            baseCharacterSpriteDic.Add($"Base_boy1_{x}", baseBoy_1_SpriteList[x]);
        }
        for ( int x = 0; x< baseBoy_2_SpriteList.Count; x++)
        {
            baseCharacterSpriteDic.Add($"Base_boy2_{x}", baseBoy_2_SpriteList[x]);
        }

        
        

        ////// hair
        for (int x = 0; x < hairCommonSpriteList.Count; x++)
        {
            hairItemsDic.Add($"Hair_common_{x}", hairCommonSpriteList[x]);
          
        }

        for (int x = 0; x < hairGirl_0_SpriteList.Count; x++)
        {
            hairItemsDic.Add($"Hair_girl0_{x}", hairGirl_0_SpriteList[x]);
        }
        for (int x = 0; x < hair_girl0_IconSpriteList.Count; x++)
        {
            hairIconDic.Add($"Hair_girl0_{x}", hair_girl0_IconSpriteList[x]);
        }


        for(int x = 0; x<hair_girl0_rankList.Count; x++)
        {
            hairRankDic.Add($"Hair_girl0_{x}", hair_girl0_rankList[x]);
        }


        for (int x = 0; x<hairGirl_1_SpriteList.Count; x++)
        {
            hairItemsDic.Add($"Hair_girl1_{x}", hairGirl_1_SpriteList[x]);
        }
        for (int x = 0; x < hairBoy_0_SpriteList.Count; x++)
        {
            hairItemsDic.Add($"Hair_boy0_{x}", hairBoy_0_SpriteList[x]);
        }
        for(int x= 0; x<hair_boy0_IconSpriteLIst.Count; x++)
        {
            hairIconDic.Add($"Hair_boy0_{x}", hair_boy0_IconSpriteLIst[x]);
        }

        ////// body
        for (int x = 0; x < bodyCommonSpriteList.Count; x++)
        {
            bodyItemsDic.Add($"Body_common_{x}", bodyCommonSpriteList[x]);
        }
        for (int x = 0; x<body_common_iconSpriteList.Count; x++)
        {
            bodyIconDic.Add($"Body_common_{x}", body_common_iconSpriteList[x]);
        }
        
        for (int x = 0; x < bodyGirl_0_SpriteList.Count; x++)
        {
            bodyItemsDic.Add($"Body_girl0_{x}", bodyGirl_0_SpriteList[x]);
        }
        for (int x= 0; x< body_girl0_IconSpriteList.Count; x++)
        {
            bodyIconDic.Add($"Body_girl0_{x}", body_girl0_IconSpriteList[x]);
        }
        for(int x= 0; x<body_girl0_rankList.Count; x++)
        {
            bodyRankDic.Add($"Body_girl0_{x}", body_girl0_rankList[x]);
        }


        for (int x = 0; x < bodyBoy_0_SpriteList.Count; x++)
        {
            bodyItemsDic.Add($"Body_boy0_{x}", bodyBoy_0_SpriteList[x]);
        }
        for (int x= 0; x<body_boy0_IconSpriteList.Count; x++)
        {
            bodyIconDic.Add($"Body_boy0_{x}", body_boy0_IconSpriteList[x]);
        }

        ///// Eyes
        for(int x =0; x< eyesCommonSpriteList.Count; x++)
        {
            eyesItemsDic.Add($"Eyes_common_{x}", eyesCommonSpriteList[x]);
        }
       
        for(int x =0; x <eyes_common_IconSpriteList.Count; x++)
        {
            eyesIconDic.Add($"Eyes_common_{x}", eyes_common_IconSpriteList[x]);
        }
     

        for(int x = 0; x< eyesBoy_0_SpriteList.Count; x++)
        {
            eyesItemsDic.Add($"Eyes_boy0_{x}", eyesBoy_0_SpriteList[x]);
        }

        ///// Ears
        for(int x= 0; x< earsCommonSpriteList.Count; x++)
        {
            earsItemsDic.Add($"Ears_common_{x}", earsCommonSpriteList[x]);
        }
     
        for(int x=0; x<ears_Common_IconSpriteList.Count; x++)
        {
            earsIconDic.Add($"Ears_common_{x}", ears_Common_IconSpriteList[x]);
        }
    
      

        //// Mouth
        for(int x= 0; x <mouthCommonSpriteList.Count; x++)
        {
            mouthItemsDic.Add($"Mouth_common_{x}", mouthCommonSpriteList[x]);
        }
        
        for(int x= 0; x< mouthGirl_0_SpriteList.Count; x++ )
        {
            mouthItemsDic.Add($"Mouth_girl0_{x}", mouthGirl_0_SpriteList[x]);
        }
        for(int x =0; x < mouth_common_iconSpriteList.Count; x++)
        {
            mouthIconDic.Add($"Mouth_common_{x}", mouth_common_iconSpriteList[x]);
        }
       



        for(int x =0; x< mouthBoy_0_SpriteList.Count; x++)
        {
            mouthItemsDic.Add($"Mouth_boy0_{x}", mouthBoy_0_SpriteList[x]);
        }

        //// Special
        for(int x = 0; x< specialParticleList.Count; x++)
        {
            specialItemsDic.Add($"Special_particle_{x}", specialParticleList[x]);
        }
        for(int x = 0;  x < specialSpriteList.Count; x++)
        {
            specialItemsDic.Add($"Special_sprite_{x}", specialSpriteList[x]);
        }
        for (int x= 0; x< specialIcon_sprite_List.Count; x++)
        {
            specialIconDic.Add($"Special_sprite_{x}", specialIcon_sprite_List[x]);
        }
        for (int x = 0; x<special_sprite_rankList.Count; x++)
        {
            specialRankDic.Add($"Special_sprite_{x}", special_sprite_rankList[x]);
        }


        print("setDictionary");
        ItemSettingIsOver = true;
    }

    public void SetPriceOfAllItems ()
    {
        ///////// Use Database 
        PriceOfAllItemsDic.Add("Hair_common_0", 1000);
        PriceOfAllItemsDic.Add("Hair_common_1", 200);
        PriceOfAllItemsDic.Add("Hair_common_2", 3000);
        PriceOfAllItemsDic.Add("Hair_common_3", 400);
   
    }



}
