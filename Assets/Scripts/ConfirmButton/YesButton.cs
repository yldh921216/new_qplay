﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class YesButton : MonoBehaviour
{

    private void OnEnable()
    {
        print(ConfirmPanel.Instance);       
        print(ConfirmPanel.Instance.yesButtonComponent);

        if (ConfirmPanel.Instance.instanceScript)
        {
            if (ConfirmPanel.Instance.deleteProgress) ConfirmPanel.Instance.yesButtonComponent.onClick.AddListener(() => BoardDeleteConfirm(ConfirmPanel.Instance.instanceScript));
            else if (ConfirmPanel.Instance.fixProgress) ConfirmPanel.Instance.yesButtonComponent.onClick.AddListener(() => BoardFixConfirm());
           
        }
        else if (ConfirmPanel.Instance.commentInstanceScript)
        {
            if (ConfirmPanel.Instance.deleteProgress) ConfirmPanel.Instance.yesButtonComponent.onClick.AddListener(() => CommentDeleteConfirm(ConfirmPanel.Instance.commentInstanceScript));
        }       


    }

    private void OnDisable()
    {
        ConfirmPanel.Instance.yesButtonComponent.onClick.RemoveAllListeners();
    }



    private void BoardDeleteConfirm(Instance_button instanceScirpt)
    {
        GoogleSignInDemo.Instance.databaseRef.Child("Boards").Child(instanceScirpt.uniqueKey).SetValueAsync(null).ContinueWith(task =>
        {
            if (task.IsCompleted)
          {
               print(task);
                // toastMessage ----> Complete;
          }
          else print("Failed");
        });
        ConfirmPanel.Instance.deleteProgress = false;
        ConfirmPanel.Instance.gameObject.SetActive(false);
        ConfirmPanel.Instance.instanceScript = null;
    }

    private void BoardFixConfirm()
    {
        ConfirmPanel.Instance.fixProgress = false;
        ConfirmPanel.Instance.gameObject.SetActive(false);
        ConfirmPanel.Instance.instanceScript = null;
    }
     
    private void CommentDeleteConfirm(Comment_instanceButton commentInstanceScript)
    {
        GoogleSignInDemo.Instance.databaseRef.Child("Boards").
        Child(commentInstanceScript.writtenBoardUniqueKey).Child("Comments").Child(commentInstanceScript.uniqueKey).SetValueAsync(null);
        ConfirmPanel.Instance.deleteProgress = false;
        ConfirmPanel.Instance.gameObject.SetActive(false);
        ConfirmPanel.Instance.commentInstanceScript = null;    
    }
}
