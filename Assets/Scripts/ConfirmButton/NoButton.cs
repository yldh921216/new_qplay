﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NoButton : MonoBehaviour
{
    GameObject confirmPanel;
    Button noButtonComponent;

    private void OnEnable()
    {
        confirmPanel = GameObject.Find("LobbyCanvas").transform.Find("ConfirmPanel").gameObject;
        noButtonComponent = GetComponent<Button>();
        noButtonComponent.onClick.AddListener(() => CancelButton()); 
    }
    void OnDisable()
    {
        noButtonComponent.onClick.RemoveAllListeners();
    }

    void CancelButton( )
    {
        confirmPanel.SetActive(false);
    }

}
