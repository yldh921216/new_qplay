﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConfirmPanel : MonoBehaviour
{
    public static ConfirmPanel confirmInstance;
    public static ConfirmPanel Instance
    {
        get
        {
            if (!confirmInstance)
            {
                confirmInstance = FindObjectOfType<ConfirmPanel>();

                if (confirmInstance == null)
                {
                    GameObject container = new GameObject();
                    container.name = "ConfirmPanel";
                    confirmInstance = container.AddComponent<ConfirmPanel>();
                }
            }
            return confirmInstance;
        }
    }

    public Button yesButtonComponent;

    public bool deleteProgress = false;
    public bool fixProgress = false;
    public Instance_button instanceScript;
    public Comment_instanceButton commentInstanceScript;

}
