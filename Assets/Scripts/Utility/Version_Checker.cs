﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Text.RegularExpressions;
using UnityEngine.UI;
using HtmlAgilityPack;


public class Version_Checker : MonoBehaviour
{
    public GameObject versionCheck_pannel;
    public Button LogInButton;

    private void Start()
    {
        UnSafeSecurityPolicy.Instate();
        string marketVersion = "";

        string url = "https://play.google.com/store/apps/details?id=com.googlelogin.check";

        HtmlWeb web = new HtmlWeb();
        HtmlDocument doc = web.Load(url);

        foreach ( HtmlNode node in doc.DocumentNode.SelectNodes("//span[@class='htlgb']"))
        {
            marketVersion = node.InnerText.Trim();
         
            if(marketVersion != null)
            {
                if(System.Text.RegularExpressions.Regex.IsMatch(marketVersion , @"^\d{1}\.\d{1}\.\d{1}$"))
                {
                    print(marketVersion);
                    Debug.Log("My App Version :" + Application.version);
                    Debug.Log("Market Version :" + marketVersion);

                    string a = marketVersion.ToString();
                    string b = Application.version.ToString();
               
                    if ( a == b )
                    {                  
                        LogInButton.interactable = true;
                    }
                    else if ( a != b )
                    {
                        versionCheck_pannel.transform.Find("Button").GetComponent<Button>().onClick.AddListener( () => OpenURL(url));
                        versionCheck_pannel.SetActive(true);
                    }             
                }
            }
        }
    }

    public void OpenURL(string url)
    {
        Application.OpenURL(url);
    }




}
