﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraResolution : MonoBehaviour
{
    private void Awake()
    {

        Camera camera = GetComponent<Camera>();
        Rect rect = camera.rect;

        float scaleHeight = ((float)Screen.width / Screen.height) / ((float)9 / 16);
        float scaleWidht = 1f / scaleHeight;
        if(scaleHeight < 1  )
        {
            rect.height = scaleHeight;
            rect.y = (1f - scaleHeight) / 2f;
        }
        else
        {
            rect.width = scaleWidht;
            rect.x = (1f - scaleWidht) / 2f;
        }
        camera.rect = rect;
        print(camera.rect);
     }


}
