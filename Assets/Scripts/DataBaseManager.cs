﻿using Firebase;
using Firebase.Auth;
using Firebase.Database;
using Firebase.Unity;
using Firebase.Extensions;
using Firebase.Storage;

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using EZObjectPools;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine.EventSystems;
using System;
using System.IO;
using UnityEngine.Networking;
using TMPro;
using DG.Tweening;



public class DataBaseManager : MonoBehaviour
{
    public static DataBaseManager _instance;
    public static DataBaseManager Instance
    {
        get
        {
            if (!_instance)
            {
                _instance = FindObjectOfType<DataBaseManager>();

                if (_instance == null)
                {
                    GameObject container = new GameObject();
                    container.name = "DataBaseManager";
                    _instance = container.AddComponent<DataBaseManager>();
                }
            }
            return _instance;
        }
    }
    EZObjectPool boardEzPool;
   public  EZObjectPool commentEzPool;
    public EZObjectPool goldEzPool;
    public Transform goldParent;
    public int goldBombCount;



    public class UserInfoData
    {
        public string PlayerUid;
        public string PlayerAccount;
        public string PlayerNickname;
    }
    public class UserLevelData
    {
        public int PlayerLevel;
        public int PlayerMaxExp;
        public int PlayerCurrentExp;
    }
    public class UserCharacterInfo
    {
        public string EquippedBaseCharacterCode;      // girl0 , girl1, girl2 , boy0, boy1, boy2   
        public string EquippedHairItem;
        public string EquippedBodyItem;
        public string EquippedEarsItem;
        public string EquippedEyesItem;
        public string EquippedBackItem;
        public string EquippedMouthItem;
        public string EquippedSpecialItem;
        public string EquippedBackgroundItem;
    }
    public class UserGoldData
    {
        public int GoldCount;
        public int GemCount;
    }
    public class UserFirstItemData
    {
        public string ItemKey;
        public string ItemCode;
        public string ItemType;
    }
    public class SlimeLevelData
    {
        public int SlimeLevel;
        public int SlimeMaxExp;
        public int SlimeCurrentExp;
    }
    public class NewComment
    {
        public string CommentUid;
        public string CommentNickname;
        public string Contents;
        public string Date;
        public int ThumbsUpCount;
    }

    public string SlimeURL;

    [Header("UserData")]
    public string currentPlayerUid;
    public string currentPlayerNickName;


    [Header("CharacterProfilePanel")]
    public Text profilePanelNickNameText;


    [Header("Friends")]
    public List<string> friendsUidList = new List<string>();

    [Header("SaveAndLoadTest_DataList")]
    public InputField userName;
    public InputField email;
    public InputField nameToRead;
    public List<string> boardsKey = new List<string>();
    private long keyCount = 100;
    public List<DataSnapshot> boardSnapshots = new List<DataSnapshot>();
    public Transform InstanceParentTransform;
    public List<GameObject> instancesList = new List<GameObject>();
    public List<DataSnapshot> myBoards_snapshotsList = new List<DataSnapshot>();
    public WriteNickName firstRegisterScript;


    [Header("ReadWrittenBoard")]
    public Image writtenBoard_rankImage;
    public GameObject writtenBoard;
    public WrittenBoard_script writtenBoardScript;

    public Text writtenBoard_contentText;
    public Text writtenBoard_titleText;
    public Text writtenBoard_nicknameText;
    public TextMeshProUGUI writtenBoard_commentsCountText;
    public TextMeshProUGUI writtenBoard_thumbsUpText;
    public Button thumbsUpButton;

    public Image writtenBoard_backgroundItemImage;
    public Image writtenBoard_baseCharacterImage;
    public Image writtenBoard_hairItemImage;
    public Image writtenBoard_bodyItemImage;
    public Image writtenBoard_eyesItemImage;
    public Image writtenBoard_earsItemImage;
    public Image writtenBoard_backItemImage;
    public Image writtenBoard_mouthItemImage;
    public Image writtenBoard_specialItemImage;


    [Header("New_Write")]
    public BoardControlManager boardControlScript;
    public TextMeshProUGUI newWriteTagText;
    public InputField newWriteTitleText;
    public InputField newWriteContent;
    public bool newWriteCheck;
    public Button newWritePublishButton;
    public float newWriteLimit = 60f;
    public float newWriteTimer;
    BulletinBoard_script boardPanelScript;


    [Header("Image")]
    public Image profileImage;
    public StorageReference storageReference;
    public FirebaseStorage storage;
    public DatabaseReference databaseReference;
    string image_url;


    [Header("DressRoom")]
    public DressManager dressRoomManagerScript;




    [Header("Comment")]
    public InputField commentInputField;
    public Button commentPushButton;
    public float commentLimitTime = 5f;
    public Image commentPushImage;
    public List<string> commentInstancesList = new List<string>();
    public Dictionary<string, long> commentsCountDic = new Dictionary<string, long>();



    [Header("Utility")]
    public float flickerTime;
    public float flickerLongTime;


    [Header("Gold")]
    public int currentGoldCount;
    public int currentGemCount;
    public TextMeshProUGUI characterProfileGoldText;
    public TextMeshProUGUI characterProfileGemText;
    public TextMeshProUGUI storeGoldText;
    public TextMeshProUGUI storeGemText;




    private void Awake()
    {
        instancesList.Clear();
        writtenBoardScript = writtenBoard.GetComponent<WrittenBoard_script>();
        boardControlScript = FindObjectOfType<BoardControlManager>();
        boardPanelScript = FindObjectOfType<BulletinBoard_script>();
        firstRegisterScript = FindObjectOfType<WriteNickName>();
        dressRoomManagerScript = FindObjectOfType<DressManager>();

        writtenBoard = GameObject.Find("LobbyCanvas").transform.Find("ShowWrittenBoard").gameObject;
        boardEzPool = GameObject.Find("NewWriteInstance_objectsPool").GetComponent<EZObjectPool>();

        StartCoroutine(firstRegisterScript.CheckFirstAccess());
    }
    private void Start()
    {
        // StartCoroutine(InitAllInstance());
        BoardsChangingListner();

    }

    public void SetUserDataToProfilePanel()
    {
        profilePanelNickNameText.text = currentPlayerNickName;
    }



    public void SaveCharacterDressedImage()
    {
        string filePath;
        // image currentCharacterImage = ; 
        RectTransform currentCharacterImage = EventSystem.current.currentSelectedGameObject.GetComponent<RectTransform>();
        Texture2D texture = new Texture2D((int)currentCharacterImage.rect.width, (int)currentCharacterImage.rect.height);
        texture.ReadPixels(new Rect(0, 0, currentCharacterImage.rect.width, currentCharacterImage.rect.height), 0, 0);
        texture.Apply();
        byte[] bytes = texture.EncodeToPNG();

        storageReference.Child("UserUniqueKey").Child("CurrentImage").PutBytesAsync(bytes).ContinueWith(task =>
        {
            if (task.IsCompleted)
            {
                StorageMetadata metadata = task.Result;
            }
            else
            {

            }
        });

    }
    IEnumerator GetText(string image_url)
    {
        UnityWebRequest www = UnityWebRequest.Get(image_url);
        print(www);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
        }
        else
        {
            // Show results as text
            Debug.Log(www.downloadHandler.text);
            // Or retrieve results as binary data
            byte[] results = www.downloadHandler.data;

            Sprite newProfile = makeImageFromByte(results);
            // button -> load
        }
    }
    public Sprite makeImageFromByte(byte[] imageData)
    {
        Sprite newProfileImage;
        Texture2D newImageTexture;
        newImageTexture = new Texture2D(300, 300);
        newImageTexture.LoadImage(imageData, false);

        Vector2 pivot = new Vector2(0.5f, 0.5f);
        Rect tRect = new Rect(0, 0, newImageTexture.width, newImageTexture.height);
        Sprite newSprite = Sprite.Create(newImageTexture, tRect, pivot);

        newProfileImage = newSprite;

        return newProfileImage;

    }


    ///////////////////////////////////////////////////  -------> Event Listner  
    public void BoardsChangingListner()
    {

        GoogleSignInDemo.Instance.databaseRef.Child("Boards").ChildAdded += DataBaseManager_ChildAdded;
        GoogleSignInDemo.Instance.databaseRef.Child("Boards").ChildRemoved += DataBaseManager_ChildRemoved;
        GoogleSignInDemo.Instance.databaseRef.Child("Boards").ChildChanged += DataBaseManager_ChildChanged;

        GoogleSignInDemo.Instance.databaseRef.Child("User").Child(GoogleSignInDemo.Instance.firebaseUser.UserId).Child("Inventory").Child("Money").ChildChanged += CheckMoneyChanged;


    }


    private void CheckMoneyChanged(object sender, ChildChangedEventArgs e)
    {

        if (e.Snapshot.Key == "GemCount")
        {
            characterProfileGemText.text = e.Snapshot.Value.ToString();
            storeGemText.text = e.Snapshot.Value.ToString();
            int.TryParse(e.Snapshot.Value.ToString(), out int result);
            currentGemCount = result;

        }
        else if (e.Snapshot.Key == "GoldCount")
        {
            characterProfileGoldText.text = e.Snapshot.Value.ToString();
            storeGoldText.text = e.Snapshot.Value.ToString();
            int.TryParse(e.Snapshot.Value.ToString(), out int result);
            currentGoldCount = result;
        }

    }



    private void DataBaseManager_ChildChanged(object sender, ChildChangedEventArgs e)
    {
        if (e.Snapshot.Child("Comments").ChildrenCount > commentsCountDic[e.Snapshot.Key])
        {
                foreach (var comment in e.Snapshot.Child("Comments").Children)
                {
                    if (!commentInstancesList.Contains(comment.Key))
                    {
                        commentsCountDic[e.Snapshot.Key] += 1;
                        commentInstancesList.Add(comment.Key);

                        string commentContentValue = null;
                        string commentorUidValue = null;
                        string commentorNicknameValue = null;
                        string commentUniqueKey = null;
                        string commentDate = null;

                        //commentUniqueKey      
                        foreach (var commentChild in comment.Children)
                        {
                            if (commentChild.Key == "Contents") { commentContentValue = commentChild.Value.ToString(); }
                            else if (commentChild.Key == "CommentUid") { commentorUidValue = commentChild.Value.ToString(); }
                            else if (commentChild.Key == "Date") { commentDate = commentChild.Value.ToString(); }
                            else if (commentChild.Key == "CommentNickname") { commentorNicknameValue = commentChild.Value.ToString(); }
                        }

                        bool isTrueComment = commentEzPool.TryGetNextObject(transform.position, Quaternion.identity, out GameObject comment_instanceButton);
                        Comment_instanceButton commentButtonScript = null;

                        if (isTrueComment)
                        {
                            if (e.Snapshot != null)
                            {
                                commentUniqueKey = comment.Key;
                                comment_instanceButton.name = "Comment_Instance" + comment.Key;

                                Text commentContentText = comment_instanceButton.transform.Find("Comment_content_text").GetComponent<Text>();

                                comment_instanceButton.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
                                commentContentText.text = commentContentValue;

                                commentButtonScript = comment_instanceButton.GetComponent<Comment_instanceButton>();
                                commentButtonScript.content = commentContentValue;
                                commentButtonScript.commentorUid = commentorUidValue;
                                commentButtonScript.uniqueKey = comment.Key;
                                commentButtonScript.writtenBoardUniqueKey = e.Snapshot.Key;
                                commentButtonScript.commentorNickName = commentorNicknameValue;
                            }
                        }

                        if (writtenBoardScript.currentWrittenBoard_uniqueKey == e.Snapshot.Key)
                        {
                            comment_instanceButton.transform.parent = writtenBoardScript.commentInstanceParentTransform;
                            comment_instanceButton.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
                            writtenBoardScript.commentInstanceList.Add(comment_instanceButton);
                        }
                        else
                        {

                        }

                        print(commentButtonScript);
                        print(commentorUidValue);
                        if (commentButtonScript != null)
                        {
                            GoogleSignInDemo.Instance.databaseRef.Child("User").Child(commentorUidValue).Child("Inventory").Child("Equipment").GetValueAsync().ContinueWithOnMainThread(task =>
                            {
                                if (task.IsCompleted)
                                {
                                    DataSnapshot snapshot = task.Result;
                                    foreach (var items in snapshot.Children)
                                    {
                                        switch (items.Key)
                                        {
                                            case "EquippedBackItem":
                                                commentButtonScript.commentor_backItemString = items.Value.ToString();
                                                break;
                                            case "EquippedBackgroundItem":
                                                commentButtonScript.commentor_backgroundItemString = items.Value.ToString();
                                                break;
                                            case "EquippedBaseCharacterCode":
                                                commentButtonScript.commentor_baseCharacterCodeString = items.Value.ToString();
                                                break;
                                            case "EquippedHairItem":
                                                commentButtonScript.commentor_hairItemString = items.Value.ToString();
                                                break;
                                            case "EquippedBodyItem":
                                                commentButtonScript.commentor_bodyItemString = items.Value.ToString();
                                                break;
                                            case "EquippedEarsItem":
                                                commentButtonScript.commentor_earsItemString = items.Value.ToString();
                                                break;
                                            case "EquippedEyesItem":
                                                commentButtonScript.commentor_eyesItemString = items.Value.ToString();
                                                break;
                                            case "EquippedMouthItem":
                                                commentButtonScript.commentor_mouthItemString = items.Value.ToString();
                                                break;
                                            case "EquippedSpecialItem":
                                                commentButtonScript.commentor_specialItemString = items.Value.ToString();
                                                break;

                                        }
                                    }
                                    commentButtonScript.getEquipment = true;
                                }
                                else
                                {

                                }
                            });

                        }
                        return;
                    }

                }

            }
        
    
        else if ( e.Snapshot.Child("Comments").ChildrenCount < commentsCountDic[e.Snapshot.Key])
        {
            if (!e.Snapshot.HasChild("Comments"))
            {
                print("1->0");
                for ( int x = 0;  x < commentEzPool.ObjectList.Count; x ++)
                {
                  if( commentEzPool.ObjectList[x].gameObject.GetComponent<Comment_instanceButton>().writtenBoardUniqueKey == e.Snapshot.Key )
                    {
                        Comment_instanceButton commentScript = commentEzPool.ObjectList[x].gameObject.GetComponent<Comment_instanceButton>();
                        commentScript.uniqueKey = null;
                        commentScript.commentorUid = null;
                        commentScript.writtenBoardUniqueKey = null;
                        
                        commentEzPool.ObjectList[x].GetComponent<PooledObject>().GoToAvailable();
                        commentsCountDic[e.Snapshot.Key] -= 1;
                        commentInstancesList.Remove(commentEzPool.ObjectList[x].gameObject.GetComponent<Comment_instanceButton>().uniqueKey);
                       
                    }
                }     
            }
             if( e.Snapshot.HasChild("Comments"))
            {
                string removedCommentString = null;

                for (int x = 0; x < commentInstancesList.Count; x++)
                {
                    print("asdaa");
                    if (! e.Snapshot.Child("Comments").HasChild(commentInstancesList[x]))
                    {      
                        for ( int y= 0; y < commentEzPool.ObjectList.Count; y++)
                        {
                            Comment_instanceButton commentScript = commentEzPool.ObjectList[y].GetComponent<Comment_instanceButton>();

                            if(commentScript.uniqueKey == commentInstancesList[x] && commentScript.writtenBoardUniqueKey == writtenBoardScript.currentWrittenBoard_uniqueKey)
                            {
                                removedCommentString = commentInstancesList[x];
                                print(removedCommentString);
                                commentsCountDic[e.Snapshot.Key] -= 1;
                                commentEzPool.ObjectList[y].GetComponent<PooledObject>().GoToAvailable();
                                commentScript.uniqueKey = null;
                                commentScript.commentorUid = null;
                                commentScript.writtenBoardUniqueKey = null;
                                commentInstancesList.Remove(commentInstancesList[x]);
                                return;
                            }

                        }

                    }
                }               
            }
         }

     }

            
     
       
    
    private void DataBaseManager_ChildRemoved(object sender, ChildChangedEventArgs e)
    {
        string removedUniqueKey = e.Snapshot.Key;

        if (e.Snapshot.HasChild("Comments"))
        {
            foreach(var item in e.Snapshot.Child("Comments").Children)
            {
                commentInstancesList.Remove(item.Key);
            }
        }

        for (int x = 0; x < instancesList.Count; x++)
        {
            if (instancesList[x] != null)
            {
                Instance_button buttonScript = instancesList[x].GetComponentInChildren<Instance_button>();
                if (buttonScript.uniqueKey == removedUniqueKey)
                {
                    instancesList[x].GetComponent<PooledObject>().GoToAvailable();
                    instancesList.RemoveAt(x);
                    return;
                }
            }

        }
    }
    private void DataBaseManager_ChildAdded(object sender, ChildChangedEventArgs e)
    {
        string contentValue = null;
        // string tagValue = null;
        string titleValue = null;
        string writerUidValue = null;
        string writerNicknameValue = null;
        long setThumbsUpCountValue = 0;

        if (!e.Snapshot.HasChild("Comments"))
        {
            commentsCountDic.Add(e.Snapshot.Key, 0);
        }

        foreach (var itemf in e.Snapshot.Children)
        {
            
            if (itemf.Key == "content") { contentValue = itemf.Value.ToString(); }
            else if (itemf.Key == "title") { titleValue = itemf.Value.ToString(); }
            else if (itemf.Key == "writerUid") { writerUidValue = itemf.Value.ToString(); }
            else if (itemf.Key == "thumbsUpCount")
            {
                if (itemf.HasChild("ThumbsUpCountValue"))
                {
                    long.TryParse(itemf.Child("ThumbsUpCountValue").Value.ToString(), out setThumbsUpCountValue);
                }

            }
            else if (itemf.Key == "writerNickname")
            {
                writerNicknameValue = itemf.Value.ToString();
            }
            else if (itemf.Key == "Comments")
            {
                string commentContentValue = null;
                string commentorUidValue = null;
                string commentorNicknameValue = null;
                string commentUniqueKey = null;
                string commentDate = null;

                //commentUniqueKey      
                foreach (var comment in itemf.Children)
                {
                    foreach ( var commentChild  in comment.Children) 
                    {
                        if (commentChild.Key == "Contents") { commentContentValue = commentChild.Value.ToString(); }
                        else if (commentChild.Key == "CommentUid") { commentorUidValue = commentChild.Value.ToString(); }
                        else if (commentChild.Key == "Date") { commentDate = commentChild.Value.ToString(); }
                        else if (commentChild.Key == "CommentNickname") { commentorNicknameValue = commentChild.Value.ToString(); }
                    }

                    bool isTrueComment = commentEzPool.TryGetNextObject(transform.position, Quaternion.identity, out GameObject comment_instanceButton);
                    Comment_instanceButton commentButtonScript = null;

                    if (isTrueComment)
                    {
                        if (e.Snapshot != null)
                        {
                            commentUniqueKey = comment.Key;
                            comment_instanceButton.name = "Comment_Instance" + comment.Key;
                            
                            Text commentContentText = comment_instanceButton.transform.Find("Comment_content_text").GetComponent<Text>();

                            comment_instanceButton.transform.parent = writtenBoardScript.commentInstanceParentTransform;
                            comment_instanceButton.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
                            commentInstancesList.Add(comment.Key);
                            commentContentText.text = commentContentValue;

                            commentButtonScript = comment_instanceButton.GetComponent<Comment_instanceButton>();
                            commentButtonScript.content = commentContentValue;
                            commentButtonScript.commentorUid = commentorUidValue;
                            commentButtonScript.uniqueKey = comment.Key;
                            commentButtonScript.writtenBoardUniqueKey = e.Snapshot.Key;
                            commentButtonScript.commentorNickName = commentorNicknameValue;
                        }
                    }

                    if (commentButtonScript != null)
                    {
                        GoogleSignInDemo.Instance.databaseRef.Child("User").Child(commentorUidValue).Child("Inventory").Child("Equipment").GetValueAsync().ContinueWithOnMainThread(task =>
                        {
                            if (task.IsCompleted)
                            {
                                DataSnapshot snapshot = task.Result;
                                foreach (var items in snapshot.Children)
                                {

                                    switch (items.Key)
                                    {

                                        case "EquippedBackItem":
                                            commentButtonScript.commentor_backItemString = items.Value.ToString();
                                            break;
                                        case "EquippedBackgroundItem":
                                            commentButtonScript.commentor_backgroundItemString = items.Value.ToString();
                                            break;
                                        case "EquippedBaseCharacterCode":
                                            commentButtonScript.commentor_baseCharacterCodeString = items.Value.ToString();
                                            break;
                                        case "EquippedHairItem":
                                            commentButtonScript.commentor_hairItemString = items.Value.ToString();
                                            break;
                                        case "EquippedBodyItem":
                                            commentButtonScript.commentor_bodyItemString = items.Value.ToString();
                                            break;
                                        case "EquippedEarsItem":
                                            commentButtonScript.commentor_earsItemString = items.Value.ToString();
                                            break;
                                        case "EquippedEyesItem":
                                            commentButtonScript.commentor_eyesItemString = items.Value.ToString();
                                            break;
                                        case "EquippedMouthItem":
                                            commentButtonScript.commentor_mouthItemString = items.Value.ToString();
                                            break;
                                        case "EquippedSpecialItem":
                                            commentButtonScript.commentor_specialItemString = items.Value.ToString();
                                            break;

                                    }
                                }
                                commentButtonScript.getEquipment = true;
                            }
                            else
                            {

                            }
                        });
                    }

                    comment_instanceButton.SetActive(false);


                }
                  commentsCountDic.Add(e.Snapshot.Key, itemf.ChildrenCount);
            }        
        }

        bool isTrue = boardEzPool.TryGetNextObject(InstanceParentTransform.transform.position, Quaternion.identity, out GameObject instanceButton);
            Instance_button buttonScript = null;
        
           if (isTrue)
            {
                if (e.Snapshot != null)
                {
                    instanceButton.name = "Instance" + e.Snapshot.Key;
                    // Text tagText = instanceButton.transform.Find("TagText").GetComponent<Text>();
                    Text titleText = instanceButton.transform.Find("Instance").transform.Find("TitleText").GetComponent<Text>();
                    Text nickNameText = instanceButton.transform.Find("Instance").transform.Find("NicknameText").GetComponent<Text>();
                    instanceButton.transform.parent = InstanceParentTransform;
                    instanceButton.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
                    instancesList.Add(instanceButton);

                    // tagText.text = tagValue;
                    titleText.text = titleValue;
                    nickNameText.text = writerNicknameValue;

                    buttonScript = instanceButton.GetComponentInChildren<Instance_button>();
                    buttonScript.title = titleValue;
                    buttonScript.content = contentValue;
                    buttonScript.writerUid = writerUidValue;
                    buttonScript.uniqueKey = e.Snapshot.Key;
                    buttonScript.thumbsUpCount = setThumbsUpCountValue;
                    buttonScript.writerNickName = writerNicknameValue;
                }
            }

            GoogleSignInDemo.Instance.databaseRef.Child("User").Child(writerUidValue).Child("Inventory").Child("Equipment").GetValueAsync().ContinueWithOnMainThread(task =>
            {
                if (task.IsCompleted)
                {
                //Image writerBackgroundImage = instanceButton.transform.parent.Find("Writer_backgroundItem").GetComponent<Image>();
                //Image writerBackImage = instanceButton.transform.parent.Find("Writer_backItem").GetComponent<Image>();
                //Image writerBodyImage = instanceButton.transform.parent.Find("Writer_bodyItem").GetComponent<Image>();
                //Image writerHairImage = instanceButton.transform.parent.Find("Writer_hairItem").GetComponent<Image>();
                //Image writerMouthImage = instanceButton.transform.parent.Find("Writer_mouthItem").GetComponent<Image>();
                //Image writerEyesImage = instanceButton.transform.parent.Find("Writer_eyesItem").GetComponent<Image>();
                //Image writerEarsImage = instanceButton.transform.parent.Find("Writer_earsItem").GetComponent<Image>();
                //Image writerSpecialImage = instanceButton.transform.parent.Find("Writer_specialItem").GetComponent<Image>();
                //Image writerBaseImage = instanceButton.transform.parent.Find("Writer_baseItem").GetComponent<Image>();

                DataSnapshot snapshot = task.Result;
                    foreach (var item in snapshot.Children)
                    {
                        if (item.Key == "EquippedBaseCharacterCode")
                        {
                            buttonScript.writer_baseCharacterCodeString = item.Value.ToString();
                        }
                        else if (item.Key == "EquippedHairItem")
                        {
                            buttonScript.writer_hairItemString = item.Value.ToString();
                        }
                        else if (item.Key == "EquippedBodyItem")
                        {
                            buttonScript.writer_bodyItemString = item.Value.ToString();
                        }
                        else if (item.Key == "EquippedEarsItem")
                        {
                            buttonScript.writer_earsItemString = item.Value.ToString();
                        }
                        else if (item.Key == "EquippedEyesItem")
                        {
                            buttonScript.writer_eyesItemString = item.Value.ToString();
                        }
                        else if (item.Key == "EquippedSpecialItem")
                        {
                            buttonScript.writer_specialItemString = item.Value.ToString();
                        }
                        else if (item.Key == "EquippedBackgroundItem")
                        {
                            buttonScript.writer_backgroundItemString = item.Value.ToString();
                        }
                        else if (item.Key == "EquippedBackItem")
                        {
                            buttonScript.writer_backItemString = item.Value.ToString();
                        }
                        else if (item.Key == "EquippedMouthItem")
                        {
                            buttonScript.writer_mouthItemString = item.Value.ToString();
                        }

                    }

                    buttonScript.getEquipment = true;
                }
                else
                {

                }
            });
    }


    public void SnapshotDataRead(DataSnapshot getSnapShot)
    {
        DataSnapshot snapshot = getSnapShot;

        foreach (var item in snapshot.Children)
        {
            Debug.Log($"ValueChanged : {item.Value}");
        }
    }
    public void SnapshotAllDataRead (DataSnapshot getSnapShot)
    {
         foreach (var item in getSnapShot.Children)
        {       
        }
    }

    ///////////////////////////////////////////////////  <------ Event Listner 
   
    /////////////  ---> Real Task

    //// Check
    
    /////// User

    public void SaveUserInfoAfterLogin() { }
    public void SaveUserInfoWhenNicknameRegistered()  // When user Register Nickname;
    {
        UserInfoData jsonUserData = new UserInfoData();
        UserCharacterInfo jsonUserCharacterData = new UserCharacterInfo();

        print("save..");
        if (GoogleSignInDemo.Instance.firebaseUser != null)
        {
            print("UserLogIn");
            jsonUserData.PlayerUid = GoogleSignInDemo.Instance.firebaseUser.UserId;
            currentPlayerUid = GoogleSignInDemo.Instance.firebaseUser.UserId;
            jsonUserData.PlayerAccount = GoogleSignInDemo.Instance.firebaseUser.Email;
            jsonUserData.PlayerNickname = firstRegisterScript.nicknameInputField.text;
            string json = JsonUtility.ToJson(jsonUserData);
            GoogleSignInDemo.Instance.databaseRef.Child("User").Child(currentPlayerUid).Child("Info").SetRawJsonValueAsync(json).ContinueWithOnMainThread(task =>
            {
                if (task.IsFaulted)
                {
                    
                }
                else if (task.IsCanceled)
                {
                   
                }
                else
                {
                             
                }
            });

            jsonUserCharacterData.EquippedBaseCharacterCode = firstRegisterScript.selectedBaseCharacterCode;
            jsonUserCharacterData.EquippedHairItem = firstRegisterScript.selectedHair;
            jsonUserCharacterData.EquippedBodyItem = firstRegisterScript.selectedBody;
            jsonUserCharacterData.EquippedEarsItem = firstRegisterScript.selectedEars;
            jsonUserCharacterData.EquippedEyesItem = firstRegisterScript.selectedEyes;
            jsonUserCharacterData.EquippedBackItem = firstRegisterScript.selectedBack;
            jsonUserCharacterData.EquippedMouthItem = firstRegisterScript.selectedMouth;
            jsonUserCharacterData.EquippedSpecialItem = firstRegisterScript.selectedSpecial;
            jsonUserCharacterData.EquippedBackgroundItem = firstRegisterScript.selectedBackground;

            string characterJson = JsonUtility.ToJson(jsonUserCharacterData);
            GoogleSignInDemo.Instance.databaseRef.Child("User").Child(currentPlayerUid).Child("Inventory").Child("Equipment").SetRawJsonValueAsync(characterJson).ContinueWithOnMainThread(task =>
            {
                if(task.IsCompleted)
                {
                    print("Register BaseCharacter");
                }
                else
                {

                }
            });

            UserGoldData goldClassJson = new UserGoldData();
            goldClassJson.GoldCount = 10000;
            goldClassJson.GemCount = 0;
            string moneyJson = JsonUtility.ToJson(goldClassJson);
            GoogleSignInDemo.Instance.databaseRef.Child("User").Child(currentPlayerUid).Child("Inventory").Child("Money").SetRawJsonValueAsync(moneyJson).ContinueWithOnMainThread(task =>
            {
                if (task.IsCompleted)
                {
                    print("setMoney");
                }
            });


            GoogleSignInDemo.Instance.databaseRef.Child("User").Child(currentPlayerUid).Child("Inventory").Child("Items").Child("Body").
                Child(firstRegisterScript.selectedBody).SetValueAsync("default");
            GoogleSignInDemo.Instance.databaseRef.Child("User").Child(currentPlayerUid).Child("Inventory").Child("Items").Child("Hair").
                Child(firstRegisterScript.selectedHair).SetValueAsync("default");
            GoogleSignInDemo.Instance.databaseRef.Child("User").Child(currentPlayerUid).Child("Inventory").Child("Items").Child("Special").
                 Child(firstRegisterScript.selectedSpecial).SetValueAsync("default");
            GoogleSignInDemo.Instance.databaseRef.Child("User").Child(currentPlayerUid).Child("Inventory").Child("Items").Child("Eyes").
                Child(firstRegisterScript.selectedEyes).SetValueAsync("default");
            GoogleSignInDemo.Instance.databaseRef.Child("User").Child(currentPlayerUid).Child("Inventory").Child("Items").Child("Ears").
                Child(firstRegisterScript.selectedEars).SetValueAsync("default");
            GoogleSignInDemo.Instance.databaseRef.Child("User").Child(currentPlayerUid).Child("Inventory").Child("Items").Child("Mouth").
                Child(firstRegisterScript.selectedMouth).SetValueAsync("default");
            GoogleSignInDemo.Instance.databaseRef.Child("User").Child(currentPlayerUid).Child("Inventory").Child("Items").Child("Back").
                Child(firstRegisterScript.selectedBack).SetValueAsync("default");
            GoogleSignInDemo.Instance.databaseRef.Child("User").Child(currentPlayerUid).Child("Inventory").Child("Items").Child("Background").
                Child(firstRegisterScript.selectedBackground).SetValueAsync("default");
            GoogleSignInDemo.Instance.databaseRef.Child("User").Child(currentPlayerUid).Child("Inventory").Child("Items").Child("Base").
                Child(firstRegisterScript.selectedBaseCharacterCode).SetValueAsync("default");

        }

        else Debug.LogError("User is Null");
    }


    public void SaveSlimeLevelInfo()
    {
        SlimeLevelData slimeLevelData = new SlimeLevelData();        
        slimeLevelData.SlimeLevel = 1;
        slimeLevelData.SlimeMaxExp = 10;
        slimeLevelData.SlimeCurrentExp = 0;
        string json = JsonUtility.ToJson(slimeLevelData);

        if (GoogleSignInDemo.Instance.firebaseUser != null)
        {
            GoogleSignInDemo.Instance.databaseRef.Child("User").Child(currentPlayerUid).Child("Slime").Child("SlimeInfo").SetRawJsonValueAsync(json).ContinueWith(task =>
            {
                if (task.IsFaulted)
                {

                }
                else if (task.IsCanceled)
                {

                }
                else
                {
                    Debug.Log("SaveSlimeData");
                }

            });
        }
    }
    public void SaveSlimeImageURL()
    {

    }
    public void SaveToFreind () 
    {
        string keyValue = "awdknawd";
        string nickName = "Holi";
        GoogleSignInDemo.Instance.databaseRef.Child("User").Child(currentPlayerUid).Child("Friends").Child(keyValue).SetValueAsync(nickName).ContinueWith(task =>
        {
            if (task.IsCanceled)
            {
                Debug.LogError(task.Exception);
            }
         else if (task.IsFaulted)
            {
                Debug.LogError(task.Exception);
            }
          else
            {
                Debug.Log("AddFriend");
                
            }
        }); 
    }



    ////////////////////////////////////  Read
    public void FindWhatIWrites()
    {
        // Find boards that i writed
        // and add to List 
        for(int x= 0; x< boardsKey.Count; x++ )
        {
            GoogleSignInDemo.Instance.databaseRef.Child("Boards").Child(boardsKey[x]).Child(currentPlayerUid).GetValueAsync().ContinueWith(task =>
            {
                if (task.IsFaulted)
                {

                }
                else if (task.IsCanceled)
                {

                }
                 else
                {
                    if (task.Result != null)
                    {
                        myBoards_snapshotsList.Add(task.Result);
                    }
                }
             
            });
        }  
       
    }
    public string GetNewBoardUniqueKey() // Use this when player write new board
    {
        return GoogleSignInDemo.Instance.databaseRef.Child("Boards").Push().Key;
    } 
    public IEnumerator GetKeyOfAllData()
    {
        boardsKey.Clear();

        GoogleSignInDemo.Instance.databaseRef.Child("Boards").GetValueAsync().ContinueWithOnMainThread(task =>
        {
            if (task.IsCompleted)
            {
                DataSnapshot boardSnapshot = task.Result;
                keyCount = boardSnapshot.ChildrenCount;
                foreach (var item in boardSnapshot.Children)
                {
                    boardsKey.Add(item.Key);
                }
            }
            else
            {

            }
        });

        while (!(keyCount == boardsKey.Count))
            yield return null;


    }   
    public IEnumerator GetSnapshotOfAllData()
    {
        if (boardsKey.Count == 0) yield break;
        boardSnapshots.Clear();
        
        for (int x = 0; x < boardsKey.Count; x++)
        {
            GoogleSignInDemo.Instance.databaseRef.Child("Boards").Child(boardsKey[x]).GetValueAsync().ContinueWithOnMainThread(task =>
            {
                if (task.IsCompleted)
                {
                    DataSnapshot snapshot = task.Result;               
                    boardSnapshots.Add(snapshot);                                                                 
                }
            });
        }
        while (!(boardSnapshots.Count == boardsKey.Count))
        {         
            yield return null;
        }     
    }
    public IEnumerator InitAllInstance()
    {
        instancesList.Clear();
       
        yield return GetKeyOfAllData();
        yield return GetSnapshotOfAllData();     
     
        for(int x = 0; x <boardSnapshots.Count; x++)
        {
            bool isTrue = boardEzPool.TryGetNextObject(InstanceParentTransform.transform.position, Quaternion.identity, out GameObject instanceButton);
            if (isTrue)
            {               
                if( boardSnapshots[x] != null)
                {
                    
                    instanceButton.name = "Instance" + boardSnapshots[x].Key.ToString();                                           
                    instancesList.Add(instanceButton);
                   
        
                    // Text thumbsUpText 

                    string tagValue = boardSnapshots[x].Child("tag").Value.ToString();
                    string titleValue = boardSnapshots[x].Child("title").Value.ToString();
                    string writerUid = boardSnapshots[x].Child("writerUid").Value.ToString();
                    // string writerNickname = boardSnapshots[x].Child("nickName").Value.ToString();
                    string contentValue = boardSnapshots[x].Child("content").Value.ToString();
                    // string thumbsUpCountValue = boardSnapshots[x].Child("thumbsUpCount").Value.ToString();
                    // string dateValue = boardSnapshot[x].Child("date").Value.ToString();
                    
                    Text tagText = instanceButton.transform.Find("TagText").GetComponent<Text>();
                    Text titleText = instanceButton.transform.Find("TitleText").GetComponent<Text>();

                    tagText.text = tagValue;
                    titleText.text = titleValue;

                    Instance_button buttonScript = instanceButton.GetComponent<Instance_button>();
                    buttonScript.title = titleValue;
               //     buttonScript.tagString = tagValue;
                    buttonScript.content = contentValue;
                }                           
            }
        }

        for(int x= 0; x<instancesList.Count; x++)
        {      
            instancesList[x].transform.SetParent(InstanceParentTransform);
            instancesList[x].GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        }


    } 
    public void DeleteAllInstance()
    {

    }
    ///////////////////////////////  Write 
  

    public void ChangeTagButtonText()
    {
        if(newWriteTagText.text == "Love")
        {
            newWriteTagText.text = "All";
            newWriteTagText.fontSize = 60;
        }
        else
        {
            newWriteTagText.text = "Love";
            newWriteTagText.fontSize = 74;
        }
    }
    public void PublishButton()
    {
        StartCoroutine(NewWritePublish());
    }  
    public IEnumerator NewWritePublish()  //add Data to Database
    {
        if(string.IsNullOrWhiteSpace(newWriteContent.text) || string.IsNullOrWhiteSpace(newWriteTitleText.text) ) 
        {
            ImageFlicker(newWritePublishButton.image, flickerTime, flickerLongTime);

            if(string.IsNullOrWhiteSpace(newWriteContent.text))
            {
                newWriteContent.text.Remove(0, newWriteContent.text.Length);
                newWriteContent.text = "";
                boardControlScript.WarningBlankContent();
            }
         
            if(string.IsNullOrWhiteSpace(newWriteTitleText.text))
            {
                newWriteTitleText.text.Remove(0, newWriteTitleText.text.Length);
                newWriteTitleText.text = "";
                boardControlScript.WarningBlankTitle();
            }

            yield break;
        }

        goldBombCount = 70;
        newWritePublishButton.interactable = false;

        // Save Data
        // show WrittenBoard() --> My Writtenboard; 
        New_Writing newWriting = new New_Writing();
        newWriting.writerUid = currentPlayerUid;
        newWriting.writerNickname = currentPlayerNickName;
        newWriting.content = newWriteContent.text;
        newWriting.title = newWriteTitleText.text;
        newWriting.tag = newWriteTagText.text;
        newWriting.thumbsUpCount = 0;
        string json = JsonUtility.ToJson(newWriting);

        GoogleSignInDemo.Instance.databaseRef.Child("Boards").Push().SetRawJsonValueAsync(json).ContinueWithOnMainThread(task =>
        {
            if(task.IsCanceled)
            {
                Debug.LogError("SaveNewWriteFailed");
            }
            else if (task.IsFaulted)
            {
                Debug.LogError("SaveNewWriteFailed");
            }
            else
            {
                Debug.Log("SetData");
               
                newWriteCheck = true;
            }
        });

        currentGoldCount += 100;
        SetGold();
        StartCoroutine(GoldBomb(newWritePublishButton.transform.position));

        while (!newWriteCheck) yield return null;

        newWriteContent.text.Remove(0, newWriteContent.text.Length);
        newWriteContent.text = "";
        newWriteTitleText.text.Remove(0, newWriteTitleText.text.Length);
        newWriteTitleText.text = "";


        newWritePublishButton.interactable = true;
        newWriteCheck = false;
        boardControlScript.ClosePanels();
        boardControlScript.bulletinBoardPanel.SetActive(true);

      
        bool coolTime = true;
        boardControlScript.newWritingActiveButton.interactable = false;
        boardControlScript.newWritingActiveImage.fillAmount = 0;






        while (coolTime)
        {
            newWriteTimer += Time.deltaTime;
          
                boardControlScript.newWritingActiveImage.fillAmount = newWriteTimer / newWriteLimit;
                if (newWriteTimer >= newWriteLimit)
                {
                    boardControlScript.newWritingActiveButton.interactable = true;
                    coolTime = false;
                    newWriteTimer = 0;
                    boardControlScript.newWritingActiveImage.fillAmount = 1;
                }        

            yield return null;
        }





        //if (!writtenBoard.activeInHierarchy) writtenBoard.SetActive(true);
        //writtenBoard_titleText.text = newWriting.title;
        //writtenBoard_tagText.text = newWriting.tag;
        //writtenBoard_contentText.text = newWriting.content;
        //writtenBoard_titleText.text.Remove(0, writtenBoard_titleText.text.Length);
        //writtenBoard_contentText.text.Remove(0, writtenBoard_contentText.text.Length);
    }
    public void NewCommentWriteButton()  //add Data to Database
    {


    }

    public void OnThumbsUpButtonClick ()
    {
        goldBombCount = 5;
        BoardsThumbsUpButton(writtenBoardScript.currentButtonScript);
    }
    public void BoardsThumbsUpButton(Instance_button buttonScript)  // Use Transaction 
    {

        GoogleSignInDemo.Instance.databaseRef.Child("Boards").Child(buttonScript.uniqueKey).Child("thumbsUpCount").RunTransaction(mutableData =>
        {

            long count = 0;

          
            if (mutableData.Child("ThumbsUpCountValue").Value == null  || mutableData.Child("ThumbsUpCountValue").Value.ToString() =="")   
            {
                count = 0;                        
            }
            else
            {
                long.TryParse(mutableData.Child("ThumbsUpCountValue").Value.ToString(), out long result);
                count = result;
            }
           
            Dictionary<string, object> thumbsUpCount = new Dictionary<string, object>();

            thumbsUpCount["ThumbsUpCountValue"] = count + 1;

            mutableData.Value = thumbsUpCount;
            return TransactionResult.Success(mutableData);
          
        }).ContinueWith(task =>
        {
            if (task.IsCompleted)
            {
               
            }
            else
            {
                
            }
        });

        currentGoldCount += 5;
        SetGold();
        StartCoroutine(GoldBomb(thumbsUpButton.transform.position));
  
    }  
    ///////////////////////// Comment 
    public void OnClickCommentPushButton()
    { 
        
        StartCoroutine(CommentPushButton());
       
    }
    public IEnumerator CommentPushButton() 
    {
         NewComment NewCommentValue = new NewComment();
        if (!string.IsNullOrWhiteSpace(commentInputField.text))
        {
            NewCommentValue = new NewComment();
            NewCommentValue.CommentUid = currentPlayerUid;
            NewCommentValue.CommentNickname = currentPlayerNickName;
            NewCommentValue.Contents = commentInputField.text;
            NewCommentValue.Date = DateTime.Now.ToString("MM/dd/yy");
            NewCommentValue.ThumbsUpCount = 0;
        }
        else // Check WhiteSpace 
        {
            commentInputField.text.Remove(0, commentInputField.text.Length);
            commentInputField.text = "";
            StartCoroutine(ImageFlicker(commentInputField.image, flickerTime, flickerLongTime));
            boardControlScript.WarningBlankContent();
            yield break;
        }

        goldBombCount = 30;
        StartCoroutine(GoldBomb(commentPushButton.transform.position));


        string json = JsonUtility.ToJson(NewCommentValue);
        GoogleSignInDemo.Instance.databaseRef.Child("Boards").Child(writtenBoardScript.currentWrittenBoard_uniqueKey).Child("Comments").Push().SetRawJsonValueAsync(json).ContinueWith(task =>
        {
            if( task.IsCompleted)
            {
                currentGoldCount += 100;
                SetGold();
            
            }
            else
            {

            }
        });

       float timer = 0;
        bool coolTime = true;
       commentPushButton.interactable = false;
       commentPushImage.fillAmount = 0;

        while (coolTime)
        {
            timer += Time.deltaTime;
            commentPushImage.fillAmount = timer/commentLimitTime ;
            if(timer >= commentLimitTime)
            {
               commentPushButton.interactable = true;
                coolTime = false;
            }
           
           yield return null;
        }
      
    }

    public IEnumerator ImageFlicker(Image image, float flickerTime , float flickerLong)
    {
        Color currentColor = image.color;
        Color flickerColor = new Color(1, 0, 0);

        float time = 0;
        bool flickerDone = false;
  

        while(!flickerDone)
        {
            time += flickerTime;
            image.color = flickerColor;
            yield return new WaitForSeconds(flickerTime);
            image.color = currentColor;
            if (time > flickerLong)
            {
                flickerDone = true;
            }
        }

        image.color = currentColor;
    }


    /////////////  <--- Real Task


    public void GetGoldFromData ()
    {
        GoogleSignInDemo.Instance.databaseRef.Child("User").Child(currentPlayerUid).Child("Inventory").Child("Money").GetValueAsync().ContinueWithOnMainThread(task =>
        {
            DataSnapshot snapShot = task.Result;
    
            if (int.TryParse(snapShot.Child("GemCount").Value.ToString(), out int gemCount))
            {
                currentGemCount = gemCount;
                characterProfileGemText.text = currentGemCount.ToString();
                storeGemText.text = currentGemCount.ToString();
            }
            else
            {
                print("Gem Tryparse Failed");
            }
           

            if( int.TryParse(snapShot.Child("GoldCount").Value.ToString() , out int goldCount))
            {
                currentGoldCount = goldCount;
                characterProfileGoldText.text = currentGoldCount.ToString();
                storeGoldText.text = currentGoldCount.ToString();
            }
            else
            {
                print("Gold Tryparse Failed");
            }
        });



    }
 
    public void SetGold(  )
    {
        UserGoldData goldJson = new UserGoldData();
        goldJson.GemCount = currentGemCount;
        goldJson.GoldCount = currentGoldCount;
        string newGold = JsonUtility.ToJson(goldJson);

        GoogleSignInDemo.Instance.databaseRef.Child("User").Child(currentPlayerUid).Child("Inventory").Child("Money").SetRawJsonValueAsync(newGold).ContinueWith(task =>
        {

        });
    }
   


    public IEnumerator GoldBomb(Vector3 pos)
    {
        List<GameObject> golds = new List<GameObject>();  

        for ( int x = 0; x <goldBombCount; x++  )
        {
            
            goldEzPool.TryGetNextObject(pos, Quaternion.identity, out GameObject goldInstance);
            
            goldInstance.transform.parent = goldParent;
            golds.Add(goldInstance);
            
            //Rigidbody2D rb = gold.GetComponent<Rigidbody2D>();
            //rb.AddForce(new Vector2(UnityEngine.Random.Range(100 ,200), UnityEngine.Random.Range(100, 300)));
            //rb.AddTorque(UnityEngine.Random.Range(500, 1000));

            goldInstance.transform.DOMove(new Vector3(pos.x + UnityEngine.Random.Range(-250,250), pos.y + UnityEngine.Random.Range(-300,300),0), 1f).SetEase(Ease.OutExpo);
           
        }
        yield return new WaitForSeconds(1.5f);
  
        for(int x=0; x<golds.Count; x++)
        {
            golds[x].transform.DOMove(boardControlScript.characterMenuButton.transform.position, 0.32f);
            yield return new WaitForSeconds(0.05f);
        }
        

    }


}

