﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Firebase.Extensions;
using Firebase.Database;
using UnityEngine.EventSystems;
using EZObjectPools;


public class DressManager : MonoBehaviour
{

    EZObjectPool dressRoomSlotsPoolScript;
    StoreManager storeManagerScript;


    [Header("Panels")]
    public GameObject dressRoomPanel;


    [Header("Inventory")]
    public List<string> inventoryItems = new List<string>();



    [Header("DressRoomPanel CharacterImages")]
    public Image dressRoom_baseCharacterImage;
    public Image dressRoom_hairImage;
    public Image dressRoom_bodyImage;
    public Image dressRoom_eyesImage;
    public Image dressRoom_backImage;
    public Image dressRoom_earsImage;
    public Image dressRoom_mouthImage;
    public Image dressRoom_specialImage;
    public Image dressRoom_backgroundImage;


    [Header("DressRoomPanel Equipped items Icon Image")]
    public Image dressRoom_equipped_baseItemIcon_image;
    public Image dressRoom_equipped_hairItemIcon_image;
    public Image dressRoom_equipped_bodyItemIcon_image;
    public Image dressRoom_equipped_eyesItemIcon_image;
    public Image dressRoom_equipped_backItemIcon_image;
    public Image dressRoom_equipped_earsItemIcon_image;
    public Image dressRoom_equipped_mouthItemIcon_image;
    public Image dressRoom_equipped_specialItemIcon_image;
    public Image dressRoom_equipped_backgroundItemIcon_image;

    [Header ("Equipped Images Parents")]
    public GameObject baseImageParent;
    public GameObject hairImageParent;
    public GameObject bodyImageParent;
    public GameObject eyesImageParent;
    public GameObject earsImageParent;
    public GameObject backImageParent;
    public GameObject mouthImageParent;
    public GameObject specialImageParent;
    public GameObject backgroundImageParent;

    public GameObject equippedItemInfoPanel;



    [Header("DressRoom Tap Buttons")]
    public Button bodySlotsTapButton;
    public Button hairSlotsTapButton;
    public Button accessorySlotsTapButton;
    public Button backgroundSlotsTapButton;
    public Button specialSlotsTapButton;
    public Button baseSlotsTapButton;
    public Button[] tapButtons;
    public Image tapSelectFocusImage;



    [Header("Scroll View")]
    public GameObject body_scrollView;
    public GameObject hair_scrollView;
    public GameObject accessory_scrollView;
 //   public GameObject background_scrollView;
    public GameObject special_scrollView;
    public GameObject base_scrollView;
    public GameObject[] views;

   

    [Header("SelectedSlotScript")]
    public string selectedSlotsItemString;

   
    [Header("Current Equipped Items")]  //// Set itemCode  
    public string currentBaseCharacter_dressRoom;
    public string currentHairItem_dressRoom;
    public string currentBodyItem_dressRoom;
    public string currentEyesItem_dressRoom;
    public string currentEarsItem_dressRoom;
    public string currentMouthItem_dressRoom;
    public string currentSpecialItem_dressRoom;
    public string currentBackItem_dressRoom;
    public string currentBackgroundItem_dressRoom;
    bool settingCurrentItemsIsOver = false;
     


    [Header("CharacterPanel CharacterImages")]
    public Image characterPanel_backgroundImage;
    public Image characterPanel_baseImage;
    public Image characterPanel_hairImage;
    public Image characterPanel_bodyImage;
    public Image characterPanel_eyesImage;
    public Image characterPanel_backImage;
    public Image characterPanel_earsImage;
    public Image characterPanel_mouthImage;
    public Image characterPanel_specialImage;


    [Header("Last My Equipment Data")]    // Get itemCode
    public string last_MyBaseCharacterString;
    public string last_MyHairItemString;
    public string last_MyBodyItemString;
    public string last_MyEyesItemString;
    public string last_MyEarsItemString;
    public string last_MyMouthItemString;
    public string last_MySpecialItemString;
    public string last_MyBackItemString;
    public string last_MyBackgroundItemString;
    public bool getLastEquipmentData = false;


    [Header("SaveButton")]
    public Button saveButton;


    [Header("Slots List")]
    public List<GameObject> baseSlotsList = new List<GameObject>();
    public List<GameObject> hairSlotsList = new List<GameObject>();
    public List<GameObject> bodySlotsList = new List<GameObject>();

    public List<GameObject> AccessorySlotsList = new List<GameObject>();
   
    public List<GameObject> specialSlotsList = new List<GameObject>();

    public List<GameObject> backgroundSlotsList = new List<GameObject>();
   

    public GameObject glowEffectToHairSlot;
    public GameObject glowEffectToBodySlot;
    public GameObject glowEffectToEyesSlot;
    public GameObject glowEffectToEarsSlot;
    public GameObject glowEffectToMouthSlot;
    public GameObject glowEffectToSpecialSlot;
    public GameObject glowEffectToBackgroundSlot;
    public GameObject glowEffectToBackSlot;
    public GameObject glowEffectToBaseSlot;

    public GameObject[] glows;


    private void Awake()
    {
        // dressRoomSlotsPoolScript = GameObject.Find("DressRoomSlotsPool").GetComponent<EZObjectPool>();
        // storeRoomSlotsPoolScript = GameObject.Find("StoreRoomSlotsPool").GetComponent<EZObjectPool>();
          storeManagerScript = FindObjectOfType<StoreManager>();
      
    }

    private void Start()
    {
        GetItemsList();
        StartCoroutine(DressDataToImageComponent());
             
       GoogleSignInDemo.Instance.databaseRef.Child("User").Child(GoogleSignInDemo.Instance.firebaseUser.UserId).Child("Inventory")
       .Child("Equipment").ChildChanged += Equipment_ChildChanged;

        GoogleSignInDemo.Instance.databaseRef.Child("User").Child(GoogleSignInDemo.Instance.firebaseUser.UserId).Child("Inventory").Child("Items").
          ChildAdded += DressRoomInventoryInstance;

        GoogleSignInDemo.Instance.databaseRef.Child("User").Child(GoogleSignInDemo.Instance.firebaseUser.UserId).Child("Inventory").Child("Items").
            ChildChanged += ItemsChanged;
 
        GoogleSignInDemo.Instance.databaseRef.Child("User").Child(GoogleSignInDemo.Instance.firebaseUser.UserId).Child("Inventory").Child("Equipment").
           ChildAdded += EquipmentChildAdded;
     
    }


    public void GetItemsList ()
    {
        GoogleSignInDemo.Instance.databaseRef.Child("User").Child(GoogleSignInDemo.Instance.firebaseUser.UserId).Child("Inventory").Child("Items")
            .GetValueAsync().ContinueWithOnMainThread(task =>
            {
                if( task.IsCompleted)
                {
                    DataSnapshot snapShot = task.Result;
      
                    foreach ( var part in snapShot.Children)
                    {
                       foreach (var item in part.Children )
                        {
                            if (!inventoryItems.Contains(item.Key))
                            {
                                inventoryItems.Add(item.Key);
                            }
                        }
                    }
                }

            });
    }
    private void ItemsChanged(object sender, ChildChangedEventArgs e)
    {
        print("changed");
        print(e.Snapshot.Key);
        if (e.Snapshot.Key == "Base")
        {
            foreach (var item in e.Snapshot.Children)
            {
                if (!inventoryItems.Contains(item.Key))
                {
                    inventoryItems.Add(item.Key);
                    DressRoomSlot_instanceScript slotScript = GetEmptySlotScript(baseSlotsList);
                    slotScript.itemKey = "Base";
                    slotScript.itemCode = item.Key;
                    slotScript.gameObject.GetComponent<Image>().sprite = ItemManager.Instance.baseCharacterIconDic[item.Key] as Sprite;
                    
                }
          
            }

        }
        else if (e.Snapshot.Key == "Hair")
        {
            foreach (var item in e.Snapshot.Children)
            {
                if (!inventoryItems.Contains(item.Key))
                {
                    inventoryItems.Add(item.Key);
                    DressRoomSlot_instanceScript slotScript = GetEmptySlotScript(hairSlotsList);
                    slotScript.itemKey = "Hair";
                    slotScript.itemCode = item.Key;
                    slotScript.gameObject.GetComponent<Image>().sprite = ItemManager.Instance.hairIconDic[item.Key] as Sprite;
                }
            }

        }
        else if (e.Snapshot.Key == "Body")
        {
            foreach (var item in e.Snapshot.Children)
            {
                if (!inventoryItems.Contains(item.Key))
                {
                    inventoryItems.Add(item.Key);
                    DressRoomSlot_instanceScript slotScript = GetEmptySlotScript(bodySlotsList);
                    slotScript.itemKey = "Body";
                    slotScript.itemCode = item.Key;
                    slotScript.gameObject.GetComponent<Image>().sprite = ItemManager.Instance.bodyIconDic[item.Key] as Sprite;
                }
            }
        }
        else if (e.Snapshot.Key == "Ears")
        {
            foreach (var item in e.Snapshot.Children)
            {
                if (!inventoryItems.Contains(item.Key))
                {
                    inventoryItems.Add(item.Key);
                    DressRoomSlot_instanceScript slotScript = GetEmptySlotScript(AccessorySlotsList);
                    slotScript.itemKey = "Ears";
                    slotScript.itemCode = item.Key;
                    slotScript.gameObject.GetComponent<Image>().sprite = ItemManager.Instance.earsIconDic[item.Key] as Sprite;
                }
            }
            
        }
        else if (e.Snapshot.Key == "Eyes")
        {
            foreach (var item in e.Snapshot.Children)
            {
                if (!inventoryItems.Contains(item.Key))
                {
                    inventoryItems.Add(item.Key);
                    DressRoomSlot_instanceScript slotScript = GetEmptySlotScript(AccessorySlotsList);
                    slotScript.itemKey = "Eyes";
                    slotScript.itemCode = item.Key;
                    slotScript.gameObject.GetComponent<Image>().sprite = ItemManager.Instance.eyesIconDic[item.Key] as Sprite;
                }
            }
        }
        else if (e.Snapshot.Key == "Mouth")
        {
            foreach (var item in e.Snapshot.Children)
            {
                if (!inventoryItems.Contains(item.Key))
                {
                    inventoryItems.Add(item.Key);
                    DressRoomSlot_instanceScript slotScript = GetEmptySlotScript(AccessorySlotsList);
                    slotScript.itemKey = "Mouth";
                    slotScript.itemCode = item.Key;
                    slotScript.gameObject.GetComponent<Image>().sprite = ItemManager.Instance.mouthIconDic[item.Key] as Sprite;
                }
            }
        }
        else if (e.Snapshot.Key == "Special")
        {
            foreach (var item in e.Snapshot.Children)
            {
                if (!inventoryItems.Contains(item.Key))
                {
                    inventoryItems.Add(item.Key);
                    DressRoomSlot_instanceScript slotScript = GetEmptySlotScript(specialSlotsList);
                    slotScript.itemKey = "Special";
                    slotScript.itemCode = item.Key;
                    slotScript.gameObject.GetComponent<Image>().sprite = ItemManager.Instance.specialIconDic[item.Key] as Sprite;
                }
            }

        }
        else if (e.Snapshot.Key == "Back")
        {
            foreach (var item in e.Snapshot.Children)
            {
                if (!inventoryItems.Contains(item.Key))
                {
                    inventoryItems.Add(item.Key);
                    DressRoomSlot_instanceScript slotScript = GetEmptySlotScript(AccessorySlotsList);
                    slotScript.itemKey = "Back";
                    slotScript.itemCode = item.Key;
                    slotScript.gameObject.GetComponent<Image>().sprite = ItemManager.Instance.backItemIconDic[item.Key] as Sprite;
                }
            }

        }
        //else if ( e.Snapshot.Key == "Background")
        //{
        //    foreach (var item in e.Snapshot.Children)
        //    { if (!inventoryItems.Contains(item.Key))
            //  {
            //      inventoryItems.Add(item.Key);
            //        DressRoomSlot_instanceScript slotScript = GetEmptySlotScript(backgroundSlotsList);
            //        slotScript.itemCode = item.Key;
            //        slotScript.gameObject.GetComponent<Image>().sprite = ItemManager.Instance.specialIconDic[item.Key] as Sprite;
            //    }

            //} 

        }
    private void EquipmentChildAdded(object sender, ChildChangedEventArgs e)
    {

            if ( e.Snapshot.Key == "EquippedBaseCharacterCode")
            {
                dressRoom_equipped_baseItemIcon_image.sprite = ItemManager.Instance.baseCharacterIconDic[e.Snapshot.Value.ToString()] as Sprite;
            }
            else if (e.Snapshot.Key == "EquippedBackItem")
            {
                dressRoom_equipped_backItemIcon_image.sprite = ItemManager.Instance.backItemIconDic[e.Snapshot.Value.ToString()] as Sprite;
            }
            else if (e.Snapshot.Key == "EquippedBodyItem")
            {
                dressRoom_equipped_bodyItemIcon_image.sprite = ItemManager.Instance.bodyIconDic[e.Snapshot.Value.ToString()] as Sprite;
            }
            else if (e.Snapshot.Key == "EquippedHairItem")
            {
                dressRoom_equipped_hairItemIcon_image.sprite = ItemManager.Instance.hairIconDic[e.Snapshot.Value.ToString()] as Sprite;
            }
            else if (e.Snapshot.Key == "EquippedEyesItem")
            {
                dressRoom_equipped_eyesItemIcon_image.sprite = ItemManager.Instance.eyesIconDic[e.Snapshot.Value.ToString()] as Sprite;
            }
            else if (e.Snapshot.Key == "EquippedEarsItem")
            {
                dressRoom_equipped_earsItemIcon_image.sprite = ItemManager.Instance.earsIconDic[e.Snapshot.Value.ToString()] as Sprite;
            }
            else if (e.Snapshot.Key == "EquippedMouthItem")
            {
                dressRoom_equipped_mouthItemIcon_image.sprite = ItemManager.Instance.mouthIconDic[e.Snapshot.Value.ToString()] as Sprite;
            }
            else if (e.Snapshot.Key == "EquippedSpecialItem")
            {
                dressRoom_equipped_specialItemIcon_image.sprite = ItemManager.Instance.specialIconDic[e.Snapshot.Value.ToString()] as Sprite;
            }
            else if (e.Snapshot.Key == "EquippedBackgroundItem")
            {
                dressRoom_equipped_backgroundItemIcon_image.sprite = ItemManager.Instance.backgroundItemIconDic[e.Snapshot.Value.ToString()] as Sprite;
            }

         
    }
    private void DressRoomInventoryInstance(object sender, ChildChangedEventArgs e)
    {
       
        if( e.Snapshot.Key == "Base")
        {
            foreach (var item in e.Snapshot.Children)
            {
                DressRoomSlot_instanceScript slotScript = GetEmptySlotScript(baseSlotsList);
                slotScript.itemKey = "Base";
                slotScript.itemCode = item.Key;
                slotScript.gameObject.GetComponent<Image>().sprite = ItemManager.Instance.baseCharacterIconDic[item.Key] as Sprite;
            }

        }
        else if ( e.Snapshot.Key =="Hair")
        {
            foreach (var item in e.Snapshot.Children)
            {
                DressRoomSlot_instanceScript slotScript = GetEmptySlotScript(hairSlotsList);
                slotScript.itemKey = "Hair";
                slotScript.itemCode = item.Key;
                slotScript.gameObject.GetComponent<Image>().sprite = ItemManager.Instance.hairIconDic[item.Key] as Sprite;
            }

        }
        else if ( e.Snapshot.Key =="Body")
        {
            foreach (var item in e.Snapshot.Children)
            {
                DressRoomSlot_instanceScript slotScript = GetEmptySlotScript(bodySlotsList);
                slotScript.itemKey = "Body";
                slotScript.itemCode = item.Key;
                slotScript.gameObject.GetComponent<Image>().sprite = ItemManager.Instance.bodyIconDic[item.Key] as Sprite;
            }
        }
        else if ( e.Snapshot.Key == "Ears")
        {
            foreach (var item in e.Snapshot.Children)
            {
                DressRoomSlot_instanceScript slotScript = GetEmptySlotScript(AccessorySlotsList);
                slotScript.itemKey = "Ears";
                slotScript.itemCode = item.Key;
                slotScript.gameObject.GetComponent<Image>().sprite = ItemManager.Instance.earsIconDic[item.Key] as Sprite;
            }
        }

        else if (e.Snapshot.Key =="Eyes")
        {
            foreach (var item in e.Snapshot.Children)
            {
                DressRoomSlot_instanceScript slotScript = GetEmptySlotScript(AccessorySlotsList);
                slotScript.itemKey = "Eyes";
                slotScript.itemCode = item.Key;
                slotScript.gameObject.GetComponent<Image>().sprite = ItemManager.Instance.eyesIconDic[item.Key] as Sprite;
            }
        }

        else if ( e.Snapshot.Key =="Mouth")
        {
            foreach (var item in e.Snapshot.Children)
            {
                DressRoomSlot_instanceScript slotScript = GetEmptySlotScript(AccessorySlotsList);
                slotScript.itemKey = "Mouth";
                slotScript.itemCode = item.Key;
                slotScript.gameObject.GetComponent<Image>().sprite = ItemManager.Instance.mouthIconDic[item.Key] as Sprite;
            }
        }
        else if (e.Snapshot.Key =="Special")
        {
            foreach (var item in e.Snapshot.Children)
            {
                DressRoomSlot_instanceScript slotScript = GetEmptySlotScript(specialSlotsList);
                slotScript.itemKey = "Special";
                slotScript.itemCode = item.Key;
                slotScript.gameObject.GetComponent<Image>().sprite = ItemManager.Instance.specialIconDic[item.Key] as Sprite;
            }

        }
        else if ( e.Snapshot.Key == "Back")
        {
            foreach (var item in e.Snapshot.Children)
            {
                DressRoomSlot_instanceScript slotScript = GetEmptySlotScript(AccessorySlotsList);
                slotScript.itemKey = "Back";
                slotScript.itemCode = item.Key;
                slotScript.gameObject.GetComponent<Image>().sprite = ItemManager.Instance.backItemIconDic[item.Key] as Sprite;
            }

        }
        //else if ( e.Snapshot.Key == "Background")
        //{
        //    foreach (var item in e.Snapshot.Children)
        //    {
        //        DressRoomSlot_instanceScript slotScript = GetEmptySlotScript(backgroundSlotsList);
        //        slotScript.itemCode = item.Key;
        //        slotScript.gameObject.GetComponent<Image>().sprite = ItemManager.Instance.specialIconDic[item.Key] as Sprite;
        //    }

        //} 

    }
    public DressRoomSlot_instanceScript GetEmptySlotScript (List<GameObject>  slots)
    {
        for ( int x  =0; x <slots.Count; x++ )
        {
            DressRoomSlot_instanceScript slotScript = slots[x].GetComponent<DressRoomSlot_instanceScript>();

            if( string.IsNullOrEmpty(slotScript.itemCode))
            {

                return slotScript;
            } 
        }

        return null;
    }

    public void SetDressRoomEquipment()
    {
        dressRoom_baseCharacterImage.sprite = ItemManager.Instance.baseCharacterSpriteDic[last_MyBaseCharacterString] as Sprite;
        //dressRoom_equipped_baseItemIcon_image.sprite = ItemManager.Instance.baseCharacterIconDic[last_MyBaseCharacterString] as Sprite;
       // dressRoom_equipped_baseRank_image.sprite = ItemManager.Instance.baseCharacterRankDic[last_MyBaseCharacterString] as Sprite;

        dressRoom_hairImage.sprite =  ItemManager.Instance.hairItemsDic[last_MyHairItemString] as Sprite;
       // dressRoom_equipped_hairItemIcon_image.sprite = ItemManager.Instance.hairIconDic[last_MyHairItemString] as Sprite;
       // dressRoom_equipped_hairRankIcon_image.sprite = ItemManager.Instance.hairRankDic[last_MyHairItemString] as Sprite;

        dressRoom_bodyImage.sprite = ItemManager.Instance.bodyItemsDic[last_MyBodyItemString] as Sprite;
       // dressRoom_equipped_bodyItemIcon_image.sprite = ItemManager.Instance.bodyIconDic[last_MyBodyItemString] as Sprite;
        //dressRoom_equipped_bodyRankIcon_image.sprite = ItemManager.Instance.bodyRankDic[currentBodyItem_dressRoom] as Sprite;

        dressRoom_eyesImage.sprite= ItemManager.Instance.eyesItemsDic[last_MyEyesItemString] as Sprite;
        //dressRoom_equipped_eyesItemIcon_image.sprite = ItemManager.Instance.eyesIconDic[last_MyEyesItemString] as Sprite;
        //dressRoom_equipped_eyesRankIcon_image.sprite = ItemManager.Instance.eyesRankDic[currentEyesItem_dressRoom] as Sprite;

        dressRoom_backImage.sprite = ItemManager.Instance.backItemDic[last_MyBackItemString] as Sprite;
      //  dressRoom_equipped_backItemIcon_image.sprite = ItemManager.Instance.backItemIconDic[last_MyBackItemString] as Sprite;
      //  dressRoom_equipped_backRankIcon_image.sprite = ItemManager.Instance.backRankDic[currentBackItem_dressRoom] as Sprite;

        dressRoom_earsImage.sprite = ItemManager.Instance.earsItemsDic[last_MyEarsItemString] as Sprite;
      //  dressRoom_equipped_earsItemIcon_image.sprite = ItemManager.Instance.earsIconDic[last_MyEarsItemString] as Sprite;
      //  dressRoom_equipped_earsRankIcon_image.sprite = ItemManager.Instance.earsRankDic[currentEarsItem_dressRoom] as Sprite;

        dressRoom_mouthImage.sprite = ItemManager.Instance.mouthItemsDic[last_MyMouthItemString] as Sprite;
       // dressRoom_equipped_mouthItemIcon_image.sprite = ItemManager.Instance.mouthItemsDic[last_MyMouthItemString] as Sprite;
      //  dressRoom_equipped_mouthRankIcon_image.sprite = ItemManager.Instance.mouthRankDic[currentMouthItem_dressRoom] as Sprite;

        dressRoom_specialImage.sprite = ItemManager.Instance.specialItemsDic[last_MySpecialItemString] as Sprite;
      //  dressRoom_equipped_specialItemIcon_image.sprite = ItemManager.Instance.specialIconDic[last_MySpecialItemString] as Sprite;
      //  dressRoom_equipped_specialRankIcon_image.sprite = ItemManager.Instance.specialRankDic[currentSpecialItem_dressRoom] as Sprite;

        dressRoom_backgroundImage.sprite = ItemManager.Instance.backgroundItemDic[last_MyBackgroundItemString] as Sprite;
      //  dressRoom_equipped_backgroundItemIcon_image.sprite = ItemManager.Instance.backgroundItemIconDic[last_MyBackgroundItemString] as Sprite;
       // dressRoom_equipped_backgroundRankIcon_image.sprite = ItemManager.Instance.backgroundRankDic[currentBackgroundItem_dressRoom] as Sprite;
    }
    public void InventoryChangingListner()
    {
        ////// Setting Instance of AllItems
        //  GoogleSignInDemo.Instance.databaseRef.Child("User").Child(GoogleSignInDemo.Instance.firebaseUser.UserId).Child("Inventory")
        //    .Child("AllItems").Child("HairItems").ChildAdded += SetHairItems_ChildAdded;
      
        //  GoogleSignInDemo.Instance.databaseRef.Child("User").Child(GoogleSignInDemo.Instance.firebaseUser.UserId).Child("Inventory")
        //     .Child("AllItems").ChildChanged += AllItems_ChildChanged;
    }


    private void Equipment_ChildChanged(object sender, ChildChangedEventArgs e)
    {
       

         if (e.Snapshot.Key == "EquippedBaseCharacterCode")
        {
            characterPanel_baseImage.sprite = ItemManager.Instance.baseCharacterSpriteDic[e.Snapshot.Value.ToString()] as Sprite;
            dressRoom_baseCharacterImage.sprite = ItemManager.Instance.baseCharacterSpriteDic[e.Snapshot.Value.ToString()] as Sprite;
            storeManagerScript.storePanel_baseImage.sprite = ItemManager.Instance.baseCharacterSpriteDic[e.Snapshot.Value.ToString()] as Sprite;
            dressRoom_equipped_baseItemIcon_image.sprite = ItemManager.Instance.baseCharacterIconDic[e.Snapshot.Value.ToString()] as Sprite;
        }
         else if (e.Snapshot.Key == "EquippedBackItem")
        {
            characterPanel_backImage.sprite = ItemManager.Instance.backItemDic[e.Snapshot.Value.ToString()] as Sprite;
            dressRoom_backImage.sprite = ItemManager.Instance.backItemDic[e.Snapshot.Value.ToString()] as Sprite;
            storeManagerScript.storePanel_backImage.sprite = ItemManager.Instance.backItemDic[e.Snapshot.Value.ToString()] as Sprite;
            dressRoom_equipped_backItemIcon_image.sprite = ItemManager.Instance.backItemIconDic[e.Snapshot.Value.ToString()] as Sprite;
        }
         else if (e.Snapshot.Key == "EquippedBodyItem")
        {
            characterPanel_bodyImage.sprite = ItemManager.Instance.bodyItemsDic[e.Snapshot.Value.ToString()] as Sprite;
            dressRoom_bodyImage.sprite = ItemManager.Instance.bodyItemsDic[e.Snapshot.Value.ToString()] as Sprite;
             storeManagerScript.storePanel_bodyImage.sprite = ItemManager.Instance.bodyItemsDic[e.Snapshot.Value.ToString()] as Sprite;
            dressRoom_equipped_bodyItemIcon_image.sprite = ItemManager.Instance.bodyIconDic[e.Snapshot.Value.ToString()] as Sprite;
 
        }
         else if (e.Snapshot.Key =="EquippedHairItem")
        {
            characterPanel_hairImage.sprite = ItemManager.Instance.hairItemsDic[e.Snapshot.Value.ToString()] as Sprite;
             dressRoom_hairImage.sprite = ItemManager.Instance.hairItemsDic[e.Snapshot.Value.ToString()] as Sprite;
            storeManagerScript.storePanel_hairImage.sprite = ItemManager.Instance.hairItemsDic[e.Snapshot.Value.ToString()] as Sprite;
           dressRoom_equipped_hairItemIcon_image.sprite = ItemManager.Instance.hairIconDic[e.Snapshot.Value.ToString()] as Sprite;
        }
        else if ( e.Snapshot.Key =="EquippedEyesItem")
        {
            characterPanel_eyesImage.sprite = ItemManager.Instance.eyesItemsDic[e.Snapshot.Value.ToString()] as Sprite;
            dressRoom_eyesImage.sprite = ItemManager.Instance.eyesItemsDic[e.Snapshot.Value.ToString()] as Sprite;
             storeManagerScript.storePanel_eyesImage.sprite = ItemManager.Instance.eyesItemsDic[e.Snapshot.Value.ToString()] as Sprite;
           dressRoom_equipped_eyesItemIcon_image.sprite = ItemManager.Instance.eyesIconDic[e.Snapshot.Value.ToString()] as Sprite;
        }
         else if ( e.Snapshot.Key =="EquippedEarsItem")
        {
            characterPanel_earsImage.sprite = ItemManager.Instance.earsItemsDic[e.Snapshot.Value.ToString()] as Sprite;
            dressRoom_earsImage.sprite = ItemManager.Instance.earsItemsDic[e.Snapshot.Value.ToString()] as Sprite;
             storeManagerScript.storePanel_earsImage.sprite = ItemManager.Instance.earsItemsDic[e.Snapshot.Value.ToString()] as Sprite;
           dressRoom_equipped_earsItemIcon_image.sprite = ItemManager.Instance.earsIconDic[e.Snapshot.Value.ToString()] as Sprite;  
        }
         else if ( e.Snapshot.Key =="EquippedMouthItem")
        {
            characterPanel_mouthImage.sprite = ItemManager.Instance.mouthItemsDic[e.Snapshot.Value.ToString()] as Sprite;
            dressRoom_mouthImage.sprite = ItemManager.Instance.mouthItemsDic[e.Snapshot.Value.ToString()] as Sprite;
            storeManagerScript.storePanel_mouthImage.sprite = ItemManager.Instance.mouthItemsDic[e.Snapshot.Value.ToString()] as Sprite;
           dressRoom_equipped_mouthItemIcon_image.sprite = ItemManager.Instance.mouthIconDic[e.Snapshot.Value.ToString()] as Sprite;
        }
         else if (e.Snapshot.Key =="EquippedSpecialItem")
        {
            characterPanel_specialImage.sprite = ItemManager.Instance.specialItemsDic[e.Snapshot.Value.ToString()] as Sprite;
            dressRoom_specialImage.sprite = ItemManager.Instance.specialItemsDic[e.Snapshot.Value.ToString()] as Sprite;
            storeManagerScript.storePanel_specialImage.sprite = ItemManager.Instance.specialItemsDic[e.Snapshot.Value.ToString()] as Sprite;
            dressRoom_equipped_specialItemIcon_image.sprite = ItemManager.Instance.specialIconDic[e.Snapshot.Value.ToString()] as Sprite;
         }
         else if (e.Snapshot.Key =="EquippedBackgroundItem")
        {
            characterPanel_backgroundImage.sprite = ItemManager.Instance.backgroundItemDic[e.Snapshot.Value.ToString()] as Sprite;
            dressRoom_backgroundImage.sprite = ItemManager.Instance.backgroundItemDic[e.Snapshot.Value.ToString()] as Sprite;
            storeManagerScript.storePanel_specialImage.sprite = ItemManager.Instance.backgroundItemDic[e.Snapshot.Value.ToString()] as Sprite;
            dressRoom_equipped_backgroundItemIcon_image.sprite = ItemManager.Instance.backgroundItemIconDic[e.Snapshot.Value.ToString()] as Sprite; 
        }
    }
    public void SetCurrentItemCode ()
    {
        currentBaseCharacter_dressRoom = last_MyBaseCharacterString;
        currentHairItem_dressRoom = last_MyHairItemString;
        currentBodyItem_dressRoom = last_MyBodyItemString;
        currentEyesItem_dressRoom = last_MyEyesItemString;
        currentEarsItem_dressRoom = last_MyEarsItemString;
        currentMouthItem_dressRoom = last_MyMouthItemString;
        currentSpecialItem_dressRoom = last_MySpecialItemString;
        currentBackItem_dressRoom = last_MyBackItemString;
        currentBackgroundItem_dressRoom = last_MyBackgroundItemString;
    }

    public IEnumerator GetMyCharacterLastEquipmentData ()
    {
       while (!DataBaseManager.Instance.firstRegisterScript.nicknameIsExisted || !ItemManager.Instance.ItemSettingIsOver ) {  yield return null; }

      GoogleSignInDemo.Instance.databaseRef.Child("User").Child(GoogleSignInDemo.Instance.firebaseUser.UserId).Child("Inventory").Child("Equipment").GetValueAsync().ContinueWithOnMainThread(task =>
        {
            if(task.IsCompleted)
            {
                DataSnapshot snapShot = task.Result;
 
                foreach ( var item in snapShot.Children)
                {
                    if (item.Key == "EquippedBaseCharacterCode") last_MyBaseCharacterString = item.Value.ToString();
                    else if (item.Key == "EquippedHairItem") last_MyHairItemString = item.Value.ToString();
                    else if (item.Key == "EquippedBodyItem") last_MyBodyItemString = item.Value.ToString();
                    else if (item.Key == "EquippedEarsItem") last_MyEarsItemString = item.Value.ToString();
                    else if (item.Key == "EquippedEyesItem") last_MyEyesItemString = item.Value.ToString();
                    else if (item.Key == "EquippedMouthItem") last_MyMouthItemString = item.Value.ToString();
                    else if (item.Key == "EquippedBackItem") last_MyBackItemString = item.Value.ToString();
                    else if (item.Key == "EquippedSpecialItem") last_MySpecialItemString = item.Value.ToString();
                    else if (item.Key == "EquippedBackgroundItem") last_MyBackgroundItemString = item.Value.ToString();
                }
               
                SetCurrentItemCode();
                getLastEquipmentData = true;
            }
            else
            {

            }
        });

       

    }
    public IEnumerator DressDataToImageComponent ()
    {
        yield return StartCoroutine(GetMyCharacterLastEquipmentData());

        while (!getLastEquipmentData) { yield return null; }

         characterPanel_backgroundImage.sprite = ItemManager.Instance.backgroundItemDic[last_MyBackgroundItemString] as Sprite;
         dressRoom_backgroundImage.sprite = ItemManager.Instance.backgroundItemDic[last_MyBackgroundItemString] as Sprite;
         storeManagerScript.storePanel_backgroundImage.sprite = ItemManager.Instance.backgroundItemDic[last_MyBackgroundItemString] as Sprite;

        characterPanel_baseImage.sprite = ItemManager.Instance.baseCharacterSpriteDic[last_MyBaseCharacterString] as Sprite;
        dressRoom_baseCharacterImage.sprite = ItemManager.Instance.baseCharacterSpriteDic[last_MyBaseCharacterString] as Sprite;
         storeManagerScript.storePanel_baseImage.sprite = ItemManager.Instance.baseCharacterSpriteDic[last_MyBaseCharacterString] as Sprite;

        characterPanel_hairImage.sprite = ItemManager.Instance.hairItemsDic[last_MyHairItemString] as Sprite;
        dressRoom_hairImage.sprite = ItemManager.Instance.hairItemsDic[last_MyHairItemString] as Sprite;
        storeManagerScript.storePanel_hairImage.sprite = ItemManager.Instance.hairItemsDic[last_MyHairItemString] as Sprite;

        characterPanel_bodyImage.sprite = ItemManager.Instance.bodyItemsDic[last_MyBodyItemString] as Sprite;
        dressRoom_bodyImage.sprite = ItemManager.Instance.bodyItemsDic[last_MyBodyItemString] as Sprite;
        storeManagerScript.storePanel_bodyImage.sprite = ItemManager.Instance.bodyItemsDic[last_MyBodyItemString] as Sprite;

        characterPanel_eyesImage.sprite = ItemManager.Instance.eyesItemsDic[last_MyEyesItemString] as Sprite;
        dressRoom_eyesImage.sprite = ItemManager.Instance.eyesItemsDic[last_MyEyesItemString] as Sprite;
        storeManagerScript.storePanel_eyesImage.sprite = ItemManager.Instance.eyesItemsDic[last_MyEyesItemString] as Sprite;

        characterPanel_earsImage.sprite = ItemManager.Instance.earsItemsDic[last_MyEarsItemString] as Sprite;
        dressRoom_earsImage.sprite = ItemManager.Instance.earsItemsDic[last_MyEarsItemString] as Sprite;
        storeManagerScript.storePanel_earsImage.sprite = ItemManager.Instance.earsItemsDic[last_MyEarsItemString] as Sprite;

        characterPanel_mouthImage.sprite = ItemManager.Instance.mouthItemsDic[last_MyMouthItemString] as Sprite;
        dressRoom_mouthImage.sprite = ItemManager.Instance.mouthItemsDic[last_MyMouthItemString] as Sprite;
        storeManagerScript.storePanel_mouthImage.sprite = ItemManager.Instance.mouthItemsDic[last_MyMouthItemString] as Sprite;

        characterPanel_specialImage.sprite = ItemManager.Instance.specialItemsDic[last_MySpecialItemString] as Sprite;
        dressRoom_specialImage.sprite = ItemManager.Instance.specialItemsDic[last_MySpecialItemString] as Sprite;
        storeManagerScript.storePanel_specialImage.sprite = ItemManager.Instance.specialItemsDic[last_MySpecialItemString] as Sprite;

        characterPanel_backImage.sprite = ItemManager.Instance.backItemDic[last_MyBackItemString] as Sprite;
        dressRoom_backImage.sprite = ItemManager.Instance.backItemDic[last_MyBackItemString] as Sprite;
        storeManagerScript.storePanel_backImage.sprite = ItemManager.Instance.backItemDic[last_MyBackItemString] as Sprite;

    }
    public void OnClickPlasticSurgeryButton()
    {

    }
    
    public void OnClickItemSlot()
    {
        GameObject currentClickedSlot = EventSystem.current.currentSelectedGameObject;
        DressRoomSlot_instanceScript dressInstanceScript = currentClickedSlot.GetComponent<DressRoomSlot_instanceScript>();

        Image currentSlotItemImage = dressInstanceScript.gameObject.GetComponent<Image>();

        if (selectedSlotsItemString != dressInstanceScript.itemCode)
        {
            selectedSlotsItemString = dressInstanceScript.itemCode;
            if (dressInstanceScript.itemKey == "Hair")
            {
              if (!glowEffectToHairSlot.activeInHierarchy) glowEffectToHairSlot.SetActive(true);
                 glowEffectToHairSlot.transform.position = currentClickedSlot.transform.position;
                 glowEffectToHairSlot.transform.parent = currentClickedSlot.transform;
                currentHairItem_dressRoom = dressInstanceScript.itemCode;
                dressRoom_hairImage.sprite = ItemManager.Instance.hairItemsDic[currentHairItem_dressRoom] as Sprite;
   
                dressRoom_equipped_hairItemIcon_image.sprite = ItemManager.Instance.hairIconDic[currentHairItem_dressRoom] as Sprite;
                DressRoom_equipment_slot equipScript = dressRoom_equipped_hairItemIcon_image.gameObject.GetComponent<DressRoom_equipment_slot>();
                equipScript.equippedItemCode = currentHairItem_dressRoom;
                equipScript.equippedItemKey = dressInstanceScript.itemKey;

            }
            else if (dressInstanceScript.itemKey == "Body")
            {
                if (!glowEffectToBodySlot.activeInHierarchy) glowEffectToBodySlot.SetActive(true);
                glowEffectToBodySlot.transform.position = currentClickedSlot.transform.position;
                glowEffectToBodySlot.transform.parent = currentClickedSlot.transform;
                currentBodyItem_dressRoom = dressInstanceScript.itemCode;
                dressRoom_bodyImage.sprite = ItemManager.Instance.bodyItemsDic[currentBodyItem_dressRoom] as Sprite;
                      
                dressRoom_equipped_bodyItemIcon_image.sprite = ItemManager.Instance.bodyIconDic[currentBodyItem_dressRoom] as Sprite;            
                DressRoom_equipment_slot equipScript = dressRoom_equipped_bodyItemIcon_image.gameObject.GetComponent<DressRoom_equipment_slot>();        
                equipScript.equippedItemCode = currentBodyItem_dressRoom;
                equipScript.equippedItemKey = dressInstanceScript.itemKey;
            }
            else if (dressInstanceScript.itemKey == "Eyes")
            {
                if (!glowEffectToEyesSlot.activeInHierarchy) glowEffectToEyesSlot.SetActive(true);
                   glowEffectToEyesSlot.transform.position = currentClickedSlot.transform.position;
                glowEffectToEyesSlot.transform.parent = currentClickedSlot.transform;
                  currentEyesItem_dressRoom = dressInstanceScript.itemCode;
                  dressRoom_eyesImage.sprite = ItemManager.Instance.eyesItemsDic[currentEyesItem_dressRoom] as Sprite;
                 
                dressRoom_equipped_eyesItemIcon_image.sprite = ItemManager.Instance.eyesIconDic[currentEyesItem_dressRoom] as Sprite;             
                DressRoom_equipment_slot equipScript = dressRoom_equipped_eyesItemIcon_image.gameObject.GetComponent<DressRoom_equipment_slot>();
                equipScript.equippedItemCode = currentEyesItem_dressRoom;
                equipScript.equippedItemKey = dressInstanceScript.itemKey;

            }
            else if (dressInstanceScript.itemKey == "Ears")
            {
                 if (!glowEffectToEarsSlot.activeInHierarchy) glowEffectToEarsSlot.SetActive(true);
                    glowEffectToEarsSlot.transform.position = currentClickedSlot.transform.position;
                glowEffectToEarsSlot.transform.parent = currentClickedSlot.transform;
                   currentEarsItem_dressRoom = dressInstanceScript.itemCode;
                   dressRoom_earsImage.sprite = ItemManager.Instance.earsItemsDic[currentEarsItem_dressRoom] as Sprite;
                   
                dressRoom_equipped_earsItemIcon_image.sprite = ItemManager.Instance.earsIconDic[currentEarsItem_dressRoom] as Sprite;     
                DressRoom_equipment_slot equipScript = dressRoom_equipped_earsItemIcon_image.gameObject.GetComponent<DressRoom_equipment_slot>();
                equipScript.equippedItemCode = currentEarsItem_dressRoom;
                equipScript.equippedItemKey = dressInstanceScript.itemKey;

            }
            else if (dressInstanceScript.itemKey == "Mouth")
            {
                 if (!glowEffectToMouthSlot.activeInHierarchy) glowEffectToMouthSlot.SetActive(true);
                   glowEffectToMouthSlot.transform.position = currentClickedSlot.transform.position;
                glowEffectToMouthSlot.transform.parent = currentClickedSlot.transform;
                  currentMouthItem_dressRoom = dressInstanceScript.itemCode;
                 dressRoom_mouthImage.sprite = ItemManager.Instance.mouthItemsDic[currentMouthItem_dressRoom] as Sprite;
                
                dressRoom_equipped_mouthItemIcon_image.sprite = ItemManager.Instance.mouthIconDic[currentMouthItem_dressRoom] as Sprite;             
                DressRoom_equipment_slot equipScript = dressRoom_equipped_mouthItemIcon_image.gameObject.GetComponent<DressRoom_equipment_slot>();          
                equipScript.equippedItemCode = currentMouthItem_dressRoom;
                equipScript.equippedItemKey = dressInstanceScript.itemKey;

            }
            else if (dressInstanceScript.itemKey == "Back")
            {
               if (!glowEffectToBackSlot.activeInHierarchy) glowEffectToBackSlot.SetActive(true);
                  glowEffectToBackSlot.transform.position = currentClickedSlot.transform.position;
                 glowEffectToBackSlot.transform.parent = currentClickedSlot.transform;
                  currentBackItem_dressRoom = dressInstanceScript.itemCode;
                  dressRoom_backImage.sprite = ItemManager.Instance.backItemDic[currentBackItem_dressRoom] as Sprite;
               
                dressRoom_equipped_backItemIcon_image.sprite = ItemManager.Instance.backItemIconDic[currentBackItem_dressRoom] as Sprite;              
                DressRoom_equipment_slot equipScript = dressRoom_equipped_backItemIcon_image.gameObject.GetComponent<DressRoom_equipment_slot>();
                equipScript.equippedItemCode = currentBackItem_dressRoom;
                equipScript.equippedItemKey = dressInstanceScript.itemKey;
            }
            else if (dressInstanceScript.itemKey == "Background")
            {
               if  (!glowEffectToBackgroundSlot.activeInHierarchy) glowEffectToBackgroundSlot.SetActive(true);
                  glowEffectToBackgroundSlot.transform.position = currentClickedSlot.transform.position;
                glowEffectToBackgroundSlot.transform.parent = currentClickedSlot.transform;
                 currentBackgroundItem_dressRoom = dressInstanceScript.itemCode;
                 dressRoom_backgroundImage.sprite = ItemManager.Instance.backgroundItemDic[currentBackgroundItem_dressRoom] as Sprite;
                 
                dressRoom_equipped_backgroundItemIcon_image.sprite = ItemManager.Instance.backgroundItemIconDic[currentBackgroundItem_dressRoom] as Sprite;                
                DressRoom_equipment_slot equipScript = dressRoom_equipped_backgroundItemIcon_image.gameObject.GetComponent<DressRoom_equipment_slot>();
                equipScript.equippedItemCode = currentBackgroundItem_dressRoom;
                equipScript.equippedItemKey = dressInstanceScript.itemKey;
            }
            else if (dressInstanceScript.itemKey == "Special")
            {
              if (!glowEffectToSpecialSlot.activeInHierarchy) glowEffectToSpecialSlot.SetActive(true);
                 glowEffectToSpecialSlot.transform.position = currentClickedSlot.transform.position;
                glowEffectToSpecialSlot.transform.parent = currentClickedSlot.transform;
                currentSpecialItem_dressRoom = dressInstanceScript.itemCode;
                dressRoom_specialImage.sprite = ItemManager.Instance.specialItemsDic[currentSpecialItem_dressRoom] as Sprite;
                
                dressRoom_equipped_specialItemIcon_image.sprite = ItemManager.Instance.specialIconDic[currentSpecialItem_dressRoom] as Sprite;              
                DressRoom_equipment_slot equipScript = dressRoom_equipped_specialItemIcon_image.gameObject.GetComponent<DressRoom_equipment_slot>();
                equipScript.equippedItemCode = currentSpecialItem_dressRoom;
                equipScript.equippedItemKey = dressInstanceScript.itemKey;

            }
            else if ( dressInstanceScript.itemKey == "Base")
            {
                if (!glowEffectToBaseSlot.activeInHierarchy) glowEffectToBaseSlot.SetActive(true);
                glowEffectToBaseSlot.transform.position = currentClickedSlot.transform.position;
                glowEffectToBaseSlot.transform.parent = currentClickedSlot.transform;
               currentBaseCharacter_dressRoom = dressInstanceScript.itemCode;
                dressRoom_baseCharacterImage.sprite = ItemManager.Instance.baseCharacterSpriteDic[currentBaseCharacter_dressRoom] as Sprite;

                dressRoom_equipped_baseItemIcon_image.sprite = ItemManager.Instance.baseCharacterIconDic[currentBaseCharacter_dressRoom] as Sprite;
                DressRoom_equipment_slot equipScript = dressRoom_equipped_baseItemIcon_image.gameObject.GetComponent<DressRoom_equipment_slot>();
                equipScript.equippedItemCode = currentBaseCharacter_dressRoom;
                equipScript.equippedItemKey = dressInstanceScript.itemKey;
            }
        }
        else 
        {
            Debug.LogError("Already Equipped");
        }
  
    }
    public void OnClickEquipSlot ()
    {
        equippedItemInfoPanel.SetActive(true);

    }


    public void OnClickSaveButton()   {  StartCoroutine(SetMyEquipmentData());  }
    IEnumerator SetMyEquipmentData()
    {
        // last ----> nonEquipment
        // NonEquipment ---> delete
        // delete ---> Equipment
     
        DataBaseManager.UserCharacterInfo dressRoomChracterInfo = new DataBaseManager.UserCharacterInfo();
        dressRoomChracterInfo.EquippedBackgroundItem = currentBackgroundItem_dressRoom;
        dressRoomChracterInfo.EquippedHairItem = currentHairItem_dressRoom;
        dressRoomChracterInfo.EquippedBodyItem = currentBodyItem_dressRoom;
        dressRoomChracterInfo.EquippedEyesItem = currentEyesItem_dressRoom;
        dressRoomChracterInfo.EquippedEarsItem = currentEarsItem_dressRoom;
        dressRoomChracterInfo.EquippedBackItem = currentBackItem_dressRoom;
        dressRoomChracterInfo.EquippedMouthItem = currentMouthItem_dressRoom;
        dressRoomChracterInfo.EquippedBaseCharacterCode = currentBaseCharacter_dressRoom;
        dressRoomChracterInfo.EquippedSpecialItem = currentSpecialItem_dressRoom;
        string currentCharacterJson = JsonUtility.ToJson(dressRoomChracterInfo);

        last_MyBaseCharacterString = currentBaseCharacter_dressRoom;
        last_MyHairItemString = currentHairItem_dressRoom;
        last_MyBodyItemString = currentBodyItem_dressRoom;
        last_MyEyesItemString = currentEyesItem_dressRoom;
        last_MyEarsItemString = currentEarsItem_dressRoom;
        last_MyMouthItemString = currentMouthItem_dressRoom;
        last_MySpecialItemString = currentSpecialItem_dressRoom;
        last_MyBackItemString = currentBackItem_dressRoom;
        last_MyBackgroundItemString = currentBackgroundItem_dressRoom;

       GoogleSignInDemo.Instance.databaseRef.Child("User").Child(GoogleSignInDemo.Instance.firebaseUser.UserId).Child("Inventory").Child("Equipment")
            .SetRawJsonValueAsync(currentCharacterJson).ContinueWithOnMainThread(task =>
           {
               if(task.IsCompleted)
               {
                   settingCurrentItemsIsOver = true;
               }
           });
    
        while (!settingCurrentItemsIsOver) yield return null;    //// Loading Panel

        dressRoomPanel.SetActive(false);
        DataBaseManager.Instance.boardControlScript.uiButtons.SetActive(true);
        DataBaseManager.Instance.boardControlScript.CharacterMenuButton();


    }


    
    public void CloseViews()
    {
          for (int x = 0; x < views.Length; x++)
        {
            if( views[x].activeInHierarchy)
            views[x].SetActive(false);
        }
    }
    public void ChangeTapButtonColorAndFocus() 
    {
        for (int x = 0; x < tapButtons.Length; x++)
        {
            if (tapButtons[x].gameObject == EventSystem.current.currentSelectedGameObject)
            {
                tapButtons[x].image.color = new Color(1, 1, 1);
                tapSelectFocusImage.transform.position = new Vector3(tapButtons[x].transform.position.x, tapSelectFocusImage.transform.position.y, 0);
            }
            else
            {
                tapButtons[x].image.color = new Color(0.2901f,0.6745f,0.9686f,1);
            }
        }

    }
    public void OnClickBodyTagButton()
    { 
        CloseViews(); 
        body_scrollView.SetActive(true);
        ChangeTapButtonColorAndFocus();
    }
    public void OnClickHairTagButton() 
    { 
        CloseViews();
        hair_scrollView.SetActive(true);
        ChangeTapButtonColorAndFocus();
    }
    public void OnClickAccessoryButton() 
    {
        CloseViews(); 
        accessory_scrollView.SetActive(true);
        ChangeTapButtonColorAndFocus();
    }
    //public void OnClickBackgroundButton()
    //{ 
    //    CloseViews(); 
    //    background_scrollView.SetActive(true);
    //    ChangeTapButtonColorAndFocus();
    //}
    public void OnClickSpecialButton() 
    { 
        CloseViews();
        special_scrollView.SetActive(true);
        ChangeTapButtonColorAndFocus();
    }
    public void OnClickBaseButton()
    { 
        CloseViews(); 
        base_scrollView.SetActive(true);
        ChangeTapButtonColorAndFocus();
    }









}
