﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using EZObjectPools;
using UnityEngine.UI;
using DG.Tweening;

public class Gold_ : MonoBehaviour
{
    PooledObject poolScript;
    SoundManager soundManager;
    AudioSource audioSource;
    AudioClip stackSound;
    Animator animator;

    private void Start()
    {
        poolScript = gameObject.GetComponent<PooledObject>();      
    }

    private void OnEnable()
    {
        soundManager = FindObjectOfType<SoundManager>();
        audioSource = soundManager.gameObject.GetComponent<AudioSource>();
        stackSound = soundManager.goldStackSoundEffect;

        // StartCoroutine(GoldSound());

        StartCoroutine(Destroy());

    }

    public IEnumerator GoldSound()
    {
        RectTransform rect = gameObject.GetComponent<RectTransform>();
        while ( rect.position.y> -300)
        {
            yield return null;
        }

        if(audioSource.clip != stackSound)
        {
            audioSource.clip = stackSound;
            audioSource.Play();
        }



    }

    
  


    private void OnTriggerEnter2D(Collider2D collision)
    {

        audioSource.clip = null;

        RectTransform rect = collision.gameObject.GetComponent<RectTransform>();

    
        if (audioSource.clip != stackSound)
        {
            audioSource.clip = stackSound;
            audioSource.PlayOneShot(stackSound);
        }

        StartCoroutine(ScaleUp(rect));


    }
    IEnumerator ScaleUp(RectTransform rect)
    {
        rect.DOScale(1.3f, 0.2f);
        yield return new WaitForSeconds(0.2f);
        rect.localScale = new Vector3(1, 1, 1);
        poolScript.GoToAvailable();
    }
 

    IEnumerator Destroy ()
    {
        
        float timeCheck = 0;
        float lifeTime = 4;
        while (timeCheck < lifeTime)
        {
            timeCheck += Time.deltaTime;
            yield return null;
        }

        if (gameObject.activeInHierarchy)
        {
            gameObject.transform.DOMove(DataBaseManager.Instance.boardControlScript.characterMenuButton.transform.position, 0.2f);
            yield return new WaitForSeconds(0.2f);

            poolScript.GoToAvailable();
        } 
    }

}
