﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Firebase.Database;
using Firebase.Extensions;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using UnityEngine.EventSystems;

public class WriteNickName : MonoBehaviour
{
    public GameObject ui_buttons;
    public Text errorLog;
    public GameObject NickNameInputPanel;
    public GameObject glowEffectToSelectedSlot;

    
    public string selectedBaseCharacterCode = null;
    public string selectedBackground = null;
    public string selectedHair = null;
    public string selectedBody = null;
    public string selectedEyes = null;
    public string selectedEars = null;
    public string selectedMouth = null;
    public string selectedBack = null;
    public string selectedSpecial = null;

    public InputField nicknameInputField;
    public Text errorMessageText;
    public bool rebundantNickname;
    public bool rebundantChecking = true;
    public bool nicknameIsExisted = false;
    
    public List<string> nicknameList = new List<string>();
    public string idChecker;
    public bool listNumberCheck;

    public DataSnapshot accessCheckSnapshot;
    public bool accessCheckDone = false;

    private void Awake()
    {
        ui_buttons.SetActive(false);
        // Loading Panel
    }
 
    public IEnumerator CheckFirstAccess()
    {
        // Waiting to set Item Run ---> Loading Panel
          while (!ItemManager.Instance.ItemSettingIsOver) yield return null;

      

        string userUid = null;
        string nicknameValue = null;

        if (GoogleSignInDemo.Instance.firebaseUser != null)
        {
            userUid = GoogleSignInDemo.Instance.firebaseUser.UserId;         
        }
        else
        {
            errorLog.text = GoogleSignInDemo.Instance.firebaseUser.UserId.ToString();
   
            yield break;
        }

        GoogleSignInDemo.Instance.databaseRef.Child("User").Child(userUid).Child("Info").GetValueAsync().ContinueWithOnMainThread(task =>
        {
            if (task.IsCompleted)
            {
                 accessCheckSnapshot= task.Result;
            
                 accessCheckDone = true;
            }
        });
        while (!accessCheckDone) yield return null;

          if( !accessCheckSnapshot.HasChild("PlayerNickname") )
          {
               print("First Access");
               DataBaseManager.Instance.boardControlScript.ClosePanels();
               ShowNicknameInputField();            
               errorMessageText.text = "Welcome to Qplay World";        
               yield break;
           }
           else
           {
                 print("Access Again");
                 nicknameValue = accessCheckSnapshot.Value.ToString();
                 DataBaseManager.Instance.currentPlayerNickName = accessCheckSnapshot.Child("PlayerNickname").Value.ToString();
                 DataBaseManager.Instance.currentPlayerUid = GoogleSignInDemo.Instance.firebaseUser.UserId;
                 ui_buttons.SetActive(true);
                 nicknameIsExisted = true;
                 DataBaseManager.Instance.GetGoldFromData();
                 DataBaseManager.Instance.SetUserDataToProfilePanel();
           }  

    }
    public void ShowNicknameInputField()
    {
        if (NickNameInputPanel.activeInHierarchy) return;
        else
        {
            NickNameInputPanel.SetActive(true);
        }     
    }


    public void OnClickBaseCharacterSlot()
    {
        GameObject selectedSlot = EventSystem.current.currentSelectedGameObject;
        BaseCharacterSlot slotScript = selectedSlot.GetComponent<BaseCharacterSlot>();
        selectedBaseCharacterCode = slotScript.baseCharacterCode;
        selectedHair = slotScript.firstHair;
        selectedBody = slotScript.firstBody;
        selectedEars = slotScript.firstEars;
        selectedEyes = slotScript.fisrtEyes;
        selectedMouth = slotScript.firstMouth;
        selectedBack = slotScript.firstBack;
        selectedSpecial = slotScript.firstSpecial;
        selectedBackground = slotScript.firstBackground;

        if (!glowEffectToSelectedSlot.activeInHierarchy) glowEffectToSelectedSlot.SetActive(true);
        glowEffectToSelectedSlot.transform.position = selectedSlot.transform.position;
    }

    public void CheckNicknameAndRegisterButton() { StartCoroutine(CheckNicknameErrorAndRebundant()); }
    public IEnumerator CheckNicknameErrorAndRebundant()
    {
        idChecker = Regex.Replace(nicknameInputField.text, @"[^0-9a-zA-Z가-힣]{1,10}", "", RegexOptions.Singleline);

        if(nicknameInputField.text.Equals(idChecker) == false)
        {
            nicknameInputField.text.Remove(0, nicknameInputField.text.Length);
            nicknameInputField.text = "";
            errorMessageText.text = "10자 이내의 한글과 영어, 숫자만 허용됩니다.";
            yield break;
        }

        errorMessageText.text = "닉네임 중복 검사 중.....";
     
        DataSnapshot snapList = null;
        bool listCheck = false;
        nicknameList.Clear();
        GoogleSignInDemo.Instance.databaseRef.Child("UserNicknameList").GetValueAsync().ContinueWithOnMainThread(task =>
        {
            if(task.IsCompleted)
            {
                snapList= task.Result;
                print( snapList.ChildrenCount);
                foreach ( var item in snapList.Children)
                {
                    print(item.Key);
                    nicknameList.Add(item.Key);
                }
                print(snapList);
                listCheck = true;
            }
            else
            {
            errorLog.text = "Get NicknameList_ Failed";
            }
        });

        while (!listCheck) yield return null;

        for (int x = 0; x < nicknameList.Count; x++)
        {
            if (nicknameInputField.text == nicknameList[x])
            {
                rebundantNickname = true;                
            }
        }

       if (rebundantNickname && selectedBaseCharacterCode != null)
        {
            errorMessageText.text = $"{nicknameInputField.text}는 이미 사용 중 입니다.";
            errorMessageText.color = Color.red;
            nicknameInputField.text.Remove(0, nicknameInputField.text.Length);
            nicknameInputField.text = "";
            rebundantNickname = false;
            yield break;
        }
       if( !rebundantNickname && ( selectedBaseCharacterCode == null || selectedBaseCharacterCode == "") )
        {
            errorMessageText.text = "캐릭터를 선택하세요!!";
            errorMessageText.color = Color.red;
            yield break;
        }
      

       else if ( !rebundantNickname && (selectedBaseCharacterCode != null && selectedBaseCharacterCode != "") )
        {
            //////// finally register User Data ;
            GoogleSignInDemo.Instance.databaseRef.Child("UserNicknameList").Child(nicknameInputField.text).SetValueAsync(GoogleSignInDemo.Instance.firebaseUser.UserId);           
            DataBaseManager.Instance.SaveUserInfoWhenNicknameRegistered();
            errorMessageText.text = "Qplay에 오신 것을 환영합니다 !!!!";
            //errorMessageText.color = Color.green;
            DataBaseManager.Instance.currentPlayerNickName = nicknameInputField.text;
            yield return new WaitForSeconds(2f);
            ui_buttons.SetActive(true);
            NickNameInputPanel.SetActive(false);
            nicknameIsExisted = true;
            DataBaseManager.Instance.GetGoldFromData();
            DataBaseManager.Instance.SetUserDataToProfilePanel();

        }
       

    }
    
}
