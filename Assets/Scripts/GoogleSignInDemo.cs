﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Firebase;
using Firebase.Auth;
using Firebase.Extensions;
using Firebase.Database;

using Google;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GoogleSignInDemo : MonoBehaviour
{

    /////////////////////////  Google Login 
    public Button googleLoginButton;
    public static GoogleSignInDemo _instance;
    public static GoogleSignInDemo Instance
    {
        get
        {
            if(!_instance)
            {
                _instance = FindObjectOfType<GoogleSignInDemo>();

                if(_instance == null )
                {
                    GameObject container = new GameObject();
                    container.name = "GoogleManager";
                    _instance = container.AddComponent<GoogleSignInDemo>();
                }                
            }
            return _instance;
        }
    }

    public Text infoText;
    public string webClientId = "<your client id here>";

    private FirebaseAuth auth;
    private GoogleSignInConfiguration configuration;

    //////////////// Email LogIn 
    public Button emailLoginButton;
    public bool isFirebaseReady { get; private set; }
    public bool isLoginProgress { get; private set; }

    public FirebaseApp firebaseApp;
    public FirebaseUser firebaseUser;

    public Text emailInput;
    public Text password;

    public DatabaseReference databaseRef;


    private void Awake()
    {
        databaseRef = FirebaseDatabase.DefaultInstance.RootReference;
        DontDestroyOnLoad(gameObject);
       
    }
    private void Start()
    {
        if (!isFirebaseReady)
        {
            configuration = new GoogleSignInConfiguration { WebClientId = webClientId, RequestEmail = true, RequestIdToken = true };
            CheckFirebaseDependencies();
        }
    }

    private void CheckFirebaseDependencies()
    {
        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWithOnMainThread(task =>
        {
            if (task.IsCompleted)
            {
                if (task.Result == DependencyStatus.Available)
                {
                    auth = FirebaseAuth.DefaultInstance;
                    firebaseApp = FirebaseApp.DefaultInstance;
                    isFirebaseReady = true;

                }
                else
                {
                    AddToInformation("Could not resolve all Firebase dependencies: " + task.Result.ToString());
                    Debug.LogError("Could not resolve all Firebase dependencies: " + task.Result.ToString());
                }
            }
            else
            {
               AddToInformation("Dependency check was not completed. Error : " + task.Exception.Message);
                Debug.LogError("Dependency check was not completed. Error : " + task.Exception.Message);
            }
        });
    }

    public void OnEmailLogin ()
    {

        if (!isFirebaseReady || firebaseUser != null) return;
        emailLoginButton.interactable = false;
        googleLoginButton.interactable = false;

        auth.SignInWithEmailAndPasswordAsync(emailInput.text, password.text).ContinueWithOnMainThread
            (task =>
          {
                   if( task. IsFaulted)
                   {
                        Debug.LogError($"Faulted :{task.Exception}");
                         emailLoginButton.interactable = true;
                         googleLoginButton.interactable = true;
              }
                   else if(task.IsCanceled)
                   {
                         Debug.LogError("Canceled");
                         emailLoginButton.interactable = true;
                         googleLoginButton.interactable =true;
                   }
                   else if (task.IsCompleted)
                   {                    
                          firebaseUser = task.Result;
                          SceneManager.LoadScene("Lobby_");                     
                   }
            });
    }

    public void SignInWithGoogle() 
    { 
       OnSignIn();
       googleLoginButton.interactable = false;
       emailLoginButton.interactable = false;
    }
    public void SignOutFromGoogle() { OnSignOut(); }

    private void OnSignIn()
    {
        GoogleSignIn.Configuration = configuration;
        GoogleSignIn.Configuration.UseGameSignIn = false;
        GoogleSignIn.Configuration.RequestIdToken = true;
       AddToInformation("Calling SignIn");
        Debug.Log("Calling SignIn");
        GoogleSignIn.DefaultInstance.SignIn().ContinueWith(OnAuthenticationFinished);
    }

    private void OnSignOut()
    {
       AddToInformation("Calling SignOut");

        GoogleSignIn.DefaultInstance.SignOut();
    }

    public void OnDisconnect()
    {
       AddToInformation("Calling Disconnect");
        GoogleSignIn.DefaultInstance.Disconnect();
    }

    internal void OnAuthenticationFinished(Task<GoogleSignInUser> task)
    {
        if (task.IsFaulted)
        {
            googleLoginButton.interactable = true;
            emailLoginButton.interactable = true;

            using (IEnumerator<Exception> enumerator = task.Exception.InnerExceptions.GetEnumerator())
            {
                if (enumerator.MoveNext())
                {
                    GoogleSignIn.SignInException error = (GoogleSignIn.SignInException)enumerator.Current;
                   AddToInformation("Got Error: " + error.Status + " " + error.Message);
                    Debug.LogError("Got Error: " + error.Status + " " + error.Message);
                }
                else
                {
                 AddToInformation("Got Unexpected Exception?!?" + task.Exception);
                    Debug.LogError("Got Unexpected Exception?!?" + task.Exception);
                }
            }
        }
        else if (task.IsCanceled)
        {
           AddToInformation("Canceled");
            Debug.LogError("Canceled");
        }
        else
        {
          AddToInformation("Welcome: " + task.Result.DisplayName + "!");
          AddToInformation("Email = " + task.Result.Email);
          AddToInformation("Google ID Token = " + task.Result.IdToken);
           AddToInformation("Email = " + task.Result.Email);
            SignInWithGoogleOnFirebase(task.Result.IdToken);

        }
    }

    private void SignInWithGoogleOnFirebase(string idToken)
    {
        Credential credential = GoogleAuthProvider.GetCredential(idToken, null);

        auth.SignInWithCredentialAsync(credential).ContinueWith(task =>
        {
        AggregateException ex = task.Exception;
            if (ex != null)
            {
                if (ex.InnerExceptions[0] is FirebaseException inner && (inner.ErrorCode != 0))
                {
                    AddToInformation("\nError code = " + inner.ErrorCode + " Message = " + inner.Message);
                    Debug.LogError("\nError code = " + inner.ErrorCode + " Message = " + inner.Message);
                }
            }
            else
            {

                firebaseUser = task.Result;
                AddToInformation($"{firebaseUser}");
                Debug.Log("Success");
                SceneManager.LoadScene("Lobby_");

            }
        });
    }

    public void OnSignInSilently()
    {
        GoogleSignIn.Configuration = configuration;
        GoogleSignIn.Configuration.UseGameSignIn = false;
        GoogleSignIn.Configuration.RequestIdToken = true;
     //   AddToInformation("Calling SignIn Silently");

        GoogleSignIn.DefaultInstance.SignInSilently().ContinueWith(OnAuthenticationFinished);
    }

    public void OnGamesSignIn()
    {
        GoogleSignIn.Configuration = configuration;
        GoogleSignIn.Configuration.UseGameSignIn = true;
        GoogleSignIn.Configuration.RequestIdToken = false;
       // AddToInformation("Calling Games SignIn");
        GoogleSignIn.DefaultInstance.SignIn().ContinueWith(OnAuthenticationFinished);


    }

    private void AddToInformation(string str)
    {
        if (infoText) infoText.text += "\n" + str;
    }

  

}