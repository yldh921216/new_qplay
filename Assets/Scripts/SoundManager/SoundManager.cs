﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class SoundManager : MonoBehaviour
{
    public static SoundManager _instance;

    public static SoundManager instance
    {
        get
        {
            if(_instance)
            {
                _instance = FindObjectOfType<SoundManager>();

                if (_instance == null)
                {
                    GameObject container = new GameObject();
                    container.name = "SoundManager";
                    _instance = container.AddComponent<SoundManager>();
                }


            }


            return _instance;

        }

       
    }


    public AudioSource audioSource;
    public AudioClip buttonClickSoundEffect;
    public AudioClip successSoundEffect;
    public AudioClip failedSoundEffect;
    public AudioClip petSoundEffect;
    public AudioClip writingSuccessSoundEffect;
    public AudioClip thumbsUpSoundEffect;
    public AudioClip goldStackSoundEffect;

    public Button[] buttons;

    public void Awake()
    {
        for ( int x = 0; x<buttons.Length; x++ )
        {
            buttons[x].onClick.AddListener(StartButtonClickSoundEffect);
        }
    }


    public void GoldStackSoundEffect()
    {
        audioSource.PlayOneShot(goldStackSoundEffect);
    }
    public void ThumbsUpSoundEffect()
    {
        audioSource.PlayOneShot(thumbsUpSoundEffect);
    }

    public void StartButtonClickSoundEffect ()
    {
        audioSource.clip = buttonClickSoundEffect;
        audioSource.Play();
    }

    public void WritingSound()
    {
        audioSource.clip = writingSuccessSoundEffect;
        audioSource.Play();
    }

    public void FailedSound()
    {

    }

    public void SuccessSound ()
    {

    }

    public void PetSound()
    {
        audioSource.clip = petSoundEffect;
        audioSource.Play();
    }

}
